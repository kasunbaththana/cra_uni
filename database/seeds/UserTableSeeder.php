<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \CRA\User::create([
        'name' => "developer",
        'email' => "developer@gmail.com",
        'password' => \Illuminate\Support\Facades\Hash::make('password'),
        'remember_token' => str_random(10),

        ]);
    }
}
