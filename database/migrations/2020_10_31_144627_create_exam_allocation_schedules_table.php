<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExamAllocationSchedulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('examAllocationSchedules', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('allocation_id');
            $table->date('schedule_date');
            $table->time('from_time');
            $table->time('to_time');
            $table->boolean('leave');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('examAllocationSchedules');
    }
}
