<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBatchDegreeSubjectTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('batch_degree_subject', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('batch_id')->unsigned();
            $table->integer('degree_id')->unsigned();
            $table->integer('subject_id')->unsigned();
            $table->integer('subject_duration');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('batch_degree_subject');
    }
}
