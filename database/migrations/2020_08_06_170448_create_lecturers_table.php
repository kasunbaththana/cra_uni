<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLecturersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lecturers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nic', 12);
            $table->string('salutation', 5)->default('');
            $table->string('name', 100);
            $table->string('long_name', 250);
            $table->string('address_line1', 100);
            $table->string('address_line2', 100)->nullable();
            $table->string('address_line3', 100)->nullable();
            $table->string('telephone1', 12);
            $table->string('telephone2', 12)->nullable();
            $table->string('mobile', 12)->nullable();
            $table->string('fax', 12)->nullable();
            $table->string('email', 200)->nullable();
            $table->string('note', 255)->nullable();
            $table->string('photo', 200)->nullable();
            $table->boolean('is_active')->default(0);
            $table->boolean('is_external')->default(0);
            $table->string('work_place', 100)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lecturers');
    }
}
