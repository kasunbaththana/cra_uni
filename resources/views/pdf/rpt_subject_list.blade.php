{{ csrf_field() }}
<html>
<head>
    <style>
        th,td,tr {
            border:0.5px solid black;
            border-collapse:collapse;
        }

        table {
            border:0.5px solid black;
            border-collapse:collapse;
        }
    </style>
</head>
<body>
<h2>Subject List Report</h2>
<h4>University of Vocational Technology</h4>
<table width="100%">
    <thead>
        <tr>
            <th>Id</th>
            <th>Name</th>
        </tr>
    </thead>
        <tbody>
            @foreach($data as $list)
                <tr>
                    <td>{{$list->id}}</td>
                    <td>{{$list->name}}</td>
                </tr>

            @endforeach
        </tbody>
</table>
</body>
</html>