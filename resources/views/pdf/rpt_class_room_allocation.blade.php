{{ csrf_field() }}
<html>
<head>
    <style>
        th,td,tr {
            border:0.5px solid black;
            border-collapse:collapse;
        }

        table  {
            border:0.5px solid black;
            border-collapse:collapse;
        }
    </style>
</head>
<body>
<h2>Class Room Allocation Report - {{date('Y')}}</h2>
<h4>University of Vocational Technology</h4>


        <p>From {{$param['from_date']}}  To {{$param['to_date']}}</p>



<table id="grd" width="100%">
    <thead>
    <tr>
        <th>Batch</th>
        <th>Class Room</th>
        <th>Subject</th>
        <th>Schedule Date</th>
        <th>On</th>
        <th>From Time</th>
        <th>To Time</th>
    </tr>
    </thead>
    <tbody>
    @foreach($data as $list)
        <tr>
            <td>{{$list->batch}}</td>
            <td>{{$list->class_room}}</td>
            <td>{{$list->sub_name}}</td>
            <td>{{$list->schedule_date}}</td>
            <td>{{$list->wk}}</td>
            <td>{{$list->from_time}}</td>
            <td>{{$list->to_time}}</td>
        </tr>

    @endforeach
    </tbody>
</table>
</body>
</html>
