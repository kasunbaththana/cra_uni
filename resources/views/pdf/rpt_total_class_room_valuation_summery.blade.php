{{ csrf_field() }}
<html>
<head>
    <style>
        th {
            border:0.5px solid grey;
            border-collapse:collapse;
            padding: 2px;

        }
        .th-w-300{
            width: 300px;
        }
        .th-w-100{
            width: 100px;
        }

        table  {
            border:0.5px solid grey;
            border-collapse:collapse;
        }
        .b_none {
            border:0.2px solid #dde2f1;

        }
        .b_active  {
           border:0.2px solid grey;

        }.center{
            text-align: center;
                 }
        .bold{
            font-weight: bold;
        }



    </style>
</head>
<body>
<h2>Total Class Room Valuation Summary Report - {{date('Y')}}</h2>
<h4>University of Vocational Technology</h4>


<p>From: {{$param['from_date']}}  To: {{$param['to_date']}}</p>
<p>Class Room: {{$param['class_room']}}</p>



<table id="grd" width="100%">
    <thead>
    <tr>
        <th class="th-w-100">Month</th>
        <th class="th-w-300">Total House</th>
    </tr>
    </thead>
    <tbody>
    {{--@foreach($rooms as $cat)--}}


       {{--{{$col1,$col2,$col3,$col4,$col5,$col6,$col7=null}}--}}
                 {{$total=0}}
                @foreach($data as $list)
                    <tr>
                   {{-- @if($cat->id == $list->id)--}}


                        <td class="b_active"><div>{!! $list->s_date !!}</div></td>

                        <td class="center b_active"><div class="center">{!! $list->time_dif !!}</div></td>

                        {{$total+=$list->time_dif   }}
                     {{--@endif--}}
                    </tr>

                {{--@endforeach--}}

    @endforeach
                <tr>
                    <td class="bold">Total Time: </td>
                    <td class="bold center">{!! round(floatval($total),2) !!}</td>
                </tr>
    </tbody>
</table>
</body>
</html>
