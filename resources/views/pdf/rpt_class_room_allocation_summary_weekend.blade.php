{{ csrf_field() }}
<html>
<head>
    <style>
        th {
            border:0.5px solid grey;
            border-collapse:collapse;
            padding: 2px;

        }
        .th-w-300{
            width: 300px;
        }
        .th-w-100{
            width: 100px;
        }

        table  {
            border:0.5px solid grey;
            border-collapse:collapse;
        }
        .b_none {
            border:0.2px solid #dde2f1;

        }
        .b_active  {
           border:0.2px solid grey;

        }
        table td{
          font-size: 24px;
        }



    </style>
</head>
<body>
<h2>Class Room Allocation Report - {{date('Y')}}</h2>
<h4>University of Vocational Technology</h4>


<p>From {{$param['from_date']}}  To {{$param['to_date']}}</p>



<table id="grd" width="100%">
    <thead>
    <tr>
        <th class="th-w-100">Batch Name</th>
        <th class="th-w-300">Saturday</th>
        <th class="th-w-300">Sunday</th>
       
   

    </tr>
    </thead>
    <tbody>
    @foreach($rooms as $cat)


       {{--{{$col1,$col2,$col3,$col4,$col5,$col6,$col7=null}}--}}

                @foreach($data as $list)
                    <tr>
                    @if($cat->id == $list->batch_id)


                        <td class="b_active"><div>{!! $cat->name !!}</div></td>

                        <td class={{$list->Saturday==""?"b_none":"b_active"}}><div>{{ $list->Saturday }}</div></td>
                        <td class={{$list->Sunday==""?"b_none":"b_active"}}><div>{!! $list->Sunday !!}</div></td>
                      
                     
                     @endif
                    </tr>

                @endforeach


                {{--@if($col1!=null)--}}
                {{--<td class="col-1"><div></div></td>--}}
                 {{--@endif--}}
                {{--@if($col2!=null)--}}
                    {{--<td class="col-1"><div></div></td>--}}
                {{--@endif--}}
                {{--@if($col3!=null)--}}
                    {{--<td class="col-1"><div></div></td>--}}
                {{--@endif--}}
                {{--@if($col4!=null)--}}
                    {{--<td class="col-1"><div></div></td>--}}
                {{--@endif--}}
                {{--@if($col5!=null)--}}
                    {{--<td class="col-1"><div></div></td>--}}
                {{--@endif--}}
               

                {{--@endif--}}
    @endforeach
    </tbody>
</table>
</body>
</html>
