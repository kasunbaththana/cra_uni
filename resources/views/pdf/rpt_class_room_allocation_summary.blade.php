{{ csrf_field() }}
<html>
<head>
    <style>
        th {
            border:0.5px solid grey;
            border-collapse:collapse;
            padding: 2px;

        }
        .th-w-300{
            width: 300px;
        }
        .th-w-100{
            width: 100px;
        }

        table  {
            border:0.5px solid grey;
            border-collapse:collapse;
        }
        .b_none {
            border:0.2px solid #dde2f1;

        }
        .b_active  {
           border:0.2px solid grey;

        }




    </style>
</head>
<body>
<h2>Class Room Allocation Report - {{date('Y')}}</h2>
<h4>University of Vocational Technology</h4>


<p>From {{$param['from_date']}}  To {{$param['to_date']}}</p>



<table id="grd" width="100%">
    <thead>
    <tr>
        <th class="th-w-100">Class Room</th>
        <th class="th-w-300">Monday</th>
        <th class="th-w-300">Tuesday</th>
        <th class="th-w-300">Wednesday</th>
        <th class="th-w-300">Thursday</th>
        <th class="th-w-300">Friday</th>
        <th class="th-w-300">Saturday</th>
        <th class="th-w-300">Sunday</th>

    </tr>
    </thead>
    <tbody>
    @foreach($rooms as $cat)


       {{--{{$col1,$col2,$col3,$col4,$col5,$col6,$col7=null}}--}}

                @foreach($data as $list)
                    <tr>
                    @if($cat->id == $list->id)


                        <td class="b_active"><div>{!! $cat->name !!}</div></td>

                        <td class={{$list->Monday==""?"b_none":"b_active"}}><div>{!! $list->Monday !!}</div></td>
                        <td class={{$list->Tuesday==""?"b_none":"b_active"}}><div>{!! $list->Tuesday !!}</div></td>
                        <td class={{$list->Wednesday==""?"b_none":"b_active"}}><div>{!! $list->Wednesday!!}</div></td>
                        <td class={{$list->Thursday==""?"b_none":"b_active"}}><div>{!! $list->Thursday!!}</div></td>
                        <td class={{$list->Friday==""?"b_none":"b_active"}}><div>{!! $list->Friday!!}</div></td>
                        <td class={{$list->Saturday==""?"b_none":"b_active"}}><div>{!! $list->Saturday!!}</div></td>
                        <td class={{$list->Sunday==""?"b_none":"b_active"}}><div>{!! $list->Sunday!!}</div></td>


                     @endif
                    </tr>

                @endforeach


                {{--@if($col1!=null)--}}
                {{--<td class="col-1"><div></div></td>--}}
                 {{--@endif--}}
                {{--@if($col2!=null)--}}
                    {{--<td class="col-1"><div></div></td>--}}
                {{--@endif--}}
                {{--@if($col3!=null)--}}
                    {{--<td class="col-1"><div></div></td>--}}
                {{--@endif--}}
                {{--@if($col4!=null)--}}
                    {{--<td class="col-1"><div></div></td>--}}
                {{--@endif--}}
                {{--@if($col5!=null)--}}
                    {{--<td class="col-1"><div></div></td>--}}
                {{--@endif--}}
                {{--@if($col6!=null)--}}
                    {{--<td class="col-1"><div></div></td>--}}
                {{--@endif--}}
                {{--@if($col7!=null)--}}
                    {{--<td class="col-1"><div></div></td>--}}
                {{--@endif--}}

                {{--@endif--}}
    @endforeach
    </tbody>
</table>
</body>
</html>
