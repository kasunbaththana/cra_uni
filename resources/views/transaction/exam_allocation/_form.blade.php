{{ csrf_field() }}
<div class="row ">
<div class="col-sm-7 border">
<div class="row ">
    <div class="col-sm-5 ">
        <div class="form-group  mb-sm-0">
            {!! Form::label('id','Request No ',['class'=>'label-info mb-sm-0']) !!}
            @if($button_caption=='Update')
                {!! Form::text('id', $exam_allocation->id,['class'=>'form-control form-control-sm','readonly'=>'true']) !!}
            @else
                {!! Form::text('id', $new_index,['class'=>'form-control form-control-sm text-gray-900','readonly'=>'true']) !!}
            @endif
            @if($errors->has('id'))
                <span class="alert-danger"> {{ $errors->first('id') }}</span>
            @endif
        </div>
    </div>


</div>
<div class="row">
    <div class="col-sm-6">
        <div class="form-group">
            {!! Form::label('hall_id','Hall No',['class'=>'label-info mb-sm-0']) !!}
            {!! Form::text('hall_id', $button_caption=='Update'?$exam_allocation->hallId->name:'',['class'=>'form-control form-control-sm text-gray-900  ','id'=>'hall_id']) !!}

                <span id="span_hall_id" class="alert-danger"> {{ $errors->first('hall_id') }}</span>

            <input type="hidden" id="h_hall_id" name="h_hall_id" value="{{$button_caption=='Update'?$exam_allocation->hall_id:''}}" />
        </div>
    </div>

    <div class="col-sm-6 btn-toolbar h-25 mt-3" role="toolbar">

    </div>
</div>
    <div class="row">
        <div class="col-sm-10">
            <div class="form-group">
                {!! Form::label('batch_id','Batch',['class'=>'label-info mb-sm-0']) !!}
                {!! Form::text('batch_id', $button_caption=='Update'?null:'',['class'=>'form-control form-control-sm text-gray-900 ']) !!}

                <span id="span_batch_id" class="alert-danger"> {{ $errors->first('batch_id') }}</span>

                <input type="hidden" id="h_batch_id" name="h_batch_id" value="{{$button_caption=='Update'?$exam_allocation->batch_id:''}}"/>
            </div>
        </div>
        <input type="hidden" name="h_batch_cap" id="h_batch_cap"/>
    </div>

    <div class="row">
        <div class="col-sm-10">
            <div class="form-group">
                {!! Form::label('subject_id','Subject',['class'=>'label-info mb-sm-0']) !!}
                {!! Form::text('subject_id', $button_caption=='Update'?null:'',['class'=>'form-control form-control-sm text-gray-900 ']) !!}

                <span id="span_subject_id" class="alert-danger"> {{ $errors->first('subject_id') }}</span>

                <input type="hidden" id="h_subject_id" name="h_subject_id" value="{{$button_caption=='Update'?$exam_allocation->subject_id:''}}"/>
            </div>
        </div>
    </div>

    <input type="hidden" name="h_hall_cap" id="h_hall_cap"/>
    <div class="m-2">
    <div class="row ml-2">
        <div class=" border col-sm-6">
            <div class="form-group">
                <div class="date_from" data-provide="datepicker">
                {!! Form::label('from_date','From Date',['class'=>'label-info mb-sm-0 chg_schedule']) !!}
                {!! Form::text('from_date',$exam_allocation->from_date,['class'=>'form-control form-control-sm text-gray-900 chg_schedule']) !!}
                <div class="input-group-addon">
                    <span class="glyphicon glyphicon-th"></span>
                </div>
            </div>
            </div>
        </div>
        <div class="col-sm-6  p-2">
            <div class="form-group ">
                <div class='input-group' >
                    {!! Form::label('from_time','From ',['class'=>'label-info mb-sm-0 col-sm-3']) !!}
                    {!! Form::time('from_time', $exam_allocation->from_time,['class'=>'form-control  form-control-sm text-gray-900 time chg_schedule','id'=>'from_time']) !!}
                    <div class="input-group-addon">
                        <span class="glyphicon glyphicon-time"></span>
                    </div>
                </div>

            </div>
            <div class="form-group">
                <div class='input-group' >
                    {!! Form::label('to_time','To ',['class'=>'label-info mb-sm-0 col-sm-3']) !!}
                    {!! Form::time('to_time', $exam_allocation->to_time,['class'=>'form-control form-control-sm text-gray-900 time chg_schedule','id'=>'to_time']) !!}
                    <div class="input-group-addon">
                        <span class="glyphicon glyphicon-time"></span>
                    </div>
                </div>

            </div>

        </div>

    </div>

    {{--</div>--}}

    {{--</div>--}}
    </div>

    <div class="row p-2">
        <div class="col-sm-12 border">
            <div class="input-group">
                <div class="col-sm-4 ">
                </div>
                <div class="col-sm-4 ">

                </div>
                <div class="col-sm-4 ">
                    <button id="generate" type="button" class="btn btn-sm  btn-info float-right">Generate <i class="fa fa-arrow-alt-circle-down"></i></button>
                </div>
            </div>
        </div>

    </div>


</div>
<div class="col-sm-5">

        <div class="card shadow mb-0">

            @include('transaction.exam_allocation.other.add_new_item')

        </div>



</div>

</div>
<div class="row mt-2">
    <div class="col-sm-12">
        <table name="tbl_schedule" class="table-sm  table-bordered table-condensed table-striped" id="tbl_schedule"
               width="100%" cellspacing="0">
            <thead>
            <tr class="bg-gray-300">
                <th>#</th>
                <th>Date</th>
                <th>Degree</th>
                <th>Subject</th>
                <th>From Time</th>
                <th>To Time</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            @if(isset($schedule))
                @foreach($schedule as $list)
                    <tr>
                        <td>{{++$i}}</td>
                        <td><input readonly type="text"  class="border-0 text-gray-800 bg-gray-100" id="s_date_{{$i}}" name="s_date_{{$i}}" value="{{$list->schedule_date}}"></td>
                        <td><input readonly type="text" class="border-0 text-gray-800 bg-gray-100" id="degree_{{$i}}" name="degree_{{$i}}" value="{{$list->batchId->name}}"></td>
                        <td><input readonly type="text" class="border-0 text-gray-800 bg-gray-100" id="subject_{{$i}}" name="subject_{{$i}}" value="{{$list->subjectId->name}}"></td>
                        <td><input  readonly type="text" class="border-0 text-gray-800 bg-gray-100" id="f_time_{{$i}}" name="f_time_{{$i}}" value="{{$list->from_time}}"></td>
                        <td><input   readonly type="text" class="border-0 text-gray-800 bg-gray-100" id="t_time_{{$i}}" name="t_time_{{$i}}" value="{{$list->to_time}}"></td>
                        <td  class="text-center"><a type="button"  class="btn btn-danger btn-circle btn-sm text-gray-100"><i class="fas fa-trash"></i></a></td>
                        <input type="hidden" name="date_{{$i}}" id="date_{{$i}}" value="{{$list->schedule_date}}" />
                        <input type="hidden" name="degree_id_{{$i}}" id="degree_id_{{$i}}" value="{{$list->batchId->id}}" />
                        <input type="hidden" name="subject_id_{{$i}}'" id="subject_id_{{$i}}" value="{{$list->subjectId->id}}" />
                    </tr>

                @endforeach
            @endif

            </tbody>
        </table>
    </div>
    <input type="hidden" name="schedule" id="schedule">
</div>
<div class="float-right mb-2 mt-5">

    <input type="hidden" id="temp_" name="temp_"  class="text-gray-900"/>
    <button type="button"  id="btnSave" class="btn  btn-success "><i class="fa fa-save"></i>{{$button_caption}}</button>
    <button type="button" class="btn  btn-info"><i class="fa fa-refresh"></i>Reset</button>

</div>


