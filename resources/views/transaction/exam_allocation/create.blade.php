@extends('layouts.main')
@section('content')
    {{-- <div class="container">--}}
    <div class="container-fluid">

        <div class="card o-hidden border-0 shadow-lg my-1">
            <div class="card-body p-0">
                <!-- Nested Row within Card Body -->
                {!! Form::model($exam_allocation , ['url' => ['exam_allocation/save'],'id'=>'exam_allocation']) !!}
                <div class="row">
                    <div class="col-lg-12">
                        <div class="p-3">
                            <div class="text-center">
                                <h1 class="h4 text-gray-900 mb-4">Exam Allocation</h1>
                            </div>

                            @include('transaction.exam_allocation._form',['button_caption'=>'Save'])
                            @include('commonForm.searchModel')
                        </div>
                    </div>


                </div>
                {!! Form::close() !!}
            </div>
        </div>

    </div>
@stop

@section('alert')

@stop
@section('application_js')
    {{--<script src="/js/application/batch.js" type="text/javascript"></script>--}}
    <script src="/js/application/exam_allocation.js" type="text/javascript"></script>

@stop

