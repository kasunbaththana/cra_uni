{{--//guarantor--}}
<div class="text-left">
    <h8 class="h8 text-gray-600 mb-4">Add Lecturer</h8>
</div>
<div class="card-header py-3">
    <div class="float-right">
        <a id="btnAddItem" class="btn btn-sm btn-info add-new text-gray-100"  data-target="#myModal" data-toggle="modal"><i
                    class="fa fa-plus "></i> Add Lecturer</a>
    </div>
</div>
<div class="card-body">
    <div class="table-sm">
        <table id="tbl_item" class="table-sm table-bordered"  width="100%" cellspacing="0">
            <thead>
            <tr>
                <th>Description</th>

            </tr>
            </thead>
            <tbody>
            @foreach($set_items as $cllist)
                <tr>
                    <td>
                        <input class="form-control form-control-sm" border="0" readonly id="tbl_item_{{$cllist->lectureId->id}}"
                        name="tbl_item[{{$cllist->lectureId->id}}]"
                        value="{{$cllist->lectureId->name}}"/>
                    </td>
                    <td> <a type="button"  class="btn btn-danger btn-circle btn-sm text-gray-100"><i class="fas fa-trash"></i></a>
                    </td>
                    </tr>
            @endforeach

            </tbody>
        </table>
    </div>
</div>
@include('commonForm.AddItem')
