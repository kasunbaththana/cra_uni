@extends('layouts.main')
@section('content')
    <div class="container-fluid">

        {{--editable Page--}}
{{--
        <div class="card-body">

        </div>--}}

        <!-- Page Heading -->
        <h6 class="h4 mb-2 text-gray-800">Exam Hall Dashboard</h6>

        <!-- DataTales Example -->
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <div class="row">
                    <div class="col-md-1">
                    {!! Form::label('t_date','Date',['class'=>'label-info mb-sm-0']) !!}
                    </div>
                    <div class="col-md-3">
                    <div class="date" data-provide="datepicker">
                        {!! Form::text('t_date',$today,['class'=>'form-control form-control-sm','id'=>'t_date']) !!}
                        <div class="input-group-addon">
                            <span class="glyphicon glyphicon-th"></span>
                        </div>
                    </div>
                </div>
                    <div class="col-md-1">
                        {!! Form::label('building_id','Filtering',['class'=>'label-info mb-sm-0']) !!}
                    </div>
                    <div class="col-md-4">



                            {!! Form::select('building_id', $buildings,'',['class'=>'form-control form-control-sm','data-live-search'=>'true']) !!}

                        </div>

                    <div class="col-md-3">

                        <a id="btnAddNew" class="btn btn-sm btn-info add-new float-right" href="{{url('exam_allocation/create')}}">
                            <i class="fa fa-plus"></i> Add New</a>

                    </div>
                </div>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table-sm table-bordered " id="grid_room" width="100%" cellspacing="0">
                        <thead>
                        <tr class="bg-gray-100">
                            <th class="col-sm-1 ">Class NO</th>
                            <th class="col-sm-2 ">Morning session</th>
                            <th class="col-sm-2 ">Evening session</th>
                        </tr>
                        </thead>
                        <tbody>


                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>
@stop
@section('alert')

@stop
@section('application_js')
    <script src="/js/application/exam_allocation.js" type="text/javascript"></script>

@stop