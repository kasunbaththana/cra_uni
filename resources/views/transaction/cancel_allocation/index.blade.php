@extends('layouts.main')
@section('content')
    <div class="container-fluid">

        {{--editable Page--}}
{{--
        <div class="card-body">

        </div>--}}

        <!-- Page Heading -->
        <h6 class="h4 mb-2 text-gray-800">Class Room Allocation Cancel</h6>

        <!-- DataTales Example -->
        <div class="card shadow mb-4">
            <div class="card-header py-3">

            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table id="dataTable" class="table-sm table-bordered " id="grid_room_approval" width="100%" cellspacing="0">
                        <thead>
                        <tr class="bg-gray-100">
                            <th class="col-sm-1 ">Request No</th>
                            <th class="col-sm-2 ">Class Room</th>
                            <th class="col-sm-4 ">Batch</th>
                            <th class="col-sm-2 ">From Date</th>
                            <th class="col-sm-2 ">To Date</th>
                            <th class="col-sm-2 ">From Time</th>
                            <th class="col-sm-2 ">To Time</th>
                            <th class="col-sm-1 ">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($room_allocation as $cllist)
                            <tr>
                                <td class="col-md-1">{{$cllist->id}}</td>
                                <td class="col-md-2">{{$cllist->roomId->name}}</td>
                                <td class="col-md-4">{{$cllist->batchId->name}}</td>
                                <td class="col-md-2">{{$cllist->from_date}}</td>
                                <td class="col-md-2">{{$cllist->to_date}}</td>
                                <td class="col-md-2">{{$cllist->from_time}}</td>
                                <td class="col-md-2">{{$cllist->to_time}}</td>
                                <td class="col-md-1">
                                    <a id="edit" type="button" href="{{route('room_cancel.edit',$cllist->id)}}"
                                       class="btn btn-sm btn-warning -edit"><i class="fa fa-edit"></i></a>
                                </td>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>
                    {{--  <div>{{$Clients->links()}}</div>--}}
                </div>
            </div>
        </div>

    </div>
@stop
@section('alert')

@stop
@section('application_js')
    <script src="/js/application/room_cancel.js" type="text/javascript"></script>

@stop