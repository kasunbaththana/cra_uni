@extends('layouts.main')
@section('content')
    {{-- <div class="container">--}}
    <div class="container-fluid">

        <div class="card o-hidden border-0 shadow-lg my-1">
            <div class="card-body p-0">
                <!-- Nested Row within Card Body -->
                {!! Form::model($room_allocation, ['url' => ['room_allocation/update'],'enctype'=>"multipart/form-data",'method' => 'put','id'=>'room_allocation']) !!}
                <div class="row">
                    <div class="col-lg-12">
                        <div class="p-3">
                            <div class="text-center">
                                <h1 class="h4 text-gray-900 mb-4">Approval</h1>
                            </div>

                            @include('transaction.approval._form',['button_caption'=>'Approve'])
                            @include('commonForm.searchModel')
                        </div>
                    </div>


                </div>
                {!! Form::close() !!}
            </div>
        </div>

    </div>
@stop

@section('alert')

@stop
@section('application_js')
    {{--<script src="/js/application/batch.js" type="text/javascript"></script>--}}
    <script src="/js/application/room_approve.js" type="text/javascript"></script>

@stop

