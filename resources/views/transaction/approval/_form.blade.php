{{ csrf_field() }}
<div class="row ">
<div class="col-sm-7 border">
<div class="row ">
    <div class="col-sm-5 ">
        <div class="form-group  mb-sm-0">
            {!! Form::label('id','Request No ',['class'=>'label-info mb-sm-0']) !!}
            @if($button_caption=='Approve')
                {!! Form::text('id', $room_allocation->id,['class'=>'form-control form-control-sm','readonly'=>'true']) !!}
            @else
                {!! Form::text('id', $new_index,['class'=>'form-control form-control-sm text-gray-900','readonly'=>'true']) !!}
            @endif
            @if($errors->has('id'))
                <span class="alert-danger"> {{ $errors->first('id') }}</span>
            @endif
        </div>
    </div>


</div>
<div class="row">
    <div class="col-sm-6">
        <div class="form-group">
            {!! Form::label('room_id','Class Room',['class'=>'label-info mb-sm-0']) !!}
            {!! Form::text('room_id', $button_caption=='Approve'?$room_allocation->roomId->name:'',['class'=>'form-control form-control-sm text-gray-900 capacity_chk']) !!}

                <span id="span_room_id" class="alert-danger"> {{ $errors->first('room_id') }}</span>

            <input type="hidden" id="h_room_id" name="h_room_id" value="{{$button_caption=='Approve'?$room_allocation->room_id:''}}" />
        </div>
    </div>

    <div class="col-sm-6 btn-toolbar h-25 mt-3" role="toolbar">
        <div class="col-xs-4 col-sm-3 col-md-2 nopad text-center mr-4">
            <label class="image-checkbox chkcl">
                <img class="img-responsive" src="{{url('/image/whiteboard.png')}}"  />
                <input id="whiteboard" type="checkbox" name="facility[]" value="1" />
                <i class="fa fa-check hidden"></i>
            </label>

        </div>
        <div class="col-xs-4 col-sm-3 col-md-2 nopad text-center mr-4">
            <label class="image-checkbox chkcl">
                <img class="img-responsive" src="{{url('/image/projector1.png')}}" />
                <input id="projector" type="checkbox" name="facility[]" value="2" />
                <i class="fa fa-check hidden"></i>
            </label>

        </div>
        <div class="col-xs-4 col-sm-3 col-md-2 nopad text-center mr-4">
            <label class="image-checkbox chkcl">
                <img class="img-responsive" src="{{url('/image/speaker.png')}}" />
                <input id="speaker" type="checkbox" name="facility[]" value="3" />
                <i class="fa fa-check hidden"></i>
            </label>

        </div>
        <div class="col-xs-4 col-sm-3 col-md-2 nopad text-center mr-4">
            <label class="image-checkbox chkcl">
                <img class="img-responsive" src="{{url('/image/internet.png')}}" />
                <input id="internet" type="checkbox" name="facility[]" value="5" />
                <i class="fa fa-check hidden"></i>
            </label>

        </div>
        <div class="col-xs-4 col-sm-3 col-md-2 nopad text-center mr-4">
            <label class="image-checkbox chkcl">
                <img class="img-responsive" src="{{url('/image/wifi.png')}}" />
                <input id="wifi" type="checkbox" name="facility[]" value="4" />
                <i class="fa fa-check hidden"></i>
            </label>

        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-10">
        <div class="form-group">
            {!! Form::label('batch_id','Batch',['class'=>'label-info mb-sm-0']) !!}
            {!! Form::text('batch_id', $button_caption=='Approve'?$room_allocation->batchId->name:'',['class'=>'form-control form-control-sm text-gray-900 capacity_chk']) !!}

                <span id="span_batch_id" class="alert-danger"> {{ $errors->first('batch_id') }}</span>

            <input type="hidden" id="h_batch_id" name="h_batch_id" value="{{$button_caption=='Approve'?$room_allocation->batch_id:''}}"/>
        </div>
    </div>
    <input type="hidden" name="h_batch_cap" id="h_batch_cap"/>
    <input type="hidden" name="h_room_cap" id="h_room_cap"/>
</div>
<div class="row">
    <div class="col-sm-10">
        <div class="form-group">
            {!! Form::label('subject_id','Subject',['class'=>'label-info mb-sm-0']) !!}
            {!! Form::text('subject_id',  $button_caption=='Approve'?$room_allocation->subjectId->name:'',['class'=>'form-control form-control-sm text-gray-900']) !!}

                <span id="span_subject_id"  class="alert-danger"> {{ $errors->first('subject_id') }}</span>
            <input type="hidden" id="h_subject_id" name="h_subject_id" value="{{$button_caption=='Update'?$room_allocation->subject_id:''}}"/>
        </div>
    </div>

</div>
    <div class="border m-2">
    <div class="row ml-2">
        <div class="col-sm-6">
            <div class="form-group">
                <div class="date_from" data-provide="datepicker">
                {!! Form::label('from_date','From Date',['class'=>'label-info mb-sm-0 chg_schedule']) !!}
                {!! Form::text('from_date',$room_allocation->from_date,['class'=>'form-control form-control-sm text-gray-900 chg_schedule']) !!}
                <div class="input-group-addon">
                    <span class="glyphicon glyphicon-th"></span>
                </div>
            </div>
            </div>
        </div>
        <div class="col-sm-6 border-left">
            <div class="form-group">
                <div class="date_to" data-provide="datepicker">
                {!! Form::label('to_date','To Date',['class'=>'label-info mb-sm-0 chg_schedule']) !!}
                {!! Form::text('to_date',$room_allocation->to_date,['class'=>'form-control form-control-sm text-gray-900 chg_schedule']) !!}
                    <div class="input-group-addon">
                        <span class="glyphicon glyphicon-th"></span>
                    </div>
            </div>
            </div>
        </div>

    </div>
    <div class="row ml-2">
        <div class="col-sm-6">

                <div class="form-group ">
                    <div class='input-group' >
                        {!! Form::label('from_time','From Time',['class'=>'label-info mb-sm-0 ']) !!}
                        {!! Form::time('from_time', $room_allocation->from_time,['class'=>'form-control form-control-sm text-gray-900 time chg_schedule','id'=>'from_time']) !!}
                        <div class="input-group-addon">
                            <span class="glyphicon glyphicon-time"></span>
                        </div>
                    </div>

        </div>
        </div>


        <div class="col-sm-6 border-left">

            <div class="form-group">
                <div class='input-group' >
                    {!! Form::label('to_time','To Time',['class'=>'label-info mb-sm-0 chg_schedule']) !!}
                    {!! Form::time('to_time', $room_allocation->to_time,['class'=>'form-control form-control-sm text-gray-900 time chg_schedule','id'=>'to_time']) !!}
                    <div class="input-group-addon">
                        <span class="glyphicon glyphicon-time"></span>
                    </div>
                </div>

            </div>
        </div>
        </div>
    {{--</div>--}}

    {{--</div>--}}
    </div>
   <div class="row p-2">
    <div class="col-sm-12 border">
        <div class="input-group">
            <div class="col-sm-1 ">
            </div>
        <div class="col-sm-2 ">
            <div class='input-group bg-gray-500 text-gray-900' >
            {!! Form::checkbox('mon',$room_allocation->mon,false,['class'=>'form-control form-control-sm text-gray-900',$room_allocation->mon==1?'checked':'','id'=>'mon']) !!}
            {!! Form::label('mon','MON',['class'=>'label-info mb-sm-0']) !!}
            </div>
        </div>
        <div class="col-sm-2 ">
            <div class='input-group bg-gray-500 text-gray-900' >
            {!! Form::checkbox('tue',$room_allocation->tue,false,['class'=>'form-control form-control-sm text-gray-900',$room_allocation->tue==1?'checked':'','id'=>'tue']) !!}
            {!! Form::label('tue','TUE',['class'=>'label-info mb-sm-0']) !!}
            </div>
        </div>
        <div class="col-sm-2 ">
            <div class='input-group bg-gray-500 text-gray-900' >
            {!! Form::checkbox('wed',$room_allocation->wed,false,['class'=>'form-control form-control-sm text-gray-900',$room_allocation->wed==1?'checked':'','id'=>'wed']) !!}
            {!! Form::label('wed','WED',['class'=>'label-info mb-sm-0']) !!}
            </div>
        </div>
        <div class="col-sm-2 ">
            <div class='input-group bg-gray-500 text-gray-900' >
            {!! Form::checkbox('thu',$room_allocation->thu,false,['class'=>'form-control form-control-sm text-gray-900',$room_allocation->thu==1?'checked':'','id'=>'thu']) !!}
            {!! Form::label('thu','THU',['class'=>'label-info mb-sm-0']) !!}
            </div>
        </div>
        <div class="col-sm-2 ">
            <div class='input-group bg-gray-500 text-gray-900' >
            {!! Form::checkbox('fri',$room_allocation->fri,false,['class'=>'form-control form-control-sm text-gray-900',$room_allocation->fri==1?'checked':'','id'=>'fri']) !!}
            {!! Form::label('fri','FRI',['class'=>'label-info mb-sm-0']) !!}
            </div>
        </div>
            <div class="col-sm-1 ">
            </div>
        </div>
    </div>

</div>
    <div class="row p-2">
        <div class="col-sm-12 border">
            <div class="input-group">
                <div class="col-sm-4 ">
                </div>
                <div class="col-sm-2 ">
                    <div class='input-group bg-success text-white' >
                        {!! Form::checkbox('sat',$room_allocation->sat,false,['class'=>'form-control form-control-sm text-gray-900',$room_allocation->sat==1?'checked':'','id'=>'sat']) !!}
                        {!! Form::label('sat','SAT',['class'=>'label-info mb-sm-0']) !!}
                    </div>
                </div>
                <div class="col-sm-2 ">
                    <div class='input-group bg-danger text-white ' >
                        {!! Form::checkbox('sun',$room_allocation->sun,false,['class'=>'form-control form-control-sm text-gray-900',$room_allocation->sun==1?'checked':'','id'=>'sun']) !!}
                        {!! Form::label('sun','SUN',['class'=>'label-info mb-sm-0']) !!}
                    </div>
                </div>
                <div class="col-sm-4 ">
                    <button id="generate" type="button" class="btn btn-sm  btn-info float-right">Generate <i class="fa fa-arrow-alt-circle-right"></i></button>
                </div>
            </div>
        </div>

    </div>
</div>
<div class="col-sm-5">
    <div class="row ">
        <div class="col-sm-12 table-h-300">
            <table class="table-sm  table-bordered table-condensed table-striped" id="tbl_facility"
                   width="100%" cellspacing="0">
                <thead>
                <tr>
                    <th>Name</th>
                </tr>
                </thead>
                <tbody>
                    @if(isset($facilities))
                            @foreach($facilities as $list)
                            <tr>
                                <td>{{$list->facilityId->name}}</td>
                            </tr>

                            @endforeach
                    @endif
                </tbody>
            </table>
        </div>

    </div>
    <div class="row">
        <div class="col-sm-12">
            <table name="tbl_schedule" class="table-sm  table-bordered table-condensed table-striped" id="tbl_schedule"
                   width="100%" cellspacing="0">
                <thead>
                <tr class="bg-gray-300">
                    <th>#</th>
                    <th>Date</th>
                    <th>From Time</th>
                    <th>To Time</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @if(isset($schedule))
                    @foreach($schedule as $list)
                        <tr>
                            <td>{{++$i}}</td>
                            <td>{{$list->schedule_date}}</td>
                            <td><input readonly type="time" class="time" id="f_time_{{$i}}" name="f_time_{{$i}}" value="{{$list->from_time}}"></td>
                            <td><input readonly type="time" class="time" id="t_time_{{$i}}" name="t_time_{{$i}}" value="{{$list->to_time}}"></td>
                            <td  class="text-center"><input  type="checkbox" class="" id="chk_{{$i}}" name="chk_{{$i}}" {{$list->leave==0?'checked':''}}></td>
                            <input type="hidden" name="date_{{$i}}" id="date_{{$i}}" value="{{$list->schedule_date}}" />
                        </tr>

                    @endforeach
                @endif

                </tbody>
            </table>
        </div>
        <input type="hidden" name="schedule" id="schedule">
    </div>

</div>

</div>
<div class="float-right mb-2">

    <input type="hidden" id="temp_" name="temp_"  class="text-gray-900"/>
    <button type="button"  id="btnSave" class="btn  btn-success "><i class="fa fa-save"></i>{{$button_caption}}</button>
    <button type="button" class="btn  btn-info"><i class="fa fa-refresh"></i>Reset</button>

</div>


