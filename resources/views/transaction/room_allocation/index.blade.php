@extends('layouts.main')
@section('content')
    <div class="container-fluid">

        {{--editable Page--}}
{{--
        <div class="card-body">

        </div>--}}

        <!-- Page Heading -->
        <h6 class="h4 mb-2 text-gray-800">Class Room Allocation</h6>

        <!-- DataTales Example -->
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <div class="row">
                    <div class="col-md-1">
                    {!! Form::label('t_date','Date',['class'=>'label-info mb-sm-0']) !!}
                    </div>
                    <div class="col-md-3">
                    <div class="date" data-provide="datepicker">
                        {!! Form::text('t_date',$today,['class'=>'form-control form-control-sm','id'=>'t_date']) !!}
                        <div class="input-group-addon">
                            <span class="glyphicon glyphicon-th"></span>
                        </div>
                    </div>
                </div>
                    <div class="col-md-1">
                        {!! Form::label('building_id','Filtering',['class'=>'label-info mb-sm-0']) !!}
                    </div>
                    <div class="col-md-4">



                            {!! Form::select('building_id', $buildings,'',['class'=>'form-control form-control-sm','data-live-search'=>'true']) !!}

                        </div>

                    <div class="col-md-3">

                        <a id="btnAddNew" class="btn btn-sm btn-info add-new float-right" href="{{url('room_allocation/create')}}">
                            <i class="fa fa-plus"></i> Add New</a>

                    </div>
                </div>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table-sm table-bordered " id="grid_room" width="100%" cellspacing="0">
                        <thead>
                        <tr class="bg-gray-100">
                            <th class="col-sm-1 ">Class NO</th>
                            <th class="col-sm-2 ">session 1</th>
                            <th class="col-sm-2 ">session 2</th>
                            <th class="col-sm-2 ">session 3</th>
                            <th class="col-sm-2 ">session 4</th>
                        </tr>
                        </thead>
                        <tbody>
                        {{--@foreach($batches as $cllist)--}}
                            {{--<tr>--}}
                                {{--<td class="col-md-1">{{$cllist->id}}</td>--}}
                                {{--<td class="col-md-1">{{$cllist->name}}</td>--}}
                                {{--<td class="col-md-4">{{$cllist->degreeId->name}}</td>--}}
                                {{--<td class="col-md-1">--}}
                                    {{--<a type="button" href="{{route('batch.edit',$cllist->id)}}"--}}
                                       {{--class="btn btn-sm btn-warning -edit"><i class="fa fa-edit"></i></a>--}}
                                {{--</td>--}}
                            {{--</tr>--}}
                        {{--@endforeach--}}

                        </tbody>
                    </table>
                    {{--  <div>{{$Clients->links()}}</div>--}}
                </div>
            </div>
        </div>

    </div>
@stop
@section('alert')

@stop
@section('application_js')
    <script src="/js/application/room_allocation.js" type="text/javascript"></script>

@stop