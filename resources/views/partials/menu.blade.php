<!-- Sidebar -->
{{csrf_field()}}
<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
        <div class="sidebar-brand-icon rotate-n-15">

        </div>
        <div class="sidebar-brand-text mx-3">CRA</div>
    </a>

    <!-- Divider -->
    <hr class="sidebar-divider my-0">

    <!-- Nav Item - Dashboard -->
    <li class="nav-item">
        <a class="nav-link" href="/room_allocation">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Dashboard</span></a>
    </li>

    <!-- Divider -->
    <hr class="sidebar-divider">

    <!-- Heading -->
    <div class="sidebar-heading">
        Master Files
    </div>

    <!-- Nav Item - Pages Collapse Menu -->
    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true"
           aria-controls="collapseTwo">
            <i class="fas fa-fw fa-folder"></i>
            <span>Registration</span>
        </a>
        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <h6 class="collapse-header">Registration Components</h6>
                <div class="collapse-divider registration">
                    <h6 class="collapse-header">Group</h6>

                    {{--<a class="collapse-item" href="{{url('clients')}}">Client</a>
                    <a class="collapse-item" href="#">Client Approve</a>
                    <a class="collapse-item" href="#">Client Information</a>--}}
                </div>
                {{--<div class="collapse-divider">--}}
                    {{--<a class="collapse-item" href="{{url('department')}}">Department</a>--}}
                    {{--<a class="collapse-item" href="#">Floor</a>--}}
                {{--</div>--}}

            </div>
        </div>
    </li>

    <!-- Nav Item - Transaction Collapse Menu -->
    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseUtilities"
           aria-expanded="true" aria-controls="collapseUtilities">
            <i class="fas fa-fw fa-folder"></i>
            <span>Transaction</span>
        </a>
        <div id="collapseUtilities" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <div class="collapse-divider transaction">

                </div>

            </div>
        </div>
    </li>
    <div class="sidebar-heading">
        Payments
    </div>

    {{--<li class="nav-item">--}}
        {{--<a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapse_payment"--}}
           {{--aria-expanded="true" aria-controls="collapse_payment">--}}
            {{--<i class="fas fa-fw fa-coins"></i>--}}
            {{--<span>Payment</span>--}}
        {{--</a>--}}
        {{--<div id="collapse_payment" class="collapse" aria-labelledby="headingPayment" data-parent="#accordionSidebar">--}}
            {{--<div class="bg-white py-2 collapse-inner rounded">--}}
                {{--<div class="collapse-divider transaction_payments">--}}

                {{--</div>--}}

            {{--</div>--}}
        {{--</div>--}}
    {{--</li>--}}
    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapse_report"
           aria-expanded="true" aria-controls="collapse_report">
            <i class="fas fa-fw fa-address-book"></i>
            <span> Report</span>
        </a>
        <div id="collapse_report" class="collapse" aria-labelledby="headingPayment" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <div class="collapse-divider report">

                </div>

            </div>
        </div>
    </li>
    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsSetting" aria-expanded="true"
           aria-controls="collapsSetting">
            <i class="fas fa-fw fa-folder"></i>
            <span>Settings</span>
        </a>
        <div id="collapsSetting" class="collapse" aria-labelledby="collapsSetting" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <div class="collapse-divider setting">

                </div>

            </div>
        </div>
    </li>

    <!-- Divider -->
    <hr class="sidebar-divider">

    <!-- Heading -->


    <!-- Nav Item - Charts -->


    <!-- Divider -->
    <hr class="sidebar-divider d-none d-md-block">

    <!-- Sidebar Toggler (Sidebar) -->
    <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>




</ul>


<!-- End of Sidebar -->
