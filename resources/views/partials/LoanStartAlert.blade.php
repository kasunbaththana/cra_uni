
<div class="modal" id="{{'loan_start_confirmation_'.$loanList->index_no}}" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Loan Start Dialog</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{url('loan_start/store')}}" id="model_loan_start" method="post" enctype="multipart/form-data">
            <div class="modal-body">

                {{ csrf_field() }}
                <div class="row">
                    <div class="col-sm-4 mb-1">
                        {!! Form::label('md_application_reference_no','Ref No',['class'=>'label-info mb-sm-0']) !!}
                    </div>
                    <div class="col-sm-8 mb-1">
                        {!! Form::text('md_application_reference_no', $loanList->application_reference_no,['class'=>'form-control form-control-sm','readonly','id'=>'md_application_reference_no']) !!}
                    </div>

                    <div class="col-sm-4 mb-1">
                        {!! Form::label('md_transaction_date','Transaction Date',['class'=>'label-info mb-sm-0']) !!}
                    </div>
                    <div class="col-sm-8 mb-1">
                        {!! Form::text('md_transaction_date',Session::get('working_date'),['class'=>'form-control form-control-sm','readonly','id'=>'md_transaction_date']) !!}
                    </div>
                    @if($errors->has('md_transaction_date'))
                        <div class="col-sm-12 mb-1">
                        <span class="alert-danger"> {{ $errors->first('md_transaction_date') }}</span>
                        </div>
                    @endif

                    <div class="col-sm-4 mb-1">
                        {!! Form::label('agreement_no','Agreement No',['class'=>'label-info mb-sm-0']) !!}
                    </div>
                    <div class="col-sm-8 mb-1">
                        {!! Form::text('agreement_no', $loanList->agreement_no,['class'=>'form-control form-control-sm','id'=>'agreement_no']) !!}
                    </div>
                    @if($errors->has('agreement_no'))
                        <div class="col-sm-12 mb-1">
                        <span class="alert-danger"> {{ $errors->first('agreement_no') }}</span>
                        </div>
                    @endif
                    <div class="col-sm-4 mb-1">
                        {!! Form::label('md_loan_document_no','Document No',['class'=>'label-info mb-sm-0']) !!}
                    </div>
                    <div class="col-sm-8 mb-1">
                        {!! Form::text('md_loan_document_no', $loanList->loan_document_no,['class'=>'form-control form-control-sm']) !!}
                    </div>

                    <div class="col-sm-4 mb-1">
                        {!! Form::label('md_loan_date','First Due Date',['class'=>'label-info mb-sm-0']) !!}
                    </div>
                    <div class="col-sm-7 mb-1">
                        <div class="input-group date" data-provide="datepicker" id="datepicker">
                            {!! Form::text('md_loan_date', $loanList->loan_date,['class'=>'form-control form-control-sm','id'=>'md_loan_date']) !!}
                            <div class="input-group-addon">
                                <span class="glyphicon glyphicon-th"></span>
                            </div>
                        </div>


                    </div>
                    <div class="col-sm1 mb-1">
                        {!! Form::checkbox('md_due_date_check',null,false,['id'=>'md_due_date_check_'.$loanList->application_reference_no]) !!}
                    </div>
                    @if($errors->has('md_loan_date'))
                        <div class="col-sm-12 mb-1">
                            <span class="alert-danger"> {{ $errors->first('md_loan_date') }}</span>
                        </div>
                    @endif


                    <div class="col-sm-4 mb-1">
                        {!! Form::label('md_loan_officer','Loan Officer',['class'=>'label-info mb-sm-0']) !!}
                    </div>
                    <div class="col-sm-8 mb-1">
                        {!! Form::select('md_loan_officer',$employees,$loanList->loan_officer,['class'=>'form-control form-control-sm']) !!}
                    </div>
                    @if($errors->has('md_loan_officer'))
                        <div class="col-sm-12 mb-1">
                            <span class="alert-danger"> {{ $errors->first('md_loan_officer') }}</span>
                        </div>
                    @endif

                    <div class="col-sm-4 mb-1">
                        {!! Form::label('md_recovery_officer','Recovery Officer',['class'=>'label-info mb-sm-0']) !!}
                    </div>
                    <div class="col-sm-8 mb-1">
                        {!! Form::select('md_recovery_officer',$employees, $loanList->recovery_officer,['class'=>'form-control form-control-sm']) !!}
                    </div>
                    @if($errors->has('md_recovery_officer'))
                        <div class="col-sm-12 mb-1">
                            <span class="alert-danger"> {{ $errors->first('md_recovery_officer') }}</span>
                        </div>
                    @endif

                    <div class="col-sm-4 mb-1">
                        {!! Form::label('md_payment_term','Loan Type',['class'=>'label-info mb-sm-0']) !!}
                    </div>
                    <div class="col-sm-8 mb-1">
                        {!! Form::text('md_payment_term', $loanList->payment_term,['class'=>'form-control form-control-sm','readonly','id'=>'md_payment_term_'.$loanList->application_reference_no]) !!}
                    </div>



                </div>


            </div>

            <div class="modal-footer">
                <button type="submit" id="btn_save"
                   class="btn btn-sm btn-success"><i class="fa fa-save"></i> Yes</button>

                <a type="button" data-dismiss="modal"
                   class="btn btn-sm btn-info text-gray-100"> No</a>
               {{-- href="{{url('loan_application/loan/remove',$loanList->index_no)}}"--}}

            </div>
            </form>
        </div>
    </div>
</div>

