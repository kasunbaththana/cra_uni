<div class="modal" id="confirmation" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Modal title</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Are You Sure You Want To Remove !</p>
            </div>
            <div class="modal-footer">
                <a type="button" href="{{url('loan_application/loan/remove',$loanList->index_no)}}"
                   class="btn btn-sm btn-success">Yes</a>
                <a type="button" data-dismiss="modal"
                   class="btn btn-sm btn-info">No</a>

            </div>
        </div>
    </div>
</div>