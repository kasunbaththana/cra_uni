@extends('layouts.main')
@section('content')
    <div class="container">
        <h6 class="h6 mb-2 font-weight-bold text-primary">{{$name}}</h6>
        <div class="card o-hidden border-0 shadow-lg my-1 mt-0">
            <div class="card-body">
                <!-- Nested Row within Card Body -->
                {{--{!! Form::open( ['url' => [$type.'/store'] ,'id'=>'report']) !!}--}}
                <div class="row mb-0 ">

                    <div class="col-sm-12 ">

                            <div class="table-responsive receipt-custom-scrollbar">
                                    <div class="row ml-1">

                                    @foreach($reports as $list)
                                        {{--<div class="col-sm-4 p-2 btn-group radio-group-toggle" data-toggle="buttons">--}}
                                            {{--<label class="btn bg-gray-600 text-gray-100 active">--}}
                                                {{--<div class="form-check float-left">--}}
                                                {{--<input class="form-check-input rdo_report" type="radio" name="options" title="{{$list->module_path}}" id="{{$list->module_id}}" autocomplete="off" > {{$list->module_name}}--}}
                                                {{--</div>--}}
                                            {{--</label>--}}
                                        {{--</div>--}}

                                                <div class="custom-radios col-sm-4 p-1  btn-group radio-group-toggle" >
                                                <div>
                                                    <input class="ss" type="radio" title="{{$list->reportId->path}}" id="{{$list->reportId->id}}" name="options" value="{{$list->reportId->id}}" >
                                                    <label class="btn text-gray-600 active" for="{{$list->reportId->id}}">
                                                      <span class="ss" >
                                                      </span>
                                                        {{$list->reportId->name}}
                                                    </label>

                                                </div>
                                                </div>
                                    @endforeach
                                        </div>

                        </div>

                        @include('commonForm.ReportParameter')
                        {{--@include('commonForm.searchModelReport')--}}
                    </div>
                </div>
                {{--{!! Form::close() !!}--}}
            </div>
        </div>

    </div>
@stop

@section('alert')

@stop

@section('application_js')
    <script src="{{asset('/js/application/report.js')}}" type="text/javascript"></script>
    <script src="{{asset('/js/application/searchModel.js')}}" type="text/javascript"></script>


@stop

