<div class="card-body">
    <div class="table-responsive">
        <table class="table-sm table-bordered p-2 move_table" id="m_table" width="100%" cellspacing="2">
            <thead>
            <tr class="bg-gray-300">
                <th>ID</th>
                <th>Name</th>
                <th>Action</th>
            </tr>
            </thead>

            <tbody>
            @foreach($users_list as $cllist)
                <tr>
                    <td class="col-md-1">{{$cllist->id}}</td>
                    <td class="col-md-2">{{$cllist->name}}</td>
                    <td class="col-md-1">
                    </td>
                </tr>
            @endforeach

            </tbody>
        </table>
        {{--  <div>{{$Clients->links()}}</div>--}}
    </div>
</div>