{{ csrf_field() }}
<div class="row ">
    <div class="col-sm-6 ">
        <div class="form-group  mb-sm-0">
            {!! Form::label('id','Code ',['class'=>'label-info mb-sm-0']) !!}
            {{--<input type="text" class="form-control form-control-sm" name="code" placeholder="Code" >--}}
            @if($button_caption=='Update')
                {!! Form::text('id', $floors->id,['class'=>'form-control form-control-sm','readonly'=>'true']) !!}
            @else
                {!! Form::text('id', $new_index,['class'=>'form-control form-control-sm','readonly'=>'true']) !!}
            @endif
            @if($errors->has('id'))
                <span class="alert-danger"> {{ $errors->first('id') }}</span>
            @endif
        </div>
    </div>

</div>
<div class="col-sm-0">
    <div class="form-group">
        {!! Form::label('name','Name',['class'=>'label-info mb-sm-0']) !!}
        {{--<input type="text" class="form-control form-control-sm" name="name" placeholder="Name With Initials">--}}
        {!! Form::text('name', $floors->name,['class'=>'form-control form-control-sm']) !!}
        @if($errors->has('name'))
            <span class="alert-danger"> {{ $errors->first('name') }}</span>
        @endif
    </div>
</div>
<div class="col-sm-0">
    <div class="form-group">
        {!! Form::label('building_id','Building',['class'=>'label-info mb-sm-0']) !!}
        {{--<input type="text" class="form-control form-control-sm" name="name" placeholder="Name With Initials">--}}
        {!! Form::select('building_id', $buildings,$floors->building_id,['class'=>'form-control form-control-sm','data-live-search'=>'true']) !!}
        @if($errors->has('building_id'))
            <span class="alert-danger"> {{ $errors->first('building_id') }}</span>
        @endif
    </div>
</div>



<div class="float-right mb-2">

    <button type="submit"  class="btn  btn-success "><i class="fa fa-save"></i>{{$button_caption}}</button>
    <button type="button" class="btn  btn-info"><i class="fa fa-refresh"></i>Reset</button>

</div>


