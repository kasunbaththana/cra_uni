{{ csrf_field() }}
<div class="row ">
    <div class="col-sm-6 ">
        <div class="form-group  mb-sm-0">
            {!! Form::label('id','Code ',['class'=>'label-info mb-sm-0']) !!}
            @if($button_caption=='Update')
                {!! Form::text('id', $halls->id,['class'=>'form-control form-control-sm','readonly'=>'true']) !!}
            @else
                {!! Form::text('id', $new_index,['class'=>'form-control form-control-sm','readonly'=>'true']) !!}
            @endif
            @if($errors->has('id'))
                <span class="alert-danger"> {{ $errors->first('id') }}</span>
            @endif
        </div>
    </div>

</div>
<div class="col-sm-0">
    <div class="form-group">
        {!! Form::label('name','Name',['class'=>'label-info mb-sm-0']) !!}
        {!! Form::text('name', $halls->name,['class'=>'form-control form-control-sm']) !!}
        @if($errors->has('name'))
            <span class="alert-danger"> {{ $errors->first('name') }}</span>
        @endif
    </div>
</div>
<div class="col-sm-0">
    <div class="form-group">
        {!! Form::label('floor_id','Floor',['class'=>'label-info mb-sm-0']) !!}
        {!! Form::select('floor_id', $floors,$halls->floor_id,['class'=>'form-control form-control-sm','data-live-search'=>'true']) !!}
        @if($errors->has('floor_id'))
            <span class="alert-danger"> {{ $errors->first('floor_id') }}</span>
        @endif
    </div>
</div>

<div class="col-sm-5">
    <div class="form-group ml-4">
        {!! Form::checkbox('is_exam_hall',$halls->is_exam_hall,($halls->is_exam_hall),['class'=>'form-check-input']) !!}
        {!! Form::label('is_exam_hall','Is Exam Hall',['class'=>'label-info mb-sm-0']) !!}
        @if($errors->has('is_exam_hall'))
            <span class="alert-danger"> {{ $errors->first('is_exam_hall') }}</span>
        @endif
    </div>
</div>



<div class="float-right mb-2">

    <button type="submit"  class="btn  btn-success "><i class="fa fa-save"></i>{{$button_caption}}</button>
    <button type="button" class="btn  btn-info"><i class="fa fa-refresh"></i>Reset</button>

</div>


