{{ csrf_field() }}
<div class="row ">
    <div class="col-sm-6 ">
        <div class="form-group  mb-sm-0">
            {!! Form::label('id','Code ',['class'=>'label-info mb-sm-0']) !!}
            @if($button_caption=='Update')
                {!! Form::text('id', $batches->id,['class'=>'form-control form-control-sm','readonly'=>'true']) !!}
            @else
                {!! Form::text('id', $new_index,['class'=>'form-control form-control-sm','readonly'=>'true']) !!}
            @endif
            @if($errors->has('id'))
                <span class="alert-danger"> {{ $errors->first('id') }}</span>
            @endif
        </div>
    </div>

</div>
<div class="col-sm-0">
    <div class="form-group">
        {!! Form::label('name','Name',['class'=>'label-info mb-sm-0']) !!}
        {!! Form::text('name', $batches->name,['class'=>'form-control form-control-sm']) !!}
        @if($errors->has('name'))
            <span class="alert-danger"> {{ $errors->first('name') }}</span>
        @endif
    </div>
</div>
<div class="col-sm-0">
    <div class="form-group">
        {!! Form::label('degree_id','Degree',['class'=>'label-info mb-sm-0']) !!}
        {!! Form::select('degree_id', $degrees,$batches->degree_id,['class'=>'form-control form-control-sm','data-live-search'=>'true']) !!}
        @if($errors->has('degree_id'))
            <span class="alert-danger"> {{ $errors->first('degree_id') }}</span>
        @endif
    </div>
</div>
<div class="row">
    <div class="col-sm-6">
        <div class="form-group">
            <div class="date" data-provide="datepicker">
            {!! Form::label('year','Year',['class'=>'label-info mb-sm-0']) !!}
                {!! Form::text('year', $batches->year,['class'=>'form-control form-control-sm','id'=>'year']) !!}
                <div class="input-group-addon">
                    <span class="glyphicon glyphicon-th"></span>
                </div>
        </div>
    </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            {!! Form::label('type','Type',['class'=>'label-info mb-sm-0']) !!}
            {!! Form::select('type', $types,$batches->type,['class'=>'form-control form-control-sm','data-live-search'=>'true']) !!}
            @if($errors->has('type'))
                <span class="alert-danger"> {{ $errors->first('type') }}</span>
            @endif
        </div>
        <input type="hidden" id="prefix" name="prefix" value="{{$prefix}}"/>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            {!! Form::label('capacity','Capacity',['class'=>'label-info mb-sm-0']) !!}
            {!! Form::number('capacity', $batches->capacity,['class'=>'form-control form-control-sm text-md-right','min'=>'1' ,'max'=>'1000']) !!}
            @if($errors->has('capacity'))
                <span class="alert-danger"> {{ $errors->first('capacity') }}</span>
            @endif
        </div>
    </div>
</div>
<div class="row">

</div>

<br />


<div class="float-right mb-2">

    <button type="button"  id="btnSave" class="btn  btn-success "><i class="fa fa-save"></i>{{$button_caption}}</button>
    <button type="button" class="btn  btn-info"><i class="fa fa-refresh"></i>Reset</button>

</div>


