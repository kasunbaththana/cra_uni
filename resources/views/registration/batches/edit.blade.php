@extends('layouts.main')
@section('content')
    <div class="container-fluid">

        <div class="card o-hidden border-0 shadow-lg my-1">
            <div class="card-body p-0">
                <!-- Nested Row within Card Body -->
                {!! Form::model($batches,['url'=>'batch/edit','enctype'=>"multipart/form-data", 'method' => 'put','id'=>'batch']) !!}
                <div class="row">
                    <div class="col-lg-5">
                        <div class="p-3">
                            <div class="text-center">
                                <h1 class="h4 text-gray-900 mb-4">Edit Batch</h1>
                            </div>


                            @include('registration.batches._form',['button_caption'=>'Update'])

                            @include('commonForm.AddItem')

                        </div>
                    </div>
                    <input type="hidden" name="tmp_table" id="tmp_table"/>
                    <div class="container pt-4 col-lg-7">
                        <div class="table-responsive">
                            <table class="table-sm table-bordered" id="subjects_" style="width: 100%;">
                                <thead>
                                <tr>
                                    <th class="text-center col-sm-1">Year</th>
                                    <th class="text-center col-sm-1">Semester</th>
                                    <th class="text-center col-sm-6">Subject</th>
                                    <th class="text-center col-sm-1">Duration</th>
                                    <th class="text-center col-sm-1">Action</th>
                                </tr>
                                </thead>
                                <tbody id="subjects_tbody">
                                @foreach($batches_subject as  $key=>$cllist)
                                <tr>
                                <td>
                                    <input type="number" class="form-control form-control-sm" border="0"  id="tbl_year{{$key}}"
                                     name="tbl_year{{$key}}" value="{{$cllist->year}}" />
                                    </td>
                                <td>
                                    <input type="number" class="form-control form-control-sm" border="0"  id="tbl_sem{{$key}}"
                                     name="tbl_sem{{$key}}" value="{{$cllist->semester}}" /></td>
                                <td>
                                    <input class="form-control form-control-sm" border="0" readonly id="tbl_subject{{$key}}"
                                    name="tbl_subject{{$key}}" value="{{$cllist->subjectId->name}}"/>
                                    <input type="hidden" name="hid_subj{{$key}}" id="hid_subj{{$key}}" value="{{$cllist->subject_id}}" />
                                    </td>
                                <td>
                                    <input type="number" class="form-control form-control-sm" border="0"  id="tbl_duration{{$key}}"
                                     name="tbl_duration{{$key}}"  value="{{$cllist->subject_duration}}" />
                                    </td>
                                <td class="text-center"> <a type="button"  class="btn btn-danger btn-circle btn-sm text-gray-100">
                                        <i class="fas fa-trash"></i></a>
                                </td>
                                </tr>

                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="float-right">
                            <a id="btnAddItem" class="btn btn-sm btn-info add-new text-gray-100"  data-target="#myModal" data-toggle="modal"><i
                                        class="fa fa-plus "></i> Add Option</a>
                        </div>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>

    </div>
@stop
@section('alert')

@stop
@section('application_js')
    <script src="/js/application/batch.js" type="text/javascript"></script>

@stop

