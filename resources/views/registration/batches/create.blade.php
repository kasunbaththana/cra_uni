@extends('layouts.main')
@section('content')
    {{-- <div class="container">--}}
    <div class="container-fluid">

        <div class="card o-hidden border-0 shadow-lg my-1">
            <div class="card-body p-0">
                <!-- Nested Row within Card Body -->
                {!! Form::model($batches, ['url' => ['batch/save'],'enctype'=>"multipart/form-data",'id'=>'batch']) !!}
                <div class="row">
                    <div class="col-lg-5">
                        <div class="p-3">
                            <div class="text-center">
                                <h1 class="h4 text-gray-900 mb-4">Batch</h1>
                            </div>

                            @include('registration.batches._form',['button_caption'=>'Save'])
                            @include('commonForm.AddItem')
                        </div>

                    </div>
                    <input type="hidden" name="tmp_table" id="tmp_table"/>
                    <div class="container pt-4 col-lg-7">
                        <div class="table-responsive">
                            <table class="table-sm table-bordered" id="subjects_" style="width: 100%;">
                                <thead>
                                <tr>
                                    <th class="text-center col-sm-1">Year</th>
                                    <th class="text-center col-sm-1">Semester</th>
                                    <th class="text-center col-sm-6">Subject</th>
                                    <th class="text-center col-sm-1">Duration</th>
                                    <th class="text-center col-sm-1">Action</th>
                                </tr>
                                </thead>
                                <tbody id="subjects_tbody">

                                </tbody>
                            </table>
                        </div>
                        <div class="float-right">
                            <a id="btnAddItem" class="btn btn-sm btn-info add-new text-gray-100"  data-target="#myModal" data-toggle="modal"><i
                                        class="fa fa-plus "></i> Add Option</a>
                        </div>
                    </div>


                </div>
                {!! Form::close() !!}
            </div>
        </div>

    </div>
@stop

@section('alert')

@stop
@section('application_js')
    <script src="/js/application/batch.js" type="text/javascript"></script>

@stop

