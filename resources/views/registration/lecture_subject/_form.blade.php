{{ csrf_field() }}
<div class="row">
    <div class="col-sm-9">
        <div class="form-group">
            {!! Form::label('batch_id','Batch Name',['class'=>'label-info mb-sm-0']) !!}
            {!! Form::select('batch_id',$batches, $subject->batches->name,['class'=>'form-control form-control-sm']) !!}
            @if($errors->has('batch_id'))
                <span class="alert-danger"> {{ $errors->first('batch_id') }}</span>
            @endif
        </div>
    </div>

</div>
<div class="row">
    <div class="col-sm-9">
        <div class="form-group">
            {!! Form::label('subject_id','Subject Name',['class'=>'label-info mb-sm-0']) !!}
            {!! Form::select('subject_id',$subjects,$subject->subjectId->name,['class'=>'form-control form-control-sm']) !!}
            @if($errors->has('subject_id'))
                <span class="alert-danger"> {{ $errors->first('subject_id') }}</span>
            @endif
        </div>
    </div>

</div>

<div class="float-left mb-2">
    <input type="hidden" name="as_id" id="as_id" value="{{$subject->id}}"/>
    <button type="submit"  class="btn  btn-success "><i class="fa fa-save"></i>{{$button_caption}}</button>
    <button type="button" class="btn  btn-info"><i class="fa fa-refresh"></i>Reset</button>

</div>


