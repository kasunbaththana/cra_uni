@extends('layouts.main')
@section('content')
    <div class="container-fluid">

        {{--editable Page--}}
{{--
        <div class="card-body">

        </div>--}}

        <!-- Page Heading -->
        <h6 class="h4 mb-2 text-gray-800">Assign Lecturer to Subject</h6>

        <!-- DataTales Example -->
        <div class="card shadow mb-4">
            <div class="card-header py-3">

            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table-sm table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                        <tr>
                            <th>Batch name</th>
                            <th>Subject Name</th>
                            <th>Year</th>
                            <th>Semester</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tfoot>
                        <tr>
                            <th>Batch name</th>
                            <th>Subject Name</th>
                            <th>Year</th>
                            <th>Semester</th>
                            <th>Action</th>

                        </tr>
                        </tfoot>
                        <tbody>
                        @foreach($degrees as $cllist)
                            <tr>
                                <td class="col-md-2">{{$cllist->batches->name}}</td>
                                <td class="col-md-3">{{$cllist->subjectId->name}}</td>
                                <td class="col-md-1">{{$cllist->year}}</td>
                                <td class="col-md-1">{{$cllist->semester}}</td>

                                <td class="col-md-1">
                                    <a type="button" href="{{route('lecture_subject.edit',$cllist->id)}}"
                                       class="btn btn-sm btn-warning -edit"><i class="fa fa-edit"></i></a>
                                </td>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>
                    {{--  <div>{{$Clients->links()}}</div>--}}
                </div>
            </div>
        </div>

    </div>
@stop
