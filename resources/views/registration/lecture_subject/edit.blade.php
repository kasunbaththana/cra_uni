@extends('layouts.main')
@section('content')
    <div class="container">

        <div class="card o-hidden border-0 shadow-lg my-1">
            <div class="card-body p-0">
                <!-- Nested Row within Card Body -->
                {!! Form::model($subject,['url'=>'lecture_subject/update', 'method' => 'put']) !!}
                <div class="row">
                    <div class="col-lg-7">
                        <div class="p-5">
                            <div class="text-center">
                                <h1 class="h4 text-gray-900 mb-4">Assign Lecturer to Subject</h1>
                            </div>

                            @include('registration.lecture_subject._form',['button_caption'=>'Assign'])

                        </div>
                    </div>
                    <div class="col-lg-5 mt-5">


                            <div class="card shadow mb-0">

                                @include('transaction.exam_allocation.other.add_new_item')

                            </div>



                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>

    </div>
@stop
@section('alert')

@stop
@section('application_js')
    <script src="/js/application/batch_lectuer.js" type="text/javascript"></script>

@stop

