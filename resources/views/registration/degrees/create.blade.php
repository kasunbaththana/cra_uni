@extends('layouts.main')
@section('content')
    {{-- <div class="container">--}}
    <div class="container-fluid">

        <div class="card o-hidden border-0 shadow-lg my-1">
            <div class="card-body p-0">
                <!-- Nested Row within Card Body -->
                {!! Form::model($degrees, ['url' => ['degree/save'],'enctype'=>"multipart/form-data"]) !!}
                <div class="row">
                    <div class="col-lg-7">
                        <div class="p-3">
                            <div class="text-center">
                                <h1 class="h4 text-gray-900 mb-4">Create a Degree</h1>
                            </div>

                            @include('registration.degrees._form',['button_caption'=>'Save'])

                        </div>
                    </div>
                    <div class="container col-lg-5 pt-4">
                        <div class="table-responsive">
                            <table id="tbl_subject" class="table-sm table-bordered" style="width: 100%;">
                                <thead>
                                <tr>
                                    <th class="text-center">Subject</th>
                                    <th class="text-center">Action</th>
                                </tr>
                                </thead>
                                <tbody id="subjects_tbody">
                                @foreach($subject as $key=>$cllist)
                                    <tr id="R{{$key}}}">
                                        <td class="row-index text-center">
                                            {{--<select class="form-control form-control-sm" name="subject{{$key}}" id="subject{{$key}}">--}}
                                            {{--${list}--}}
                                            {{--</select>--}}
                                            {!! Form::select('subject'.$key,$subject_d, $cllist->subject_id,['class'=>'form-control form-control-sm subject_','id'=>'subject'.$key]) !!}
                                        </td>
                                        <td class="text-center">
                                            <button class="btn btn-sm btn-danger remove" type="button">Remove</button>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        <button class="btn btn-sm btn-primary"
                                id="subjects_addBtn" type="button">
                            Add subject
                        </button>
                    </div>


                </div>
                {!! Form::close() !!}
            </div>
        </div>

    </div>
@stop

@section('alert')

@stop
@section('application_js')
   {{-- <script src="/js/application/register.js" type="text/javascript"></script>--}}
   <script src="/js/application/degree.js" type="text/javascript"></script>

@stop

