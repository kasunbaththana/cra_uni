{{ csrf_field() }}
<div class="row ">
    <div class="col-sm-6 ">
        <div class="form-group  mb-sm-0">
            {!! Form::label('id','Code ',['class'=>'label-info mb-sm-0']) !!}
            @if($button_caption=='Update')
                {!! Form::text('id', $degrees->id,['class'=>'form-control form-control-sm','readonly'=>'true']) !!}
            @else
                {!! Form::text('id', $new_index,['class'=>'form-control form-control-sm','readonly'=>'true']) !!}
            @endif
            @if($errors->has('id'))
                <span class="alert-danger"> {{ $errors->first('id') }}</span>
            @endif
        </div>
    </div>

</div>
<div class="col-sm-0">
    <div class="form-group">
        {!! Form::label('name','Name',['class'=>'label-info mb-sm-0']) !!}
        {!! Form::text('name', $degrees->name,['class'=>'form-control form-control-sm']) !!}
        @if($errors->has('name'))
            <span class="alert-danger"> {{ $errors->first('name') }}</span>
        @endif
    </div>
</div>
<div class="col-sm-0">
    <div class="form-group">
        {!! Form::label('department_id','Department',['class'=>'label-info mb-sm-0']) !!}
        {!! Form::select('department_id',$departments, $degrees->department_id,['class'=>'form-control form-control-sm']) !!}
        @if($errors->has('department_id'))
            <span class="alert-danger"> {{ $errors->first('department_id') }}</span>
        @endif
    </div>
</div>
<div class="col-sm-0">
    <div class="form-group">
        {!! Form::label('prefix','Prefix',['class'=>'label-info mb-sm-0']) !!}
        {!! Form::text('prefix', $degrees->prefix,['class'=>'form-control form-control-sm']) !!}
        @if($errors->has('prefix'))
            <span class="alert-danger"> {{ $errors->first('prefix') }}</span>
        @endif
    </div>
</div>
<div class="row">
    <div class="col-sm-1">
    </div>
    <div class="col-sm-5">
        <div class="form-group">
            {!! Form::checkbox('is_weekend',$degrees->is_weekend,($degrees->is_weekend),['class'=>'form-check-input']) !!}
            {!! Form::label('is_weekend','Weekend',['class'=>'label-info mb-sm-0']) !!}
            @if($errors->has('is_weekend'))
                <span class="alert-danger"> {{ $errors->first('is_weekend') }}</span>
            @endif
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            {!! Form::checkbox('is_weekday',$degrees->is_weekday,($degrees->is_weekday),['class'=>'form-check-input']) !!}
            {!! Form::label('is_weekday','Weekday',['class'=>'label-info mb-sm-0']) !!}
            @if($errors->has('is_weekday'))
                <span class="alert-danger"> {{ $errors->first('is_weekday') }}</span>
            @endif
        </div>
    </div>
</div>
<div class="row">

</div>

<div class="float-right mb-2">

    <button type="submit"  class="btn  btn-success "><i class="fa fa-save"></i>{{$button_caption}}</button>
    <button type="button" class="btn  btn-info"><i class="fa fa-refresh"></i>Reset</button>

</div>


