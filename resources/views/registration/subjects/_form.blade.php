{{ csrf_field() }}
<div class="row ">
    <div class="col-sm-6 ">
        <div class="form-group  mb-sm-0">
            {!! Form::label('id','ID',['class'=>'label-info mb-sm-0']) !!}
            @if($button_caption=='Update')
                {!! Form::text('id', $subjects->id,['class'=>'form-control form-control-sm','readonly'=>'true']) !!}
            @else
                {!! Form::text('id', $new_index,['class'=>'form-control form-control-sm','readonly'=>'true']) !!}
            @endif
            @if($errors->has('id'))
                <span class="alert-danger"> {{ $errors->first('id') }}</span>
            @endif
        </div>
    </div>

    {{--<div class="col-sm-6">--}}
        {{--<div class="form-group">--}}
            {{--{!! Form::label('prefix','Prefix',['class'=>'label-info mb-sm-0']) !!}--}}
            {{--{!! Form::text('prefix', $subjects->prefix,['class'=>'form-control form-control-sm']) !!}--}}
            {{--@if($errors->has('prefix'))--}}
                {{--<span class="alert-danger"> {{ $errors->first('prefix') }}</span>--}}
            {{--@endif--}}
        {{--</div>--}}
    {{--</div>--}}
</div>

<div class="row">
    <div class="col-sm-9">
        <div class="form-group">
            {!! Form::label('name','Name',['class'=>'label-info mb-sm-0']) !!}
            {!! Form::text('name', $subjects->name,['class'=>'form-control form-control-sm']) !!}
            @if($errors->has('name'))
                <span class="alert-danger"> {{ $errors->first('name') }}</span>
            @endif
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-9">
    <div class="form-group">
        {!! Form::label('description','Description',['class'=>'label-info mb-sm-0']) !!}
        {!! Form::text('description', $subjects->description,['class'=>'form-control form-control-sm']) !!}
        @if($errors->has('description'))
            <span class="alert-danger"> {{ $errors->first('description') }}</span>
        @endif
    </div>
    </div>
</div>

<div class="float-right mb-2">

    <button type="submit"  class="btn  btn-success "><i class="fa fa-save"></i>{{$button_caption}}</button>
    <button type="button" class="btn  btn-info"><i class="fa fa-refresh"></i>Reset</button>

</div>


