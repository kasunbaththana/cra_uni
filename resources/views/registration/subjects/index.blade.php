@extends('layouts.main')
@section('content')
    <div class="container-fluid">

        {{--editable Page--}}
{{--
        <div class="card-body">

        </div>--}}

        <!-- Page Heading -->
        <h6 class="h4 mb-2 text-gray-800">Subjects</h6>

        <!-- DataTales Example -->
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <div class="float-right">

                    <a id="btnAddNew" class="btn btn-sm btn-info add-new" href="{{url('subjects/create')}}">
                        <i class="fa fa-plus"></i> Add New</a>

                </div>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table-sm table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Prefix</th>
                            <th>Name</th>
                            <th>Description</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tfoot>
                        <tr>
                            <th>ID</th>
                            <th>Prefix</th>
                            <th>Name</th>
                            <th>Description</th>
                            <th>Action</th>

                        </tr>
                        </tfoot>
                        <tbody>
                        @foreach($subjects as $subject)
                            <tr>
                                <td class="col-md-1">{{$subject->id}}</td>
                                <td class="col-md-2">{{$subject->prefix}}</td>
                                <td class="col-md-2">{{$subject->name}}</td>
                                <td class="col-md-2">{{$subject->description}}</td>

                                <td class="col-md-1">
                                    <a type="button" href="{{route('subjects.edit',$subject->id)}}"
                                       class="btn btn-sm btn-warning -edit"><i class="fa fa-edit"></i></a>
                                </td>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>
                    {{--  <div>{{$Clients->links()}}</div>--}}
                </div>
            </div>
        </div>

    </div>
@stop
