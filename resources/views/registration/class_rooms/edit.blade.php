@extends('layouts.main')
@section('content')
    <div class="container-fluid">

        <div class="card o-hidden border-0 shadow-lg my-1">
            <div class="card-body p-0">
                <!-- Nested Row within Card Body -->
                {!! Form::model($floors,['url'=>'class_room/edit','enctype'=>"multipart/form-data", 'method' => 'put']) !!}
                <div class="row">
                    <div class="col-lg-7">
                        <div class="p-5">
                            <div class="text-center">
                                <h1 class="h4 text-gray-900 mb-4">Edit Class Room</h1>
                            </div>

                            @include('registration.class_rooms._form',['button_caption'=>'Update'])

                        </div>
                    </div>
                    <div class="col-sm-5 mt-lg-5">
                        <div class="card shadow mb-0">

                            @include('registration.class_rooms.other.add_new_item')

                        </div>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>

    </div>
@stop
@section('alert')

@stop
@section('application_js')
    <script src="/js/application/class_room.js" type="text/javascript"></script>

@stop

