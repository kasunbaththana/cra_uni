{{ csrf_field() }}


<div class="row ">
    <div class="col-sm-6 ">
        <div class="form-group  mb-sm-0">
            {!! Form::label('id','Code ',['class'=>'label-info mb-sm-0']) !!}
            @if($button_caption=='Update')
                {!! Form::text('id', $class_rooms->id,['class'=>'form-control form-control-sm','readonly'=>'true']) !!}
            @else
                {!! Form::text('id', $new_index,['class'=>'form-control form-control-sm','readonly'=>'true']) !!}
            @endif
            @if($errors->has('id'))
                <span class="alert-danger"> {{ $errors->first('id') }}</span>
            @endif
        </div>
    </div>

</div>
<div class="col-sm-0">
    <div class="form-group">
        {!! Form::label('name','Name',['class'=>'label-info mb-sm-0']) !!}
        {!! Form::text('name', $class_rooms->name,['class'=>'form-control form-control-sm']) !!}
        @if($errors->has('name'))
            <span class="alert-danger"> {{ $errors->first('name') }}</span>
        @endif
    </div>
</div>

<div class="row border">
<div class="col-sm-4">
    <div class="form-group">
        {!! Form::label('building_id','Building',['class'=>'label-info mb-sm-0']) !!}
        {!! Form::select('building_id', $buildings,$class_rooms->hallId==null?null:$class_rooms->hallId->floorId->buildingId->id,['class'=>'form-control form-control-sm','data-live-search'=>'true']) !!}
        @if($errors->has('building_id'))
            <span class="alert-danger"> {{ $errors->first('building_id') }}</span>
        @endif
    </div>
</div>


<div class="col-sm-4">
    <div class="form-group">
        {!! Form::label('floor_id','Floor',['class'=>'label-info mb-sm-0']) !!}
        {!! Form::select('floor_id', $floors,$class_rooms->hallId==null?null:$class_rooms->hallId->floorId->id,['class'=>'form-control form-control-sm','data-live-search'=>'true']) !!}
        @if($errors->has('floor_id'))
            <span class="alert-danger"> {{ $errors->first('floor_id') }}</span>
        @endif
    </div>
</div>
<div class="col-sm-4">
    <div class="form-group">
        {!! Form::label('hall_id','Hall',['class'=>'label-info mb-sm-0']) !!}
        {!! Form::select('hall_id', $halls,$class_rooms->hallId==null?null:$class_rooms->hall_id,['class'=>'form-control form-control-sm','data-live-search'=>'true']) !!}
        @if($errors->has('hall_id'))
            <span class="alert-danger"> {{ $errors->first('hall_id') }}</span>
        @endif
    </div>
</div>
</div>
<div class="col-sm-0">
    <div class="form-group">
        {!! Form::label('type','Type',['class'=>'label-info mb-sm-0']) !!}
        {!! Form::select('type', $types,$class_rooms->type,['class'=>'form-control form-control-sm','data-live-search'=>'true']) !!}
        @if($errors->has('type'))
            <span class="alert-danger"> {{ $errors->first('type') }}</span>
        @endif
    </div>
</div>
<div class="col-sm-0">
    <div class="form-group">
        {!! Form::label('capacity','Capacity',['class'=>'label-info mb-sm-0']) !!}
        {!! Form::number('capacity', $class_rooms->capacity==null?0:$class_rooms->capacity,['class'=>'form-control form-control-sm text-md-left']) !!}
        @if($errors->has('capacity'))
            <span class="alert-danger"> {{ $errors->first('capacity') }}</span>
        @endif
    </div>
</div>
<div class="col-sm-0 d-none">
    <div class="form-group">
        {!! Form::label('location','Location',['class'=>'label-info mb-sm-0']) !!}
        {!! Form::text('location', $class_rooms->location,['class'=>'form-control form-control-sm']) !!}
        @if($errors->has('location'))
            <span class="alert-danger"> {{ $errors->first('location') }}</span>
        @endif
    </div>
</div>

<div class="float-right mb-2">

    <button type="submit"  class="btn  btn-success "><i class="fa fa-save"></i>{{$button_caption}}</button>
    <button type="button" class="btn  btn-info"><i class="fa fa-refresh"></i>Reset</button>

</div>


