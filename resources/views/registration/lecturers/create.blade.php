@extends('layouts.main')
@section('content')
    {{-- <div class="container">--}}
    <div class="container-fluid">

        <div class="card o-hidden border-0 shadow-lg my-1">
            <div class="card-body p-0">
                <!-- Nested Row within Card Body -->
                {!! Form::model($lecturers, ['url' => ['lecturers/save'], 'enctype'=>"multipart/form-data","id"=>"lecture"]) !!}
                <div class="row">
                    <div class="col-lg-7">
                        <div class="p-3">
                            <div class="text-center">
                                <h1 class="h4 text-gray-900 mb-4">Create a Lecturer</h1>
                            </div>

                            @include('registration.lecturers._form',['button_caption'=>'Save'])

                        </div>
                    </div>


                </div>
                {!! Form::close() !!}
            </div>
        </div>

    </div>
@stop

@section('alert')

@stop
@section('application_js')
    {{--<script src="/js/application/lecturer.js" type="text/javascript"></script>--}}

@stop

