{{ csrf_field() }}
<div class="row ">
    <div class="col-sm-6 ">
        <div class="form-group  mb-sm-0">
            {!! Form::label('id','ID',['class'=>'label-info mb-sm-0']) !!}
            @if($button_caption=='Update')
                {!! Form::text('id', $lecturers->id,['class'=>'form-control form-control-sm','readonly'=>'true']) !!}
            @else
                {!! Form::text('id', $new_index,['class'=>'form-control form-control-sm','readonly'=>'true']) !!}
            @endif
            @if($errors->has('id'))
                <span class="alert-danger"> {{ $errors->first('id') }}</span>
            @endif
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-3">
        <div class="form-group">
            {!! Form::label('salutation','Salutation',['class'=>'label-info mb-sm-0']) !!}
            {!! Form::text('salutation', $lecturers->salutation,['class'=>'form-control form-control-sm']) !!}
            @if($errors->has('salutation'))
                <span class="alert-danger"> {{ $errors->first('salutation') }}</span>
            @endif
        </div>
    </div>
    <div class="col-sm-9">
        <div class="form-group">
            {!! Form::label('name','Name (with initials)',['class'=>'label-info mb-sm-0']) !!}
            {!! Form::text('name', $lecturers->name,['class'=>'form-control form-control-sm']) !!}
            @if($errors->has('name'))
                <span class="alert-danger"> {{ $errors->first('name') }}</span>
            @endif
        </div>
    </div>
</div>

<div class="col-sm-0">
    <div class="form-group">
        {!! Form::label('long_name','Full name',['class'=>'label-info mb-sm-0']) !!}
        {!! Form::text('long_name', $lecturers->long_name,['class'=>'form-control form-control-sm']) !!}
        @if($errors->has('long_name'))
            <span class="alert-danger"> {{ $errors->first('long_name') }}</span>
        @endif
    </div>
</div>

<div class="col-sm-0">
    <div class="form-group">
        {!! Form::label('address_line1','Address Line 1',['class'=>'label-info mb-sm-0']) !!}
        {!! Form::text('address_line1', $lecturers->address_line1,['class'=>'form-control form-control-sm']) !!}
        @if($errors->has('address_line1'))
            <span class="alert-danger"> {{ $errors->first('address_line1') }}</span>
        @endif
    </div>
</div>
<div class="col-sm-0">
    <div class="form-group">
        {!! Form::label('address_line2','Address Line 2',['class'=>'label-info mb-sm-0']) !!}
        {!! Form::text('address_line2', $lecturers->address_line2,['class'=>'form-control form-control-sm']) !!}
        @if($errors->has('address_line2'))
            <span class="alert-danger"> {{ $errors->first('address_line2') }}</span>
        @endif
    </div>
</div>
<div class="col-sm-0">
    <div class="form-group">
        {!! Form::label('address_line3','Address Line 3',['class'=>'label-info mb-sm-0']) !!}
        {!! Form::text('address_line3', $lecturers->address_line3,['class'=>'form-control form-control-sm']) !!}
        @if($errors->has('address_line3'))
            <span class="alert-danger"> {{ $errors->first('address_line3') }}</span>
        @endif
    </div>
</div>

<div class="row">
    <div class="col-sm-6">
        <div class="form-group">
            {!! Form::label('telephone1','Telephone 1',['class'=>'label-info mb-sm-0']) !!}
            {!! Form::text('telephone1', $lecturers->telephone1,['class'=>'form-control form-control-sm']) !!}
            @if($errors->has('telephone1'))
                <span class="alert-danger"> {{ $errors->first('telephone1') }}</span>
            @endif
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            {!! Form::label('mobile','Mobile',['class'=>'label-info mb-sm-0']) !!}
            {!! Form::text('mobile', $lecturers->mobile,['class'=>'form-control form-control-sm']) !!}
            @if($errors->has('mobile'))
                <span class="alert-danger"> {{ $errors->first('mobile') }}</span>
            @endif
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-6">
        <div class="form-group">
            {!! Form::label('telephone2','Telephone 2',['class'=>'label-info mb-sm-0']) !!}
            {!! Form::text('telephone2', $lecturers->telephone2,['class'=>'form-control form-control-sm']) !!}
            @if($errors->has('telephone2'))
                <span class="alert-danger"> {{ $errors->first('telephone2') }}</span>
            @endif
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            {!! Form::label('fax','Fax',['class'=>'label-info mb-sm-0']) !!}
            {!! Form::text('fax', $lecturers->fax,['class'=>'form-control form-control-sm']) !!}
            @if($errors->has('fax'))
                <span class="alert-danger"> {{ $errors->first('fax') }}</span>
            @endif
        </div>
    </div>
</div>

<div class="col-sm-0">
    <div class="form-group">
        {!! Form::label('email','Email',['class'=>'label-info mb-sm-0']) !!}
        {!! Form::text('email', $lecturers->email,['class'=>'form-control form-control-sm']) !!}
        @if($errors->has('email'))
            <span class="alert-danger"> {{ $errors->first('email') }}</span>
        @endif
    </div>
</div>

<div class="col-sm-0">
    <div class="form-group">
        {!! Form::label('note','Note',['class'=>'label-info mb-sm-0']) !!}
        {!! Form::text('note', $lecturers->note,['class'=>'form-control form-control-sm']) !!}
        @if($errors->has('note'))
            <span class="alert-danger"> {{ $errors->first('note') }}</span>
        @endif
    </div>
</div>

<div class="row">
    <div class="col-sm-1">
    </div>
    <div class="col-sm-4">
        <div class="form-group">
            {!! Form::checkbox('is_external',$lecturers->is_external,($lecturers->is_external),['class'=>'form-check-input']) !!}
            {!! Form::label('is_external','Visiting Lecturer',['class'=>'label-info mb-sm-0', 'value'=>true]) !!}
            @if($errors->has('is_external'))
                <span class="alert-danger"> {{ $errors->first('is_external') }}</span>
            @endif
        </div>
    </div>
    <div class="col-sm-7">
        <div class="form-group">
            {!! Form::label('work_place','Work Place',['class'=>'label-info mb-sm-0']) !!}
            {!! Form::text('work_place', $lecturers->work_place,['class'=>'form-control form-control-sm']) !!}
            @if($errors->has('work_place'))
                <span class="alert-danger"> {{ $errors->first('work_place') }}</span>
            @endif
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-1">
    </div>
    <div class="col-sm-5">
        <div class="form-group">
            {!! Form::checkbox('is_active',$lecturers->is_active,($lecturers->is_active),['class'=>'form-check-input', 'value'=>true]) !!}
            {!! Form::label('is_active','Active',['class'=>'label-info mb-sm-0']) !!}
            @if($errors->has('is_active'))
                <span class="alert-danger"> {{ $errors->first('is_active') }}</span>
            @endif
        </div>
    </div>
</div>

<div class="float-right mb-2">

    <button type="submit"  class="btn  btn-success "><i class="fa fa-save"></i>{{$button_caption}}</button>
    <button type="button" class="btn  btn-info"><i class="fa fa-refresh"></i>Reset</button>

</div>


