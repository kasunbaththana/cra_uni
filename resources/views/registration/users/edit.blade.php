@extends('layouts.main')
@section('content')
    <div class="container">

        <div class="card o-hidden border-0 shadow-lg my-1">
            <div class="card-body p-0">
                <!-- Nested Row within Card Body -->
                {!! Form::model($users,['url'=>'users/edit', 'method' => 'put']) !!}
                <div class="row">
                    <div class="col-lg-7">
                        <div class="p-5">
                            <div class="text-center">
                                <h1 class="h4 text-gray-900 mb-4">Edit User</h1>
                            </div>

                            @include('registration.users._form',['button_caption'=>'Update'])

                        </div>
                    </div>
                    <div class="col-lg-5">

                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>

    </div>
@stop
@section('alert')

@stop
@section('application_js')
    {{--<script src="/js/application/department.js" type="text/javascript"></script>--}}

@stop

