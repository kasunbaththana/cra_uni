@extends('layouts.main')
@section('content')
    <div class="container-fluid">

        <!-- Page Heading -->
        <h6 class="h4 mb-2 text-gray-800">User Role</h6>

        <!-- DataTales Example -->
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <div class="float-right">

                    <a id="btnAddNew" class="btn btn-sm btn-info add-new" href="{{url('users_role/create')}}">
                        <i class="fa fa-plus"></i> Add New</a>

                </div>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table-sm table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tfoot>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Action</th>
                        </tr>
                        </tfoot>
                        <tbody>
                        @foreach($users as $cllist)
                            <tr>
                                <td class="col-md-1">{{$cllist->id}}</td>
                                <td class="col-md-2">{{$cllist->name}}</td>
                                <td class="col-md-1">
                                    <a type="button" href="{{route('users_role.edit',$cllist->id)}}"
                                       class="btn btn-sm btn-warning -edit"><i class="fa fa-edit"></i></a>
                                </td>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>
                    {{--  <div>{{$Clients->links()}}</div>--}}
                </div>
            </div>
        </div>

    </div>
@stop
