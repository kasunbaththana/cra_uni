@extends('layouts.main')
@section('content')
    {{-- <div class="container">--}}
    <div class="container-fluid">

        <div class="card o-hidden border-0 shadow-lg my-1">
            <div class="card-body p-0">
                <!-- Nested Row within Card Body -->
                {!! Form::model($users, ['url' => ['user_role_setup/save'], 'enctype'=>"multipart/form-data" ,'id'=>'user_role_setup']) !!}
                <div class="row">
                    <div class="col-lg-12">
                        <div class="p-3">
                            <div class="text-center">
                                <h1 class="h4 text-gray-900 mb-4">User Role Setup</h1>
                            </div>

                            @include('registration.users.role_setup._form',['button_caption'=>'Save'])

                        </div>
                    </div>


                </div>
                {!! Form::close() !!}
            </div>
        </div>

    </div>
@stop

@section('alert')

@stop
@section('application_js')
    <script src="/js/application/user_role_setup.js" type="text/javascript"></script>

@stop

