{{ csrf_field() }}
<div class="row">
<div class="col-sm-4">
    <div class="form-group">
        {!! Form::label('role_id','User Role',['class'=>'label-info mb-sm-0']) !!}
        {!! Form::select('role_id',$roles, $users->role_id,['class'=>'form-control form-control-sm']) !!}
        @if($errors->has('role_id'))
            <span class="alert-danger"> {{ $errors->first('role_id') }}</span>
        @endif
    </div>
</div>
</div>

<div class="col-sm-12">
<div class="card shadow mb-8">
    <div class="card-header py-3">
        <div class="float-right">

            <button type="button" id="view_permission" class="btn  btn-info "><i class="fa fa-th-list"></i> View Permission</button>

        </div>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table-sm table-bordered table-hover " id="model_view" width="100%" cellspacing="0">
                <thead>
                <tr>
                    <th class="col-sm-6">Module name</th>
                    <th class="col-sm-1">View</th>
                    <th class="col-sm-1">Add</th>
                    <th class="col-sm-1">Edit</th>
                    <th class="col-sm-1">Delete</th>
                    <th class="col-sm-1">Print</th>
                </tr>
                </thead>
                <tbody>


                </tbody>
            </table>
            {{--  <div>{{$Clients->links()}}</div>--}}
        </div>
    </div>
</div>
</div>

<div class="float-right m-5">

    <button type="button" id="btnSave" class="btn  btn-success "><i class="fa fa-save"></i>{{$button_caption}}</button>
    <button type="button" class="btn  btn-info"><i class="fa fa-refresh"></i>Reset</button>

</div>


