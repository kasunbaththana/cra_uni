/*
SQLyog Ultimate v11.11 (64 bit)
MySQL - 5.7.19 : Database - cra
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`cra` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `cra`;

/*Table structure for table `batch_degree_subject` */

DROP TABLE IF EXISTS `batch_degree_subject`;

CREATE TABLE `batch_degree_subject` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `batch_id` int(10) unsigned NOT NULL,
  `degree_id` int(10) unsigned NOT NULL,
  `subject_id` int(10) unsigned NOT NULL,
  `subject_duration` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `batch_degree_subject` */

insert  into `batch_degree_subject`(`id`,`batch_id`,`degree_id`,`subject_id`,`subject_duration`,`created_at`,`updated_at`) values (1,1,1,1,0,NULL,NULL);

/*Table structure for table `batches` */

DROP TABLE IF EXISTS `batches`;

CREATE TABLE `batches` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `degree_id` int(11) unsigned NOT NULL,
  `year` year(4) NOT NULL,
  `type` enum('B1','B2') CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `capacity` int(3) NOT NULL,
  `expired_date` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `degree_id` (`degree_id`),
  CONSTRAINT `batches_ibfk_1` FOREIGN KEY (`degree_id`) REFERENCES `degrees` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_swedish_ci;

/*Data for the table `batches` */

insert  into `batches`(`id`,`name`,`degree_id`,`year`,`type`,`capacity`,`expired_date`,`created_at`,`updated_at`) values (1,'FTP/2020/B2',4,2020,'B2',45,'2021-11-08','2020-05-09 17:26:30','2020-05-17 05:54:51'),(2,'FTP/2024/B1',4,2024,'B1',0,'2021-11-08','2020-05-09 17:27:33','2020-05-09 17:53:43'),(3,'BST/2020/B1',1,0000,'B1',0,'2021-11-08','2020-05-09 17:28:27','2020-05-09 17:28:27'),(4,'BST/2020/B2',1,2020,'B2',0,'2021-11-08','2020-05-09 17:29:35','2020-05-09 17:29:35'),(5,'FTP/2024/B2',4,2024,'B2',0,'2021-11-08','2020-05-11 14:25:17','2020-05-11 14:25:17');

/*Table structure for table `branch` */

DROP TABLE IF EXISTS `branch`;

CREATE TABLE `branch` (
  `code` varchar(10) NOT NULL,
  `prefix` varchar(10) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `address_line1` varchar(50) DEFAULT NULL,
  `address_line2` varchar(50) DEFAULT NULL,
  `address_line3` varchar(50) DEFAULT NULL,
  `hotline` varchar(10) DEFAULT NULL,
  `telephone1` varchar(10) DEFAULT NULL,
  `telephone2` varchar(10) DEFAULT NULL,
  `fax` varchar(10) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `note` varchar(100) DEFAULT NULL,
  `active` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `branch` */

insert  into `branch`(`code`,`prefix`,`name`,`address_line1`,`address_line2`,`address_line3`,`hotline`,`telephone1`,`telephone2`,`fax`,`email`,`note`,`active`) values ('001','UVT','Univocity of vocational Technology','NO 135 C','100 Kandawala Road, ','Dehiwala-Mount Lavinia','0112 630 7','+94-112630','',' +94-11263','univotec@univotec.ac.lk','',1);

/*Table structure for table `buildings` */

DROP TABLE IF EXISTS `buildings`;

CREATE TABLE `buildings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(30) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `buildings` */

insert  into `buildings`(`id`,`name`,`created_at`,`updated_at`) values (1,'New Building','2020-04-03 17:22:24','2020-04-03 17:22:24'),(2,'Old Building','2020-04-03 17:26:16','2020-04-03 17:47:59'),(3,'Media Complex','2020-04-03 17:27:04','2020-04-03 17:27:04');

/*Table structure for table `class_facilities` */

DROP TABLE IF EXISTS `class_facilities`;

CREATE TABLE `class_facilities` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `class_room_id` int(10) unsigned NOT NULL,
  `facility_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `class_room_id` (`class_room_id`),
  KEY `facility_id` (`facility_id`),
  CONSTRAINT `class_facilities_ibfk_1` FOREIGN KEY (`class_room_id`) REFERENCES `class_rooms` (`id`),
  CONSTRAINT `class_facilities_ibfk_2` FOREIGN KEY (`facility_id`) REFERENCES `facilities` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 COLLATE=utf8_swedish_ci;

/*Data for the table `class_facilities` */

insert  into `class_facilities`(`id`,`class_room_id`,`facility_id`,`created_at`,`updated_at`) values (12,4,1,'2020-04-15 17:08:24','2020-04-15 17:08:24'),(13,4,3,'2020-04-15 17:08:24','2020-04-15 17:08:24'),(14,4,4,'2020-04-15 17:08:24','2020-04-15 17:08:24'),(15,5,5,'2020-04-16 14:50:13','2020-04-16 14:50:13'),(16,6,1,'2020-04-16 14:52:46','2020-04-16 14:52:46'),(17,6,3,'2020-04-16 14:52:46','2020-04-16 14:52:46'),(18,6,4,'2020-04-16 14:52:47','2020-04-16 14:52:47');

/*Table structure for table `class_rooms` */

DROP TABLE IF EXISTS `class_rooms`;

CREATE TABLE `class_rooms` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(30) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `type` int(10) NOT NULL,
  `capacity` int(11) NOT NULL,
  `hall_id` int(11) unsigned NOT NULL,
  `location` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `hall` (`hall_id`),
  KEY `type` (`type`),
  CONSTRAINT `class_rooms_ibfk_2` FOREIGN KEY (`hall_id`) REFERENCES `halls` (`id`),
  CONSTRAINT `class_rooms_ibfk_3` FOREIGN KEY (`type`) REFERENCES `class_types` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

/*Data for the table `class_rooms` */

insert  into `class_rooms`(`id`,`name`,`type`,`capacity`,`hall_id`,`location`,`created_at`,`updated_at`) values (1,'CR-1',1,20,1,'','2020-04-09 17:19:19','2020-04-12 16:37:37'),(2,'CR-5',2,567,1,'asdas','2020-04-10 16:37:15','2020-04-10 16:37:15'),(3,'Lab -01',2,50,1,'near toilet','2020-04-10 17:20:31','2020-04-10 17:20:31'),(4,'Lab -02',1,90,1,'34','2020-04-14 16:14:00','2020-04-14 16:14:00'),(5,'CR-3',2,35,1,NULL,'2020-04-16 14:50:13','2020-04-16 14:50:13'),(6,'CR-6',1,43,1,NULL,'2020-04-16 14:52:46','2020-04-16 14:52:46');

/*Table structure for table `class_types` */

DROP TABLE IF EXISTS `class_types`;

CREATE TABLE `class_types` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL,
  `creates_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `class_types` */

insert  into `class_types`(`id`,`name`,`creates_at`) values (1,'CLASS ROOM ONLY',NULL),(2,'LAB',NULL);

/*Table structure for table `degrees` */

DROP TABLE IF EXISTS `degrees`;

CREATE TABLE `degrees` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `department_id` int(11) NOT NULL,
  `prefix` varchar(10) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `is_weekend` tinyint(1) NOT NULL,
  `is_weekday` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_swedish_ci;

/*Data for the table `degrees` */

insert  into `degrees`(`id`,`name`,`department_id`,`prefix`,`is_weekend`,`is_weekday`,`created_at`,`updated_at`) values (1,'Bachelor of Technology in Building Services Technology',5,'BST',1,1,'2020-04-25 16:10:00','2020-04-27 16:23:08'),(2,'Bachelor of Technology in Manufacturing Technology',5,'TMT',0,1,'2020-04-25 16:13:23','2020-04-25 16:13:23'),(3,'Bachelor of Technology in Construction Technology & Resource Management',5,'CTRM',0,0,'2020-04-27 15:28:09','2020-04-27 15:28:09'),(4,'Bachelor of Technology in Food Process Technology',6,'FTP',1,1,'2020-04-27 15:37:31','2020-04-28 14:53:32');

/*Table structure for table `departments` */

DROP TABLE IF EXISTS `departments`;

CREATE TABLE `departments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `faculty_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `faculty_id` (`faculty_id`),
  CONSTRAINT `departments_ibfk_1` FOREIGN KEY (`faculty_id`) REFERENCES `faculties` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

/*Data for the table `departments` */

insert  into `departments`(`id`,`name`,`faculty_id`,`created_at`,`updated_at`) values (1,'Department of Education and Training',1,NULL,'2020-04-17 15:18:31'),(2,'Department of Language Studies',1,NULL,'2020-04-17 15:58:50'),(3,'Continuing Education Center (CEC) & Institute of Technical Teacher Development (ITTD)',1,'2020-03-23 16:17:29','2020-04-17 15:59:03'),(4,'Curriculum Development Unit',1,'2020-03-23 16:48:16','2020-04-17 15:59:17'),(5,'Department of Construction Technology',2,'2020-04-17 15:10:18','2020-04-17 15:59:55'),(6,'Department of Manufacturing Technology',2,'2020-04-17 15:10:49','2020-04-17 16:00:06');

/*Table structure for table `exam_allocation_lectures` */

DROP TABLE IF EXISTS `exam_allocation_lectures`;

CREATE TABLE `exam_allocation_lectures` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `allocation_id` int(11) unsigned NOT NULL,
  `lecture_id` int(11) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `allocation_id` (`allocation_id`),
  KEY `exam_allocation_lecture_ibfk_2` (`lecture_id`),
  CONSTRAINT `exam_allocation_lectures_ibfk_1` FOREIGN KEY (`allocation_id`) REFERENCES `exam_allocations` (`id`),
  CONSTRAINT `exam_allocation_lectures_ibfk_2` FOREIGN KEY (`lecture_id`) REFERENCES `lecturers` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `exam_allocation_lectures` */

insert  into `exam_allocation_lectures`(`id`,`allocation_id`,`lecture_id`,`created_at`,`updated_at`) values (1,25,3,'2020-11-10 18:16:03','2020-11-10 18:16:03');

/*Table structure for table `exam_allocation_schedules` */

DROP TABLE IF EXISTS `exam_allocation_schedules`;

CREATE TABLE `exam_allocation_schedules` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `allocation_id` int(10) unsigned NOT NULL,
  `schedule_date` date NOT NULL,
  `from_time` time NOT NULL,
  `to_time` time NOT NULL,
  `batch_id` int(10) unsigned NOT NULL,
  `subject_id` int(10) unsigned NOT NULL,
  `leave` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `allocation_id` (`allocation_id`),
  KEY `batch_id` (`batch_id`),
  KEY `subject_id` (`subject_id`),
  CONSTRAINT `exam_allocation_schedules_ibfk_1` FOREIGN KEY (`allocation_id`) REFERENCES `exam_allocations` (`id`),
  CONSTRAINT `exam_allocation_schedules_ibfk_2` FOREIGN KEY (`batch_id`) REFERENCES `batches` (`id`),
  CONSTRAINT `exam_allocation_schedules_ibfk_3` FOREIGN KEY (`subject_id`) REFERENCES `subjects` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `exam_allocation_schedules` */

insert  into `exam_allocation_schedules`(`id`,`allocation_id`,`schedule_date`,`from_time`,`to_time`,`batch_id`,`subject_id`,`leave`,`created_at`,`updated_at`) values (12,26,'2020-11-11','08:00:00','13:30:00',4,1,0,'2020-11-11 14:53:32','2020-11-11 14:53:32'),(13,27,'2020-11-11','08:00:00','12:30:00',2,1,0,'2020-11-11 16:30:54','2020-11-11 16:30:54'),(14,25,'2020-11-11','08:00:00','12:00:00',2,1,0,'2020-11-14 14:31:17','2020-11-14 14:31:17');

/*Table structure for table `exam_allocations` */

DROP TABLE IF EXISTS `exam_allocations`;

CREATE TABLE `exam_allocations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `hall_id` int(11) unsigned NOT NULL,
  `status` char(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `hall_id` (`hall_id`),
  CONSTRAINT `exam_allocations_ibfk_1` FOREIGN KEY (`hall_id`) REFERENCES `halls` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `exam_allocations` */

insert  into `exam_allocations`(`id`,`hall_id`,`status`,`created_at`,`updated_at`) values (25,3,'P','2020-11-10 18:16:03','2020-11-10 18:16:03'),(26,3,'P','2020-11-11 14:53:32','2020-11-11 14:53:32'),(27,3,'P','2020-11-11 16:30:54','2020-11-11 16:30:54');

/*Table structure for table `facilities` */

DROP TABLE IF EXISTS `facilities`;

CREATE TABLE `facilities` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_swedish_ci;

/*Data for the table `facilities` */

insert  into `facilities`(`id`,`name`,`created_at`,`updated_at`) values (1,'White Board',NULL,NULL),(2,'Projector',NULL,NULL),(3,'Sounds',NULL,NULL),(4,'Wifi',NULL,NULL),(5,'Internet',NULL,NULL);

/*Table structure for table `faculties` */

DROP TABLE IF EXISTS `faculties`;

CREATE TABLE `faculties` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `hod` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `faculties` */

insert  into `faculties`(`id`,`name`,`hod`,`created_at`,`updated_at`) values (1,'Faculty of Education',0,NULL,NULL),(2,'Faculty of Engineering Technology',0,'2020-04-17 14:37:33','2020-04-17 14:43:03');

/*Table structure for table `floors` */

DROP TABLE IF EXISTS `floors`;

CREATE TABLE `floors` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(30) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `building_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `building_id` (`building_id`),
  CONSTRAINT `floors_ibfk_1` FOREIGN KEY (`building_id`) REFERENCES `buildings` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `floors` */

insert  into `floors`(`id`,`name`,`building_id`,`created_at`,`updated_at`) values (1,'1st Floor',3,'2020-04-04 16:48:38','2020-04-04 16:48:38'),(2,'1st Floor',2,'2020-04-04 16:49:37','2020-04-06 14:34:01'),(3,'2nd Floor',1,'2020-04-06 14:37:32','2020-04-06 14:37:32'),(4,'1st Floor',1,'2020-04-28 16:01:25','2020-04-28 16:01:25');

/*Table structure for table `halls` */

DROP TABLE IF EXISTS `halls`;

CREATE TABLE `halls` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(30) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `is_exam_hall` tinyint(1) NOT NULL,
  `floor_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `floor_id` (`floor_id`),
  CONSTRAINT `halls_ibfk_1` FOREIGN KEY (`floor_id`) REFERENCES `floors` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `halls` */

insert  into `halls`(`id`,`name`,`is_exam_hall`,`floor_id`,`created_at`,`updated_at`) values (1,'Hall-1',0,3,'2020-04-08 15:37:31','2020-04-08 15:37:31'),(2,'Exam Hall 1',1,3,'2020-10-29 15:03:44','2020-10-29 15:03:44'),(3,'Lab hall',1,1,'2020-10-29 15:13:24','2020-10-29 15:15:20'),(4,'Exam hall 2',1,1,'2020-10-29 15:14:47','2020-10-29 15:14:47');

/*Table structure for table `lecturers` */

DROP TABLE IF EXISTS `lecturers`;

CREATE TABLE `lecturers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nic` varchar(12) COLLATE utf8_unicode_ci NOT NULL,
  `salutation` varchar(5) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `long_name` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `address_line1` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `address_line2` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address_line3` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `telephone1` varchar(12) COLLATE utf8_unicode_ci NOT NULL,
  `telephone2` varchar(12) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mobile` varchar(12) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fax` varchar(12) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `note` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `photo` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `is_external` tinyint(1) NOT NULL DEFAULT '0',
  `work_place` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `lecturers` */

insert  into `lecturers`(`id`,`nic`,`salutation`,`name`,`long_name`,`address_line1`,`address_line2`,`address_line3`,`telephone1`,`telephone2`,`mobile`,`fax`,`email`,`note`,`photo`,`is_active`,`is_external`,`work_place`,`created_at`,`updated_at`) values (1,'893993','mr','Uruthitan','','',NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,0,0,NULL,NULL,NULL),(2,'43453','Mrs','Nambuwasam','','',NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,0,0,NULL,NULL,NULL),(3,'44444','Mr','Pathirana','','',NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,0,0,NULL,NULL,NULL);

/*Table structure for table `migrations` */

DROP TABLE IF EXISTS `migrations`;

CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=24 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `migrations` */

insert  into `migrations`(`id`,`migration`,`batch`) values (1,'2019_03_07_050757_create_sessions_table',1),(2,'2020_03_22_175533_create_department',1),(3,'2020_04_02_144739_create_buildings_table',2),(4,'2020_04_02_144948_create_floors_table',2),(5,'2020_04_02_145147_create_halls_table',3),(6,'2020_04_02_145959_create_class_rooms_table',3),(7,'2020_04_14_143341_create_facilities_table',4),(8,'2020_04_14_143717_create_class_facilities_table',4),(9,'2020_04_16_160702_create_faculties_table',5),(10,'2020_04_18_163943_create_batches_table',6),(11,'2020_04_18_171152_create_degrees_table',6),(12,'2020_05_17_081230_create_room_allocations_table',7),(13,'2020_05_21_153418_create_room_allocation_details_table',8),(14,'2020_05_21_161923_create_transaction_types_table',8),(15,'2020_06_11_142352_create_room_allocation_shedules_table',9),(16,'2020_08_06_170448_create_lecturers_table',10),(17,'2020_08_07_135110_create_subjects_table',10),(18,'2020_08_09_045148_create_batch_degree_subject_table',10),(19,'2020_08_16_131825_add_priority_level_to_user_roles',11),(20,'2020_10_31_143805_create_exam_allocations_table',12),(21,'2020_10_31_144627_create_exam_allocation_schedules_table',12),(22,'2020_11_08_160304_exam_allocatio-batches',13),(23,'2020_11_08_161059_exam_allocation_batches',14);

/*Table structure for table `photo` */

DROP TABLE IF EXISTS `photo`;

CREATE TABLE `photo` (
  `index_no` int(11) NOT NULL AUTO_INCREMENT,
  `photo` longblob,
  PRIMARY KEY (`index_no`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Data for the table `photo` */

/*Table structure for table `pro_user_logins` */

DROP TABLE IF EXISTS `pro_user_logins`;

CREATE TABLE `pro_user_logins` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_register_id` int(10) unsigned NOT NULL,
  `user_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `user_email` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `company_id` int(11) NOT NULL,
  `project_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `pro_user_logins` */

/*Table structure for table `pro_user_module` */

DROP TABLE IF EXISTS `pro_user_module`;

CREATE TABLE `pro_user_module` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `module_order_no` int(11) NOT NULL,
  `module_path` varchar(100) NOT NULL,
  `module_category_id` int(10) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `module_category_id` (`module_category_id`),
  CONSTRAINT `pro_user_module_ibfk_1` FOREIGN KEY (`module_category_id`) REFERENCES `pro_user_module_category` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

/*Data for the table `pro_user_module` */

insert  into `pro_user_module`(`id`,`name`,`module_order_no`,`module_path`,`module_category_id`) values (1,'Department',6,'/department',1),(2,'Building',1,'/building',1),(3,'Floor',2,'/floor',1),(4,'Hall',3,'/hall',1),(5,'Class Room',4,'/class_room',1),(6,'Facility',1,'/facility',3),(7,'Faculty',5,'/faculty',1),(8,'Degree',7,'/degree',1),(9,'Batches',8,'/batch',1),(10,'Class Room Allocation',1,'/room_allocation',2),(11,'Class Room Approval',2,'/room_approval',2),(12,'Allocation Cancel',2,'/room_cancel',2),(13,'Lectures',9,'/lecturers',1),(14,'Subjects',10,'/subjects',1),(15,'Exam Hall Allocation',11,'/exam_allocation',2),(16,'User',1,'/users',3),(17,'User Role',2,'/users_role',3),(18,'User Role Setup',2,'/user_role_setup',3);

/*Table structure for table `pro_user_module_category` */

DROP TABLE IF EXISTS `pro_user_module_category`;

CREATE TABLE `pro_user_module_category` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `icon` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `pro_user_module_category` */

insert  into `pro_user_module_category`(`id`,`name`,`icon`) values (1,'Registration',''),(2,'Transaction',''),(3,'Setting','');

/*Table structure for table `pro_user_permissions` */

DROP TABLE IF EXISTS `pro_user_permissions`;

CREATE TABLE `pro_user_permissions` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `role_id` int(10) NOT NULL,
  `module_id` int(10) NOT NULL,
  `view` tinyint(1) NOT NULL,
  `add` tinyint(1) NOT NULL,
  `edit` tinyint(1) NOT NULL,
  `delete` tinyint(1) NOT NULL,
  `print` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `role_id` (`role_id`),
  KEY `module_id` (`module_id`),
  CONSTRAINT `pro_user_permissions_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `pro_user_role` (`id`),
  CONSTRAINT `pro_user_permissions_ibfk_2` FOREIGN KEY (`module_id`) REFERENCES `pro_user_module` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=latin1;

/*Data for the table `pro_user_permissions` */

insert  into `pro_user_permissions`(`id`,`role_id`,`module_id`,`view`,`add`,`edit`,`delete`,`print`,`created_at`,`updated_at`) values (1,1,1,1,0,0,0,0,NULL,NULL),(2,1,2,1,0,0,0,0,NULL,NULL),(3,1,3,1,0,0,0,0,NULL,NULL),(4,1,4,1,0,0,0,0,NULL,NULL),(5,1,5,1,0,0,0,0,NULL,NULL),(6,1,6,1,0,0,0,0,NULL,NULL),(7,1,7,1,0,0,0,0,NULL,NULL),(8,1,8,1,0,0,0,0,NULL,NULL),(9,1,9,1,0,0,0,0,NULL,NULL),(10,1,10,1,0,0,0,0,NULL,NULL),(11,1,11,1,0,0,0,0,NULL,NULL),(12,1,12,1,0,0,0,0,NULL,NULL),(13,1,13,1,0,0,0,0,NULL,NULL),(14,1,14,1,0,0,0,0,NULL,NULL),(15,1,15,1,0,0,0,0,NULL,NULL),(16,1,16,1,0,0,0,0,NULL,NULL),(17,1,17,1,0,0,0,0,NULL,NULL),(18,1,18,1,0,0,0,0,NULL,NULL),(40,6,9,1,0,0,0,0,'2020-11-19 18:33:20','2020-11-19 18:33:20'),(41,6,2,1,0,0,0,0,'2020-11-19 18:33:20','2020-11-19 18:33:20'),(42,6,5,1,0,0,0,0,'2020-11-19 18:33:20','2020-11-19 18:33:20');

/*Table structure for table `pro_user_role` */

DROP TABLE IF EXISTS `pro_user_role`;

CREATE TABLE `pro_user_role` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(25) DEFAULT NULL,
  `priority` smallint(6) NOT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

/*Data for the table `pro_user_role` */

insert  into `pro_user_role`(`id`,`name`,`priority`,`updated_at`,`created_at`) values (1,'Admin',2,NULL,NULL),(2,'Executive',3,NULL,NULL),(3,'SupperAdmin',1,NULL,NULL),(4,'User',4,'2020-11-18 14:38:19',NULL),(5,'USER2',5,NULL,NULL),(6,'Operator',6,'2020-11-17 16:07:52','2020-11-17 16:07:52');

/*Table structure for table `room_allocation_details` */

DROP TABLE IF EXISTS `room_allocation_details`;

CREATE TABLE `room_allocation_details` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `allocation_id` int(10) unsigned NOT NULL,
  `lecture_id` int(11) NOT NULL,
  `subject_id` int(11) NOT NULL,
  `request_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `allocation_id` (`allocation_id`),
  CONSTRAINT `room_allocation_details_ibfk_1` FOREIGN KEY (`allocation_id`) REFERENCES `room_allocations` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `room_allocation_details` */

/*Table structure for table `room_allocation_schedules` */

DROP TABLE IF EXISTS `room_allocation_schedules`;

CREATE TABLE `room_allocation_schedules` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `allocation_id` int(11) NOT NULL,
  `schedule_date` date NOT NULL,
  `from_time` time NOT NULL,
  `to_time` time NOT NULL,
  `day_of_wk` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `leave` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=224 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `room_allocation_schedules` */

insert  into `room_allocation_schedules`(`id`,`allocation_id`,`schedule_date`,`from_time`,`to_time`,`day_of_wk`,`leave`,`created_at`,`updated_at`) values (5,22,'2020-06-13','08:00:00','08:00:00','',0,'2020-06-13 18:03:57','2020-06-13 18:03:57'),(6,22,'2020-06-14','08:00:00','08:00:00','',1,'2020-06-13 18:03:57','2020-06-13 18:03:57'),(7,22,'2020-06-20','08:00:00','08:00:00','',1,'2020-06-13 18:03:57','2020-06-13 18:03:57'),(8,22,'2020-06-21','08:00:00','08:00:00','',0,'2020-06-13 18:03:57','2020-06-13 18:03:57'),(9,22,'2020-06-27','08:00:00','08:00:00','',1,'2020-06-13 18:03:57','2020-06-13 18:03:57'),(10,23,'2020-06-28','08:00:00','12:30:00','',0,'2020-06-16 16:45:34','2020-06-16 16:45:34'),(11,23,'2020-07-01','08:00:00','12:30:00','',0,'2020-06-16 16:45:34','2020-06-16 16:45:34'),(12,23,'2020-07-03','08:00:00','12:30:00','',1,'2020-06-16 16:45:34','2020-06-16 16:45:34'),(13,23,'2020-07-04','08:00:00','12:30:00','',0,'2020-06-16 16:45:34','2020-06-16 16:45:34'),(14,23,'2020-06-29','08:00:00','12:30:00','',0,'2020-06-19 16:29:29','2020-06-19 16:29:29'),(15,23,'2020-07-02','08:00:00','12:30:00','',0,'2020-06-19 16:29:29','2020-06-19 16:29:29'),(16,23,'2020-07-06','08:00:00','12:30:00','',0,'2020-06-19 16:29:29','2020-06-19 16:29:29'),(17,23,'2020-07-08','08:00:00','12:30:00','',0,'2020-06-19 16:29:29','2020-06-19 16:29:29'),(18,23,'2020-07-09','08:00:00','12:30:00','',0,'2020-06-19 16:29:29','2020-06-19 16:29:29'),(19,23,'2020-07-11','08:00:00','12:30:00','',0,'2020-06-19 16:29:29','2020-06-19 16:29:29'),(68,24,'2020-07-04','08:00:00','17:30:00','',0,'2020-06-19 17:12:52','2020-06-19 17:12:52'),(69,24,'2020-07-05','08:00:00','17:30:00','',0,'2020-06-19 17:12:52','2020-06-19 17:12:52'),(70,24,'2020-07-11','08:00:00','17:30:00','',0,'2020-06-19 17:12:52','2020-06-19 17:12:52'),(71,24,'2020-07-12','08:00:00','17:30:00','',0,'2020-06-19 17:12:52','2020-06-19 17:12:52'),(72,24,'2020-07-18','08:00:00','17:30:00','',0,'2020-06-19 17:12:52','2020-06-19 17:12:52'),(73,24,'2020-07-19','08:00:00','17:30:00','',0,'2020-06-19 17:12:52','2020-06-19 17:12:52'),(74,24,'2020-07-25','08:00:00','17:30:00','',0,'2020-06-19 17:12:52','2020-06-19 17:12:52'),(75,24,'2020-07-26','08:00:00','17:30:00','',0,'2020-06-19 17:12:52','2020-06-19 17:12:52'),(76,25,'2020-08-05','08:00:00','12:30:00','',0,'2020-08-05 14:08:18','2020-08-05 14:08:18'),(77,25,'2020-08-12','08:00:00','12:30:00','',0,'2020-08-05 14:08:18','2020-08-05 14:08:18'),(78,25,'2020-08-19','08:00:00','12:30:00','',0,'2020-08-05 14:08:18','2020-08-05 14:08:18'),(79,25,'2020-08-26','08:00:00','12:30:00','',0,'2020-08-05 14:08:18','2020-08-05 14:08:18'),(80,26,'2020-08-13','08:00:00','12:30:00','',0,'2020-08-05 15:14:55','2020-08-05 15:14:55'),(81,26,'2020-08-20','08:00:00','12:30:00','',0,'2020-08-05 15:14:55','2020-08-05 15:14:55'),(82,27,'2020-08-20','08:00:00','12:30:00','',0,'2020-08-05 15:48:39','2020-08-05 15:48:39'),(83,30,'2020-08-05','08:00:00','08:00:00','',0,'2020-08-05 15:55:50','2020-08-05 15:55:50'),(84,30,'2020-08-12','08:00:00','08:00:00','',0,'2020-08-05 15:55:50','2020-08-05 15:55:50'),(85,30,'2020-08-19','08:00:00','08:00:00','',0,'2020-08-05 15:55:50','2020-08-05 15:55:50'),(86,30,'2020-08-26','08:00:00','08:00:00','',0,'2020-08-05 15:55:50','2020-08-05 15:55:50'),(87,31,'2020-08-19','00:00:00','04:05:00','',0,'2020-08-13 17:04:26','2020-08-13 17:04:26'),(88,31,'2020-08-26','00:00:00','04:05:00','',0,'2020-08-13 17:04:26','2020-08-13 17:04:26'),(89,31,'2020-09-02','00:00:00','04:05:00','',0,'2020-08-13 17:04:26','2020-08-13 17:04:26'),(90,31,'2020-09-09','00:00:00','04:05:00','',0,'2020-08-13 17:04:26','2020-08-13 17:04:26'),(91,31,'2020-09-16','00:00:00','04:05:00','',0,'2020-08-13 17:04:26','2020-08-13 17:04:26'),(92,31,'2020-09-23','00:00:00','04:05:00','',0,'2020-08-13 17:04:26','2020-08-13 17:04:26'),(93,31,'2020-09-30','00:00:00','04:05:00','',0,'2020-08-13 17:04:26','2020-08-13 17:04:26'),(94,31,'2020-10-07','00:00:00','04:05:00','',0,'2020-08-13 17:04:26','2020-08-13 17:04:26'),(95,31,'2020-10-14','00:00:00','04:05:00','',0,'2020-08-13 17:04:26','2020-08-13 17:04:26'),(96,31,'2020-10-21','00:00:00','04:05:00','',0,'2020-08-13 17:04:26','2020-08-13 17:04:26'),(97,32,'2020-09-19','08:00:00','08:00:00','',0,'2020-09-19 13:51:21','2020-09-19 13:51:21'),(98,32,'2020-09-20','08:00:00','08:00:00','',0,'2020-09-19 13:51:21','2020-09-19 13:51:21'),(99,32,'2020-09-26','08:00:00','08:00:00','',0,'2020-09-19 13:51:21','2020-09-19 13:51:21'),(100,32,'2020-09-27','08:00:00','08:00:00','',0,'2020-09-19 13:51:21','2020-09-19 13:51:21'),(101,32,'2020-10-03','08:00:00','08:00:00','',0,'2020-09-19 13:51:21','2020-09-19 13:51:21'),(102,32,'2020-10-04','08:00:00','08:00:00','',0,'2020-09-19 13:51:21','2020-09-19 13:51:21'),(103,32,'2020-10-10','08:00:00','08:00:00','',0,'2020-09-19 13:51:21','2020-09-19 13:51:21'),(104,32,'2020-10-11','08:00:00','08:00:00','',0,'2020-09-19 13:51:21','2020-09-19 13:51:21'),(105,32,'2020-10-17','08:00:00','08:00:00','',0,'2020-09-19 13:51:21','2020-09-19 13:51:21'),(106,32,'2020-10-18','08:00:00','08:00:00','',0,'2020-09-19 13:51:21','2020-09-19 13:51:21'),(107,32,'2020-10-24','08:00:00','08:00:00','',0,'2020-09-19 13:51:21','2020-09-19 13:51:21'),(108,32,'2020-10-25','08:00:00','08:00:00','',0,'2020-09-19 13:51:21','2020-09-19 13:51:21'),(109,32,'2020-10-31','08:00:00','08:00:00','',0,'2020-09-19 13:51:21','2020-09-19 13:51:21'),(110,32,'2020-11-01','08:00:00','08:00:00','',0,'2020-09-19 13:51:21','2020-09-19 13:51:21'),(111,32,'2020-11-07','08:00:00','08:00:00','',0,'2020-09-19 13:51:21','2020-09-19 13:51:21'),(128,34,'2020-10-10','13:00:00','14:30:00','',0,'2020-10-09 17:25:17','2020-10-09 17:25:17'),(129,34,'2020-10-11','13:00:00','14:30:00','',0,'2020-10-09 17:25:17','2020-10-09 17:25:17'),(130,34,'2020-10-17','13:00:00','14:30:00','',0,'2020-10-09 17:25:17','2020-10-09 17:25:17'),(131,34,'2020-10-18','13:00:00','14:30:00','',0,'2020-10-09 17:25:17','2020-10-09 17:25:17'),(132,34,'2020-10-24','13:00:00','14:30:00','',0,'2020-10-09 17:25:17','2020-10-09 17:25:17'),(133,34,'2020-10-25','13:00:00','14:30:00','',0,'2020-10-09 17:25:17','2020-10-09 17:25:17'),(134,34,'2020-10-31','13:00:00','14:30:00','',0,'2020-10-09 17:25:17','2020-10-09 17:25:17'),(135,34,'2020-11-01','13:00:00','14:30:00','',0,'2020-10-09 17:25:17','2020-10-09 17:25:17'),(136,34,'2020-11-07','13:00:00','14:30:00','',0,'2020-10-09 17:25:17','2020-10-09 17:25:17'),(137,34,'2020-11-08','13:00:00','14:30:00','',0,'2020-10-09 17:25:17','2020-10-09 17:25:17'),(138,34,'2020-11-14','13:00:00','14:30:00','',0,'2020-10-09 17:25:17','2020-10-09 17:25:17'),(139,34,'2020-11-15','13:00:00','14:30:00','',0,'2020-10-09 17:25:17','2020-10-09 17:25:17'),(140,34,'2020-11-21','13:00:00','14:30:00','',0,'2020-10-09 17:25:17','2020-10-09 17:25:17'),(141,34,'2020-11-22','13:00:00','14:30:00','',0,'2020-10-09 17:25:17','2020-10-09 17:25:17'),(142,34,'2020-11-28','13:00:00','14:30:00','',0,'2020-10-09 17:25:17','2020-10-09 17:25:17'),(143,34,'2020-11-29','13:00:00','14:30:00','',0,'2020-10-09 17:25:17','2020-10-09 17:25:17'),(144,35,'2020-10-10','14:00:00','17:30:00','',0,'2020-10-09 17:28:57','2020-10-09 17:28:57'),(145,35,'2020-10-11','14:00:00','17:30:00','',0,'2020-10-09 17:28:57','2020-10-09 17:28:57'),(146,35,'2020-10-17','14:00:00','17:30:00','',0,'2020-10-09 17:28:57','2020-10-09 17:28:57'),(147,35,'2020-10-18','14:00:00','17:30:00','',0,'2020-10-09 17:28:57','2020-10-09 17:28:57'),(148,35,'2020-10-24','14:00:00','17:30:00','',0,'2020-10-09 17:28:57','2020-10-09 17:28:57'),(149,35,'2020-10-25','14:00:00','17:30:00','',0,'2020-10-09 17:28:57','2020-10-09 17:28:57'),(150,35,'2020-10-31','14:00:00','17:30:00','',0,'2020-10-09 17:28:57','2020-10-09 17:28:57'),(151,36,'2020-10-10','08:00:00','12:00:00','',0,'2020-10-09 17:31:52','2020-10-09 17:31:52'),(152,36,'2020-10-11','08:00:00','12:00:00','',0,'2020-10-09 17:31:52','2020-10-09 17:31:52'),(153,36,'2020-10-17','08:00:00','12:00:00','',0,'2020-10-09 17:31:52','2020-10-09 17:31:52'),(154,36,'2020-10-18','08:00:00','12:00:00','',0,'2020-10-09 17:31:52','2020-10-09 17:31:52'),(155,36,'2020-10-24','08:00:00','12:00:00','',0,'2020-10-09 17:31:52','2020-10-09 17:31:52'),(156,36,'2020-10-25','08:00:00','12:00:00','',0,'2020-10-09 17:31:52','2020-10-09 17:31:52'),(157,36,'2020-10-31','08:00:00','12:00:00','',0,'2020-10-09 17:31:52','2020-10-09 17:31:52'),(158,37,'2020-10-11','08:00:00','12:00:00','',0,'2020-10-11 03:18:19','2020-10-11 03:18:19'),(159,37,'2020-10-14','08:00:00','12:00:00','',0,'2020-10-11 03:18:19','2020-10-11 03:18:19'),(160,37,'2020-10-17','08:00:00','12:00:00','',0,'2020-10-11 03:18:19','2020-10-11 03:18:19'),(161,37,'2020-10-18','08:00:00','12:00:00','',0,'2020-10-11 03:18:19','2020-10-11 03:18:19'),(162,37,'2020-10-24','08:00:00','12:00:00','',0,'2020-10-11 03:18:19','2020-10-11 03:18:19'),(163,37,'2020-10-25','08:00:00','12:00:00','',0,'2020-10-11 03:18:19','2020-10-11 03:18:19'),(164,37,'2020-10-31','08:00:00','12:00:00','',0,'2020-10-11 03:18:19','2020-10-11 03:18:19'),(165,38,'2020-10-21','08:00:00','09:00:00','',0,'2020-10-11 03:21:51','2020-10-11 03:21:51'),(166,39,'2020-10-21','10:00:00','11:00:00','',0,'2020-10-11 04:00:42','2020-10-11 04:00:42'),(167,40,'2020-10-21','09:00:00','10:00:00','',0,'2020-10-15 16:43:34','2020-10-15 16:43:34'),(206,33,'2020-10-10','08:00:00','12:30:00','',0,'2020-11-14 14:58:55','2020-11-14 14:58:55'),(207,33,'2020-10-11','08:00:00','12:30:00','',0,'2020-11-14 14:58:55','2020-11-14 14:58:55'),(208,33,'2020-10-17','08:00:00','12:30:00','',0,'2020-11-14 14:58:55','2020-11-14 14:58:55'),(209,33,'2020-10-18','08:00:00','12:30:00','',0,'2020-11-14 14:58:55','2020-11-14 14:58:55'),(210,33,'2020-10-24','08:00:00','12:30:00','',0,'2020-11-14 14:58:55','2020-11-14 14:58:55'),(211,33,'2020-10-25','08:00:00','12:30:00','',0,'2020-11-14 14:58:55','2020-11-14 14:58:55'),(212,33,'2020-10-31','08:00:00','12:30:00','',0,'2020-11-14 14:58:55','2020-11-14 14:58:55'),(213,33,'2020-11-01','08:00:00','12:30:00','',0,'2020-11-14 14:58:55','2020-11-14 14:58:55'),(214,33,'2020-11-07','08:00:00','12:30:00','',0,'2020-11-14 14:58:55','2020-11-14 14:58:55'),(215,33,'2020-11-08','08:00:00','12:30:00','',0,'2020-11-14 14:58:55','2020-11-14 14:58:55'),(216,33,'2020-11-14','08:00:00','12:30:00','',0,'2020-11-14 14:58:55','2020-11-14 14:58:55'),(217,33,'2020-11-15','08:00:00','12:30:00','',0,'2020-11-14 14:58:55','2020-11-14 14:58:55'),(218,33,'2020-11-21','08:00:00','12:30:00','',0,'2020-11-14 14:58:55','2020-11-14 14:58:55'),(219,33,'2020-11-22','08:00:00','12:30:00','',0,'2020-11-14 14:58:55','2020-11-14 14:58:55'),(220,33,'2020-11-28','08:00:00','12:30:00','',0,'2020-11-14 14:58:55','2020-11-14 14:58:55'),(221,33,'2020-11-29','08:00:00','12:30:00','',0,'2020-11-14 14:58:55','2020-11-14 14:58:55'),(222,42,'2020-10-21','13:30:00','15:30:00','',0,'2020-11-14 15:03:09','2020-11-14 15:03:09'),(223,41,'2020-10-21','08:00:00','17:30:00','',0,'2020-11-14 15:04:56','2020-11-14 15:04:56');

/*Table structure for table `room_allocations` */

DROP TABLE IF EXISTS `room_allocations`;

CREATE TABLE `room_allocations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `room_id` int(10) unsigned NOT NULL,
  `batch_id` int(10) unsigned NOT NULL,
  `from_date` date NOT NULL,
  `to_date` date NOT NULL,
  `from_time` time NOT NULL,
  `to_time` time NOT NULL,
  `status` char(1) COLLATE utf8_swedish_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `sun` tinyint(1) NOT NULL DEFAULT '0',
  `sat` tinyint(1) NOT NULL DEFAULT '0',
  `mon` tinyint(1) NOT NULL DEFAULT '0',
  `tue` tinyint(1) NOT NULL DEFAULT '0',
  `wed` tinyint(1) NOT NULL DEFAULT '0',
  `thu` tinyint(1) NOT NULL DEFAULT '0',
  `fri` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `room_id` (`room_id`),
  KEY `batch_id` (`batch_id`),
  CONSTRAINT `room_allocations_ibfk_1` FOREIGN KEY (`room_id`) REFERENCES `class_rooms` (`id`),
  CONSTRAINT `room_allocations_ibfk_2` FOREIGN KEY (`batch_id`) REFERENCES `batches` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=utf8 COLLATE=utf8_swedish_ci;

/*Data for the table `room_allocations` */

insert  into `room_allocations`(`id`,`room_id`,`batch_id`,`from_date`,`to_date`,`from_time`,`to_time`,`status`,`created_at`,`updated_at`,`sun`,`sat`,`mon`,`tue`,`wed`,`thu`,`fri`) values (1,1,1,'2020-05-17','2020-05-17','13:30:00','17:00:00','O',NULL,NULL,0,0,0,0,0,0,0),(2,1,1,'2020-05-17','2020-05-17','08:30:00','12:00:00','O',NULL,NULL,0,0,0,0,0,0,0),(3,4,2,'2020-06-24','2020-06-24','08:00:00','12:30:00','','2020-06-05 14:21:57','2020-06-05 14:21:57',0,0,0,0,0,0,0),(4,3,3,'2020-06-05','2020-06-05','08:00:00','12:30:00','O','2020-06-05 14:24:29','2020-06-05 14:24:29',0,0,0,0,0,0,0),(5,3,3,'2020-06-10','2020-06-10','08:00:00','12:30:00','O','2020-06-05 15:19:48','2020-06-06 16:24:32',0,0,0,0,0,0,0),(6,2,4,'2020-06-10','2020-06-11','07:30:00','21:00:00','P','2020-06-08 16:51:50','2020-06-08 16:51:50',0,0,0,0,0,0,0),(7,3,3,'2020-06-10','2020-06-10','14:00:00','17:30:00','P','2020-06-05 15:19:48','2020-06-06 16:24:32',0,0,0,0,0,0,0),(8,3,2,'2020-06-11','2020-06-15','08:00:00','14:12:00','P','2020-06-10 16:25:07','2020-06-10 16:25:07',0,0,0,0,0,0,0),(22,2,2,'2020-06-13','2020-06-27','08:00:00','08:00:00','P','2020-06-13 18:03:57','2020-06-13 18:03:57',0,0,0,0,0,0,0),(23,2,3,'2020-06-28','2020-07-11','08:00:00','12:30:00','P','2020-06-16 16:45:34','2020-06-19 16:29:29',0,1,1,0,1,1,0),(24,6,4,'2020-07-01','2020-07-31','08:00:00','17:30:00','P','2020-06-19 16:38:18','2020-06-19 17:12:52',0,0,0,0,0,0,0),(25,3,4,'2020-08-05','2020-08-26','08:00:00','12:30:00','P','2020-08-05 14:08:18','2020-08-05 14:08:18',0,0,0,0,0,0,0),(26,3,3,'2020-08-12','2020-08-25','08:00:00','12:30:00','P','2020-08-05 15:14:55','2020-08-05 15:14:55',0,0,0,0,0,0,0),(27,1,2,'2020-08-12','2020-08-27','08:00:00','12:30:00','P','2020-08-05 15:48:39','2020-08-05 15:48:39',0,0,0,0,0,0,0),(28,1,2,'2020-08-05','2020-08-26','08:00:00','08:00:00','P','2020-08-05 15:49:47','2020-08-05 15:49:47',0,0,0,0,0,0,0),(29,1,4,'2020-08-05','2020-08-26','08:00:00','12:30:00','P','2020-08-05 15:51:27','2020-08-05 15:51:27',0,0,0,0,0,0,0),(30,1,2,'2020-08-05','2020-08-26','08:00:00','08:00:00','P','2020-08-05 15:55:50','2020-08-05 15:55:50',0,0,0,0,0,0,0),(31,1,2,'2020-08-19','2020-10-27','00:00:00','04:05:00','P','2020-08-13 17:04:26','2020-08-13 17:04:26',0,0,0,0,0,0,0),(32,4,2,'2020-09-19','2020-11-07','08:00:00','08:00:00','P','2020-09-19 13:51:21','2020-09-19 13:51:21',1,1,0,0,0,0,0),(33,6,3,'2020-10-09','2020-11-30','08:00:00','12:30:00','P','2020-10-09 17:24:01','2020-11-14 14:58:55',1,1,0,0,0,0,0),(34,4,5,'2020-10-09','2020-11-30','13:00:00','14:30:00','P','2020-10-09 17:25:17','2020-10-09 17:25:17',1,1,0,0,0,0,0),(35,2,4,'2020-10-10','2020-10-31','14:00:00','17:30:00','P','2020-10-09 17:28:57','2020-10-09 17:28:57',1,1,0,0,0,0,0),(36,2,4,'2020-10-10','2020-10-31','08:00:00','12:00:00','P','2020-10-09 17:31:52','2020-10-09 17:31:52',1,1,0,0,0,0,0),(37,5,5,'2020-10-11','2020-10-31','08:00:00','12:00:00','P','2020-10-11 03:18:19','2020-10-11 03:18:19',1,1,0,0,1,0,0),(38,5,4,'2020-10-14','2020-10-21','08:00:00','09:00:00','P','2020-10-11 03:21:51','2020-10-11 03:21:51',1,0,0,0,1,0,0),(39,5,4,'2020-10-21','2020-10-21','10:00:00','11:00:00','P','2020-10-11 04:00:42','2020-10-11 04:00:42',0,0,0,0,1,0,0),(40,5,3,'2020-10-21','2020-10-21','09:00:00','10:00:00','P','2020-10-15 16:43:34','2020-10-15 16:43:34',0,0,0,0,1,0,0),(41,2,3,'2020-10-21','2020-10-21','08:00:00','17:30:00','O','2020-10-19 14:19:58','2020-11-14 15:04:56',0,0,0,0,1,0,0),(42,3,2,'2020-10-21','2020-10-21','13:30:00','15:30:00','O','2020-10-19 17:22:13','2020-11-14 15:03:09',0,0,0,0,1,0,0),(43,2,3,'2020-10-21','2020-10-21','08:00:00','08:00:00','P','2020-10-19 17:26:02','2020-10-19 17:26:02',0,0,0,0,1,0,0);

/*Table structure for table `sessions` */

DROP TABLE IF EXISTS `sessions`;

CREATE TABLE `sessions` (
  `id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(10) unsigned DEFAULT NULL,
  `ip_address` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_agent` text COLLATE utf8_unicode_ci,
  `payload` text COLLATE utf8_unicode_ci NOT NULL,
  `last_activity` int(11) NOT NULL,
  UNIQUE KEY `sessions_id_unique` (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `sessions` */

/*Table structure for table `subjects` */

DROP TABLE IF EXISTS `subjects`;

CREATE TABLE `subjects` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `prefix` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `subjects` */

insert  into `subjects`(`id`,`name`,`description`,`prefix`,`created_at`,`updated_at`) values (1,'English','English','ENG',NULL,NULL);

/*Table structure for table `transaction_types` */

DROP TABLE IF EXISTS `transaction_types`;

CREATE TABLE `transaction_types` (
  `code` varchar(30) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_swedish_ci;

/*Data for the table `transaction_types` */

insert  into `transaction_types`(`code`,`name`,`created_at`,`updated_at`) values ('ROOM_ALLOCATION','Room Allocation',NULL,NULL);

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `nic_no` varchar(25) DEFAULT NULL,
  `salutation` varchar(5) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `long_name` varchar(250) DEFAULT NULL,
  `prefix` varchar(10) DEFAULT NULL,
  `address_line1` varchar(100) DEFAULT NULL,
  `address_line2` varchar(100) DEFAULT NULL,
  `address_line3` varchar(100) DEFAULT NULL,
  `mobile` varchar(10) DEFAULT NULL,
  `telephone1` varchar(10) DEFAULT NULL,
  `telephone2` varchar(10) DEFAULT NULL,
  `fax` varchar(10) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `note` varchar(100) DEFAULT NULL,
  `user_role` int(10) DEFAULT NULL,
  `login_access` tinyint(1) DEFAULT NULL,
  `password` varchar(150) DEFAULT NULL,
  `photo` longblob,
  `active` tinyint(1) DEFAULT NULL,
  `remember_token` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_employee_user_role1_idx` (`user_role`),
  CONSTRAINT `users_ibfk_1` FOREIGN KEY (`user_role`) REFERENCES `pro_user_role` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

/*Data for the table `users` */

insert  into `users`(`id`,`nic_no`,`salutation`,`name`,`long_name`,`prefix`,`address_line1`,`address_line2`,`address_line3`,`mobile`,`telephone1`,`telephone2`,`fax`,`email`,`note`,`user_role`,`login_access`,`password`,`photo`,`active`,`remember_token`,`created_at`,`updated_at`) values (1,'123456','Mr.','Admin_developer','Admin','AD','Ad1','','','001','','','','developer@gmail.com','',NULL,1,'123456',NULL,1,NULL,NULL,NULL),(2,NULL,NULL,'developer',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'admin@gmail.com',NULL,1,1,'$2y$10$UQXIXf5GcgK2QCnMGVZsPOUFZSlBij5tfyGjYibisJ2tO.XqCC8.q',NULL,1,'ASegfY05ml','2020-03-22 17:01:29','2020-11-16 16:19:35'),(3,NULL,NULL,'dsf',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'sadfs@',NULL,3,NULL,'$2y$10$Bg.xiyn1SF1z106S6BEJf.IoAQK/PWQ4xoyI25uLXRAwKKJ3Txc42',NULL,NULL,NULL,'2020-11-16 14:50:39','2020-11-24 15:05:01'),(4,NULL,NULL,'dfg','sdsssssssssssss',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'d@gmail.com',NULL,NULL,NULL,'$2y$10$.vQy5yxfJeYSmax3OxJ.9OjKjP8MVetSvW7wolVzS9E1s.1cNInHO',NULL,NULL,NULL,'2020-11-16 14:53:51','2020-11-16 16:24:06'),(5,NULL,'sd','sdf','sdf',NULL,'sdf','sdf','sdf','sd','sdf','2134','2134','kasunibm@gmail.com',NULL,NULL,NULL,'$2y$10$Mrig4g4vc2DYj13QQKv.fOHlpvVjIXygOvZkiJZKxz3RHCk1mtfHG',NULL,NULL,NULL,'2020-11-16 16:20:04','2020-11-16 16:20:04'),(6,NULL,'Mr','kamal','kamal adarachi',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'kamal@gmail.com',NULL,5,NULL,'$2y$10$lUNqEya.BjlHTEDE5AGpPukjAnQtoXi4qIyc9lghH2hHixuduHo.i',NULL,NULL,NULL,'2020-11-24 14:28:03','2020-11-24 15:05:27');

/* Function  structure for function  `gradeCalculation` */

/*!50003 DROP FUNCTION IF EXISTS `gradeCalculation` */;
DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` FUNCTION `gradeCalculation`(from_time time) RETURNS varchar(2) CHARSET latin1
BEGIN
  DECLARE RESULT VARCHAR(2) ;
    
    CASE
    WHEN from_time < "10:00" 
    THEN  SET RESULT = "A" ;
    WHEN from_time < "12:30" && from_time >= "10:00" 
    THEN SET  RESULT ="B" ;
    WHEN from_time < "14:30" && from_time >= "12:30" 
    THEN SET RESULT ="C" ;
    WHEN from_time < "17:00" && from_time >= "14:30" 
    THEN SET RESULT = "D" ;
  END CASE;
  RETURN RESULT;
    END */$$
DELIMITER ;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
