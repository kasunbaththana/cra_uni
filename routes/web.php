<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use CRA\Http\Controllers\DegreeController;

Route::get('/', function () {
    return view('welcome');
});

Route::group(['middleware' => ['prevent-back-history']],function(){

Route::get('dashboard', 'UserController@dashboard');
Route::get('main_page', 'LoginController@index');
Route::post('main_page/login', 'UserController@login');
Route::post('main_page/logout', 'UserController@logout');
Route::post('main_page/login/permission', 'UserModuleController@loadPermission');
Route::post('{name}/main_page/login/permission', 'UserModuleController@loadPermission');
Route::post('{id}/{name}/main_page/login/permission', 'UserModuleController@loadPermission');
Route::group(['middleware' => ['auth_user']], function () {});

Route::resource('department','DepartmentController');
Route::post('department/save', 'DepartmentController@store');
Route::put('department/edit', 'DepartmentController@update');

Route::resource('building','BuildingController');
Route::post('building/save', 'BuildingController@store');
Route::put('building/edit', 'BuildingController@update');

Route::resource('floor','FloorController');
Route::post('floor/save', 'FloorController@store');
Route::put('floor/edit', 'FloorController@update');

Route::resource('hall','HallController');
Route::post('hall/save', 'HallController@store');
Route::put('hall/edit', 'HallController@update');

Route::resource('class_room','ClassRoomController');
Route::post('class_room/load_floors','ClassRoomController@loadFloors');
Route::post('class_room/load_halls','ClassRoomController@loadHalls');
Route::post('class_room/get_items','ClassRoomController@getItems');
Route::post('class_room/save', 'ClassRoomController@store');
Route::put('class_room/edit', 'ClassRoomController@update');


Route::resource('faculty','FacultyController');
Route::post('faculty/save', 'FacultyController@store');
Route::put('faculty/edit', 'FacultyController@update');

Route::resource('degree','DegreeController');
Route::post('degree/save', 'DegreeController@store');
Route::put('degree/edit', 'DegreeController@update');


Route::resource('batch','BatchController');
Route::post('batch/save', 'BatchController@store');
Route::put('batch/edit', 'BatchController@update');
Route::post('batch/prefix','BatchController@getPrefix');
Route::post('degree/subjects_list','DegreeController@getSubjects');
Route::post('batch/batch_list','BatchController@getSubjectList');



Route::resource('room_allocation','RoomAllocationController');
Route::post('room_allocation/data_find', 'RoomAllocationController@dataFind');
Route::post('room_allocation/room', 'RoomAllocationController@dataRoom');
Route::post('room_allocation/batch', 'RoomAllocationController@dataBatch');
Route::post('room_allocation/facility', 'RoomAllocationController@dataFacility');
Route::post('room_allocation/save', 'RoomAllocationController@save');
Route::put('room_allocation/update', 'RoomAllocationController@update');
Route::get('room_allocation/edit/{id}', 'RoomAllocationController@edit');
Route::post('room_allocation/subject', 'RoomAllocationController@subject');

Route::resource('lecturers','LecturerController');
Route::post('lecturers/save', 'LecturerController@store');
Route::put('lecturers/edit', 'LecturerController@update');

Route::resource('subjects','SubjectController');
Route::post('subjects/save', 'SubjectController@store');
Route::put('subjects/edit', 'SubjectController@update');
Route::get('subjects_list','SubjectController@getSubjectList');

Route::get('room_approval/edit/{id}', 'RoomAllocationApproval@edit');
Route::resource('room_approval','RoomAllocationApproval');
Route::put('room_approval/update', 'RoomAllocationApproval@update');

Route::resource('room_cancel','RoomAllocationCancelController');
Route::get('room_cancel/edit/{id}', 'RoomAllocationCancelController@edit');

Route::resource('exam_allocation','ExamAllocationController');
Route::post('exam_allocation/data_find', 'ExamAllocationController@dataFind');
Route::post('exam_allocation/halls', 'ExamAllocationController@dataHall');
Route::post('exam_allocation/save', 'ExamAllocationController@save');
Route::post('exam_allocation/get_items', 'ExamAllocationController@getItems');
Route::post('exam_allocation/validate_check', 'ExamAllocationController@resValidation');
Route::get('exam_allocation/edit/{id}', 'ExamAllocationController@edit');
Route::put('exam_allocation/update', 'ExamAllocationController@update');

Route::resource('users','UserAuthenticationController');
Route::post('users/save', 'UserAuthenticationController@store');
Route::get('users/edit/{id}', 'UserAuthenticationController@edit');
Route::put('users/update', 'UserAuthenticationController@update');
Route::post('/forgot-password', 'UserAuthenticationController@reset');

Route::resource('users_role','UserRoleController');
Route::post('users_role/save', 'UserRoleController@store');
Route::get('users_role/edit/{id}', 'UserRoleController@edit');
Route::put('users_role/update', 'UserRoleController@update');

Route::resource('user_role_setup','UserRoleSetupController');
Route::post('user_role_setup/save', 'UserRoleSetupController@store');
Route::post('user_role_setup/view', 'UserRoleSetupController@viewPermission');
Route::get('user_role_setup/edit/{id}', 'UserRoleSetupController@edit');
Route::put('user_role_setup/update', 'UserRoleSetupController@update');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('report_view', 'ReportGenerateController@reportView');
//Route::post('report_view/loan','ReportGenerateController@loan');
Route::post('reports/print', 'ReportGenerateController@reportPdf');


Route::resource('lecture_subject','LectureAssignController');
Route::get('lecture_subject/edit/{id}', 'LectureAssignController@edit');
Route::get('lecture_subject/update', 'LectureAssignController@update');

});