<?php

namespace CRA;

use Illuminate\Database\Eloquent\Model;

class BatchSubjectLecture extends Model
{

    protected $table = 'batch_subject_lectures';

    protected $fillable = ['batch_subject_id','lecture_id'];

    public function lectureId(){


        return $this->belongsTo(Lecturer::class,'lecture_id');
    }

}
