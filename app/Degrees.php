<?php

namespace CRA;

use Illuminate\Database\Eloquent\Model;

class Degrees extends Model
{
    protected $fillable = ['id','name','year','department_id','prefix','is_weekend','is_weekday'];


    public function  departmentId(){

        return $this->belongsTo(Department::class,'department_id');
    }
    public function degree_subjects()
    {

        return $this->hasMany(DegreeSubject::class, 'degree_id', 'id');
    }

//    public function subjects()
//    {
//        return $this->belongsToMany('App\Subject', 'degree_subject', 'degree_id', 'subject_id')->withPivot('subject_duration')->withTimestamps();
//    }
//    public function getIsWeekEndAttribute(){
//
//        return $this->attributes['is_weekend']==0?false:true;
//
//
//    }
//    public function getIsWeekDayAttribute(){
//
//        return $this->attributes['is_weekday']==0?false:true;
//
//
//    }
//    public function setIsWeekendAttribute($value){
//        if(empty($value) || is_null($value))
//        {
//            $this->attributes['is_weekend'] = false;
//        }
//        else
//        {
//            $this->attributes['is_weekend'] = true;
//        }
//
//    }
//    public function setIsWeekdayAttribute($value){
//        if(empty($value) || is_null($value))
//        {
//            $this->attributes['is_weekday'] = false;
//        }
//        else
//        {
//            $this->attributes['is_weekday'] = true;
//        }
//

//    }
}
