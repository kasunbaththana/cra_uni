<?php

namespace CRA;

use Illuminate\Database\Eloquent\Model;

class UserPermission extends Model
{
    protected $table="pro_user_permissions";

    protected $fillable=[
        'id',
        'role_id',
        'module_id',
        'view',
        'add',
        'edit',
        'delete',
        'print'];

     public function role(){

         return $this->belongsTo(UserRole::class,'role_id');
     }
     public function module(){

         return $this->belongsTo(UserModule::class,'module_id');
     }
}
