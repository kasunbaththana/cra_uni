<?php

namespace CRA;

use Illuminate\Database\Eloquent\Model;

class ReportPermissions extends Model
{
    protected $table = 'pro_report_permissions';
    protected $fillable = [
        'user_role_id',
        'report_id',
        'view'
    ];

    public function reportId(){


        return $this->belongsTo(Reports::class,'report_id');
    }

}
