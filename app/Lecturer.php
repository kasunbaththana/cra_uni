<?php

namespace CRA;

use Illuminate\Database\Eloquent\Model;

class Lecturer extends Model
{
    public $table="lecturers";
    protected $fillable = [
        'id',
        'nic',
        'salutation',
        'name',
        'long_name',
        'address_line1',
        'address_line2',
        'address_line3',
        'telephone1',
        'telephone2',
        'mobile',
        'fax',
        'email',
        'note',
        'photo',
        'is_active',
        'is_external',
        'work_place'
    ];


}
