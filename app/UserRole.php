<?php

namespace CRA;

use Illuminate\Database\Eloquent\Model;

class UserRole extends Model
{
    protected $table="pro_user_role";

    protected $fillable=['id','name','priority'];


    // public function permission()
    // {
    //     return $this->hasMany('App\UserPermission', 'role_id', 'id');
    // }
}
