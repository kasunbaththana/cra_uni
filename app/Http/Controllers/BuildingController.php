<?php

namespace CRA\Http\Controllers;

use CRA\Buildings;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Mockery\Exception;

class BuildingController extends Controller
{
    public function index(){

        $buildings = (new Buildings())->get();

        return view('registration.buildings.index', compact('buildings'));

    }
    public function create()
    {

        $buildings = new Buildings();

        $new_index =(new Buildings())->count('id')+1;

        return view('registration.buildings.create', compact('buildings','new_index'));

    }
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:30|unique:buildings',
        ]);


        try {

            DB::beginTransaction();

            $department = new Buildings($request->all());
            $department->save();


            DB::commit();
            return redirect('building/create')->with('alert', 'save!');
        } catch (Exception $ex) {
            DB::rollback();
            return $ex;
        }

    }
    public function edit($code)
    {


        $buildings= (new Buildings())->find($code);


        return view('registration.buildings.edit', compact('buildings'));

    }
    public function update(Request $request, $code)
    {

        $this->validate($request, [

            'name' => 'required|max:30|unique:buildings,name,'.$request->id,

        ]);

        try {

            $department = (new Buildings())->find($request->id);
            $department->update($request->all());

            return redirect('building')->with('alert', 'Updated!');
        } catch (Exception $e) {
            return $e;
        }


    }

}
