<?php

namespace CRA\Http\Controllers;

use CRA\Floors;
use CRA\Halls;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Mockery\Exception;

class HallController extends Controller
{
    public function index(){

        $halls = (new Halls())->with('floorId','floorId.buildingId')->get();

        return view('registration.halls.index', compact('halls'));

    }
    public function create()
    {


        $halls = new Halls();

        $new_index =(new Halls())->count('id')+1;

//        $floors = (new Floors())->orderBy('name')->pluck('mix_name','id');

        $floors =  DB::table('floors')
            ->selectRaw('floors.id,CONCAT(floors.name ," - ",buildings.name) as name')
            ->join('buildings','buildings.id','=','floors.building_id')
            ->orderBy('floors.name','asc')
            ->pluck('name','id');

        return view('registration.halls.create', compact('halls','floors','new_index'));

    }
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:30',
        ]);


        try {

            DB::beginTransaction();


            $department = new Halls($request->all());
            $department ->is_exam_hall =$request->is_exam_hall == 'on' ? '1' : '0';
            $department->save();


            DB::commit();
            return redirect('hall/create')->with('alert', 'save!');
        } catch (Exception $ex) {
            DB::rollback();
            return $ex;
        }

    }
    public function edit($code)
    {


        $halls= (new Halls())->with('floorId')->find($code);
        $floors =  DB::table('floors')
            ->selectRaw('floors.id,CONCAT(floors.name ," - ",buildings.name) as name')
            ->join('buildings','buildings.id','=','floors.building_id')
            ->orderBy('floors.name','asc')
            ->pluck('name','id');

        return view('registration.halls.edit', compact('halls','floors'));

    }
    public function update(Request $request, $code)
    {

        $this->validate($request, [

            'name' => 'required|max:30',

        ]);

        try {

            $hall= (new Halls())->find($request->id);
            $hall->is_exam_hall =$request->is_exam_hall == 'on' ? '1' : '0';
            $hall->update($request->all());

            return redirect('hall')->with('alert', 'Updated!');
        } catch (Exception $e) {
            return $e;
        }


    }
}
