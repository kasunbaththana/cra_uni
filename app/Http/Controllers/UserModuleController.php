<?php

namespace CRA\Http\Controllers;

use CRA\Permission;
use CRA\UserModule;
use CRA\UserPermission;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class UserModuleController extends Controller
{

    public function loadPermission(Request $request)
    {

//dd( $request->session()->get('user_role'));
        $user_auth = (new UserPermission())->
        where('role_id', $request->session()->get('user_role'))->
            with('role','module')->
        get();
        $module = (new UserModule())->orderBy('module_order_no')->get();




        return compact('user_auth','module');
    }

}
