<?php

namespace CRA\Http\Controllers;

use CRA\Buildings;
use CRA\Floors;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Mockery\Exception;

class FloorController extends Controller
{
    public function index(){

        $floors = (new Floors())->with('buildingId')->get();

        return view('registration.floors.index', compact('floors'));

    }
    public function create()
    {

        $floors = new Floors();

        $new_index =(new Floors())->count('id')+1;

        $buildings = (new Buildings())->orderBy('name')->pluck('name','id');

        return view('registration.floors.create', compact('floors','buildings','new_index'));

    }
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:30|unique:floors,name,null,null,building_id,'.$request->building_id,

        ]);


        try {

            DB::beginTransaction();

            $department = new Floors($request->all());
            $department->save();


            DB::commit();
            return redirect('floor/create')->with('alert', 'save!');
        } catch (Exception $ex) {
            DB::rollback();
            return $ex;
        }

    }
    public function edit($code)
    {


        $floors= (new Floors())->with('buildingId')->find($code);
        $buildings = (new Buildings())->orderBy('name')->pluck('name','id');

        return view('registration.floors.edit', compact('floors','buildings'));

    }
    public function update(Request $request, $code)
    {

        $this->validate($request, [

            'name' => 'required|max:30',

        ]);

        try {

            $floors = (new Floors())->find($request->id);
            $floors->update($request->all());

            return redirect('floor')->with('alert', 'Updated!');
        } catch (Exception $e) {
            return $e;
        }


    }
}
