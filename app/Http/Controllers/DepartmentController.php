<?php

namespace CRA\Http\Controllers;

use CRA\Department;
use CRA\Faculties;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Mockery\Exception;

class DepartmentController extends Controller
{

    public function index(){

        $department = (new Department())->with('facultyId')->get();

        return view('registration.department.index', compact('department'));

    }

    public function create()
    {

        $department = (new Department());
        $faculties = (new Faculties())->pluck('name','id');
        $new_index =(new Department())->count('id')+1;

        return view('registration.department.create', compact('department','faculties','new_index'));

    }

    public function edit($code)
    {


        $department = (new Department())->find($code);
        $faculties = (new Faculties())->pluck('name','id');

        return view('registration.department.edit', compact('department','faculties'));

    }
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:200|unique:departments',
        ]);


        try {



            DB::beginTransaction();

            $department = new Department($request->all());
            $department->save();


            DB::commit();
            return redirect('department/create')->with('alert', 'save!');
        } catch (Exception $ex) {
            DB::rollback();
            return $ex;
        }

    }

    public function update(Request $request, $code)
    {

        $this->validate($request, [

            'name' => 'required|max:200',

        ]);

       // dd($request->all());

        try {

            $department = (new Department())->find($request->id);
            $department->update($request->all());

            return redirect('department')->with('alert', 'Updated!');
        } catch (Exception $e) {
            return $e;
        }


    }

}
