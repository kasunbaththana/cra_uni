<?php

namespace CRA\Http\Controllers;

use Carbon\Carbon;
use CRA\Batches;
use CRA\BatchSubject;
use CRA\Buildings;
use CRA\ClassFacilities;
use CRA\ClassRooms;
use CRA\ExamAllocationSchedules;
use CRA\RoomAllocation;
use CRA\RoomAllocationSchedule;
use CRA\Subject;
use CRA\Util\ApplicationVarible;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Mockery\Exception;

class RoomAllocationController extends Controller
{
    public function index(){

        $room_allocation = (new RoomAllocation())->with('roomId','batchId')->get();
        $today = date("Y-m-d");
        $buildings = (new Buildings())->pluck('name','id');

        return view('transaction.room_allocation.index', compact('room_allocation','today','buildings'));

    }
    public function dataFind(Request $request){


//        $data = DB::table('room_allocations')
//            ->select(
//                'room_allocations.id',
//                'room_allocations.room_id',
//                'room_allocations.batch_id',
//                'room_allocations.from_date',
//                'room_allocations.to_date',
//                'room_allocations.from_time',
//                'room_allocations.to_time',
//                'room_allocations.status'
//
//            )
//            ->join('class_rooms','class_rooms.id','=','room_allocations.room_id')
//            ->where('room_allocations.from_date','<=',$request->t_date)
//            ->where('room_allocations.to_date','>=',$request->t_date)
//            ->orderBy('class_rooms.id')
//            ->get();

        $data = DB::table('room_allocation_schedules')
            ->select(
                'room_allocations.id',
                'room_allocations.room_id',
                'room_allocations.batch_id',
                'room_allocations.from_date',
                'room_allocations.to_date',
                'room_allocation_schedules.from_time',
                'room_allocation_schedules.to_time',
                'room_allocations.status',
                'batches.name as batch_name',

                DB::raw( 'gradeCalculation(`room_allocation_schedules`.`from_time`) AS grade'),

                DB::raw('IF(
                        `room_allocation_schedules`.`to_time` > "8:30" && gradeCalculation(`room_allocation_schedules`.`from_time`) <= "A",
                        "A",
                        ""
                      ) AS S1'),
                DB::raw('IF(
                        `room_allocation_schedules`.`to_time` > "10:30" && gradeCalculation(`room_allocation_schedules`.`from_time`) <= "B" ,
                        "B",
                        ""
                      ) AS S2'),
                DB::raw('IF(
                        `room_allocation_schedules`.`to_time` > "13:30" && gradeCalculation(`room_allocation_schedules`.`from_time`) <= "C",
                        "C",
                        ""
                      ) AS S3'),
                DB::raw('IF(
                        `room_allocation_schedules`.`to_time` > "15:30" && gradeCalculation(`room_allocation_schedules`.`from_time`) <= "D" ,
                        "D",
                        ""
                      ) AS S4'),

                DB::raw( 'CASE
                   WHEN `room_allocation_schedules`.`to_time` < "10:30" THEN 1
                   WHEN `room_allocation_schedules`.`to_time` < "13:30" && `room_allocation_schedules`.`to_time` >= "10:00" THEN 2
                   WHEN `room_allocation_schedules`.`to_time` < "15:30" && `room_allocation_schedules`.`to_time` >= "13:30" THEN 3
                   WHEN `room_allocation_schedules`.`to_time` < "17:30" && `room_allocation_schedules`.`to_time` >= "15:30" THEN 4
                  END ending')

            )
            ->join('room_allocations','room_allocation_schedules.allocation_id','=','room_allocations.id')
            ->join('class_rooms','class_rooms.id','=','room_allocations.room_id')
            ->join('halls','halls.id','=','class_rooms.hall_id')
            ->join('floors','floors.id','=','halls.floor_id')
            ->join('buildings','buildings.id','=','floors.building_id')
            ->join('batches','batches.id','=','room_allocations.batch_id')
            ->where('room_allocation_schedules.schedule_date','=',$request->t_date)
            ->where('room_allocation_schedules.leave','=',"0")
            ->where('buildings.id','=',$request->building_id)
            ->orderBy('class_rooms.id')
            ->orderBy('room_allocation_schedules.from_time')
            ->get();



        $rooms = DB::table('class_rooms')
            ->select(
                'class_rooms.*'

            )
            ->join('halls','halls.id','=','class_rooms.hall_id')
            ->join('floors','floors.id','=','halls.floor_id')
            ->join('buildings','buildings.id','=','floors.building_id')
            ->where('buildings.id','=',$request->building_id)
            ->orderBy('class_rooms.name')
            ->get();


       return compact('data','rooms');
    }

    public function create(){


        $room_allocation = new RoomAllocation();
        $new_index =(new RoomAllocation())->max('id')+1;

        $batches = (new Batches())->pluck('name','id');

        $transaction_type = ApplicationVarible::$ROOM_ALLOCATION;

        $title = "";
//        $head = ['Name','Type','Capacity','Hall'];
//        $data = ($this->getColumnData());



        return view('transaction.room_allocation.create',compact('room_allocation','new_index','batches','transaction_type','title'));

    }
    function getRoomColumnData($facility){


        $addQry="";
        $add="";
        if(sizeof($facility) > 0){
            foreach ($facility as $key=>$row){

                        $addQry .= ($add." facilities.id=".$row);
                            $add =" AND ";
                    }

            $class_rooms = DB::table('class_rooms')->
                join('class_facilities','class_facilities.class_room_id','=','class_rooms.id')->
                join('facilities','facilities.id','=','class_facilities.facility_id')->
                join('class_types','class_types.id','=','class_rooms.type')->
                join('halls','halls.id','=','class_rooms.hall_id')->
                where('class_rooms.capacity','>','0')->
                whereRaw($addQry)->
                select('class_rooms.*','class_types.name as type','halls.name as hallName')->
                groupBy('class_rooms.id')->
                get();
            $data = [];
            if(sizeof($class_rooms)>0) {
                foreach ($class_rooms as $list) {

                    $data[] =
                        [
                            $list->id,
                            $list->name,
                            $list->type,
                            $list->capacity,
                            $list->hallName

                        ];
                }
            }
        }else{
            $class_rooms = (new ClassRooms())->get();

            $data = [];
            if(sizeof($class_rooms)>0) {
                foreach ($class_rooms as $list) {

                    $data[] =
                        [
                            $list->id,
                            $list->name,
                            $list->types->name,
                            $list->capacity,
                            $list->hallId->name

                        ];
                }
            }

        }

        return $data;

    }

    public function dataRoom(Request $request){

        //dd($request->chk_facility);
        $head[] = ['title'=>'#'];
        $head[] = ['title'=>'Name'];
        $head[] = ['title'=>'Type'];
        $head[] = ['title'=>'Capacity'];
        $head[] = ['title'=>'Hall'];
        $data = ($this->getRoomColumnData(isset($request->chk_facility)?$request->chk_facility:[]));

        $title = "Class Room";



        return compact('head','data','title');
    }

    public function dataBatch(){
        $head[] = ['title'=>'#'];
        $head[] = ['title'=>'Name'];
        $head[] = ['title'=>'Degree'];
        $head[] = ['title'=>'Capacity'];
        $head[] = ['title'=>'Type'];
        $head[] = ['title'=>'Year'];

        $data = ($this->getBatchColumnData());

        $title = "Batch";

        return compact('head','data','title');

    }
    function getBatchColumnData(){

        $batches = (new Batches())->with('degreeId')->get();
        $data = [];

        foreach ($batches as $list){

            $data[] =
                [
                    $list->id,
                    $list->name,
                    $list->degreeId->name,
                    $list->capacity,
                    $list->type,
                    $list->year,


                ];
        }

        return $data;

    }public function subject(Request $request){
        $head[] = ['title'=>'#'];
        $head[] = ['title'=>'Name'];


        $data = ($this->getSubjectColumnData($request->id));

        $title = "Subject";

        return compact('head','data','title');

    }
    function getSubjectColumnData($id){

       // $batches = (new Subject())->where->get();
        $batches = (new BatchSubject())->where('batch_id',$id)->get();

        $data = [];

        foreach ($batches as $list){

            $data[] =
                [
                    $list->subjectId->id,
                    $list->subjectId->name,



                ];
        }

        return $data;

    }

    public function dataFacility(Request $request){


        $facility = (new ClassFacilities())
            ->with('facilityId')
            ->where('class_room_id',$request->id)
            ->get();



        return compact('facility');
    }
//
//    function getRoomColumnData(){
//
//        $data = (new ClassRooms())->get();
//
//        return $data;
//
//    }
//
//    public function dataRoom(){
//
//        $head = ['Name','Type','Capacity','Hall'];
//
//        $data = ($this->getRoomColumnData());
//
//
//
//        return compact('head','data');
//    }
        public function save(Request $request){


           $this->validate(Request(),
                [
                    'room_id' => 'required',
                    'h_room_id' => 'required',
                    'batch_id' => 'required',
                    'h_batch_id' => 'required',
                    'from_date' => 'required',
                    'to_date' => 'required',
                    'from_time' => 'required',
                    'to_time' => 'required',
                    'h_subject_id' => 'required',
                    'subject_id' => 'required',

                ]
            );



              try{

                     if(!isset($request->schedule) && !sizeof($request->schedule)>0){
                        return ["Generate Schedule and try again"];
                    }else {

                         $sch_list = json_decode($request->schedule, true);
                         $date_check = [];
                         $i = 0;

                         foreach ($sch_list as $key => $list) {

                             if (!$list[3]) {


                                 $date_check[$i] = $list[0];
                                 $i++;
                             }


                         }

//                         $datetime_from = (new Carbon($request->from_time))->addMinutes(1)->format('H:i:s');
//                         $datetime_to = (new Carbon($request->to_time))->subMinutes(1)->format('H:i:s');

                         $datetime_from = (new Carbon($request->from_time))->format('H:i:s');
                         $datetime_to = (new Carbon($request->to_time))->format('H:i:s');

                         $validate_batch_possible = (new RoomAllocationSchedule())->
                         selectRaw('count(room_allocation_schedules.id) as num,room_allocation_schedules.schedule_date as res_date,class_rooms.name as classroom,subjects.name as subjectname')->
                         join('room_allocations', 'room_allocations.id', '=', 'room_allocation_schedules.allocation_id')->
                         join('class_rooms', 'class_rooms.id', '=', 'room_allocations.room_id')->
                         join('subjects', 'subjects.id', '=', 'room_allocations.subject_id')->

                         where('room_allocations.room_id', $request->h_room_id)->
                         where('room_allocations.batch_id','<>', $request->h_batch_id)->
                         where('room_allocations.subject_id', $request->h_subject_id)->
                         where(function ($query) use ($request, $date_check) {
                             //$query->whereBetween('room_allocation_schedules.schedule_date', [$request->from_date, $request->to_date]);
                             $query->whereIn('room_allocation_schedules.schedule_date', $date_check);
                         })->
                         where(function ($query) use ($datetime_from, $datetime_to) {
                             $query->whereBetween('room_allocation_schedules.from_time', [$datetime_from, $datetime_to]);
                             $query->orwhereBetween('room_allocation_schedules.to_time', [$datetime_from, $datetime_to]);
                             $query->orwhereRaw('? BETWEEN room_allocation_schedules.from_time AND room_allocation_schedules.to_time', [$datetime_from]);
                             $query->orwhereRaw('? BETWEEN room_allocation_schedules.from_time AND room_allocation_schedules.to_time', [$datetime_to]);
                         })->
                         first();


                         $validate = (new RoomAllocationSchedule())->
                         selectRaw('count(room_allocation_schedules.id) as num,room_allocation_schedules.schedule_date as res_date')->
                         join('room_allocations', 'room_allocations.id', '=', 'room_allocation_schedules.allocation_id')->
                         where('room_allocations.room_id', $request->h_room_id)->
                         where(function ($query) use ($request, $date_check) {
                             //$query->whereBetween('room_allocation_schedules.schedule_date', [$request->from_date, $request->to_date]);
                             $query->whereIn('room_allocation_schedules.schedule_date', $date_check);
                         })->
                         where(function ($query) use ($datetime_from, $datetime_to) {
                             $query->whereBetween('room_allocation_schedules.from_time', [$datetime_from, $datetime_to]);
                             $query->orwhereBetween('room_allocation_schedules.to_time', [$datetime_from, $datetime_to]);
                             $query->orwhereRaw('? BETWEEN room_allocation_schedules.from_time AND room_allocation_schedules.to_time', [$datetime_from]);
                             $query->orwhereRaw('? BETWEEN room_allocation_schedules.from_time AND room_allocation_schedules.to_time', [$datetime_to]);
                         })->
                         first();

                         $validate_batch = (new RoomAllocationSchedule())->
                         selectRaw('count(room_allocation_schedules.id) as num,room_allocation_schedules.schedule_date as res_date')->
                         join('room_allocations', 'room_allocations.id', '=', 'room_allocation_schedules.allocation_id')->
                         where('room_allocations.batch_id', $request->h_batch_id)->
                         where(function ($query) use ($request, $date_check) {
                             //$query->whereBetween('room_allocation_schedules.schedule_date', [$request->from_date, $request->to_date]);
                             $query->whereIn('room_allocation_schedules.schedule_date', $date_check);
                         })->
                         where(function ($query) use ($datetime_from, $datetime_to) {
                             $query->whereBetween('room_allocation_schedules.from_time', [$datetime_from, $datetime_to]);
                             $query->orwhereBetween('room_allocation_schedules.to_time', [$datetime_from, $datetime_to]);
                             $query->orwhereRaw('? BETWEEN room_allocation_schedules.from_time AND room_allocation_schedules.to_time', [$datetime_from]);
                             $query->orwhereRaw('? BETWEEN room_allocation_schedules.from_time AND room_allocation_schedules.to_time', [$datetime_to]);
                         })->
                         first();


                         $find_hall_id = (new ClassRooms())->find($request->h_room_id);

                         $validate_exam_hall = (new ExamAllocationSchedules())->
                         selectRaw('count(exam_allocation_schedules.id) as num,exam_allocation_schedules.schedule_date as res_date')->
                         join('exam_allocations', 'exam_allocations.id', '=', 'exam_allocation_schedules.allocation_id')->
                         where('exam_allocations.hall_id', $find_hall_id->hall_id)->
                         where(function ($query) use ($request, $date_check) {
                             //$query->whereBetween('room_allocation_schedules.schedule_date', [$request->from_date, $request->to_date]);
                             $query->whereIn('exam_allocation_schedules.schedule_date', $date_check);
                         })->
                         first();




                             if (intval($validate_batch_possible->num) > 0 && $request->h_validate=="false") {

                                 return ["confirm", "Class room already reserve in " . $validate_batch_possible->classroom . "\n"
                                     . 'Date: ' . $validate_batch_possible->res_date . "\nSame Subject: " . $validate_batch_possible->subjectname];
                             } else if (intval($validate->num) > 0 && $request->h_validate=="false") {

                                 return ["Class room already reserve ! " . $validate->res_date];

                             } else if (intval($validate_batch->num) > 0 ) {

                                 return ["This Batch Cannot Reserve another! " . $validate->res_date];

                             } else if (intval($validate_exam_hall->num) > 0) {

                                 return ["This Class Room Cannot Reserve ! " . $validate_exam_hall->res_date . ' This Class Room is already assign to Exam Hall'];

                             } else {


                                 DB::beginTransaction();


                                  $index = (new RoomAllocation())->store($request);

                                    foreach ($sch_list as $key => $list) {
                                        if(!$list[3]) {
                                            $schedule = new RoomAllocationSchedule();
                                            $schedule->allocation_id = $index->id;
                                            $schedule->schedule_date = $list[0];
                                            $schedule->from_time = $list[1];
                                            $schedule->to_time = $list[2];
                                            $schedule->day_of_wk = "";
                                            $schedule->leave = $list[3];
                                            $schedule->save();
                                        }
                                    }


                                 DB::commit();
                                 return ["save", '/room_allocation'];

                             }

                     }
              }catch (Exception $ex){
                    dd($ex);
              }
        }

        public function dataSave(Request $request,$sch_list){
            try{


        }catch (Exception $ex){
        DB::rollback();
        }


        }
    public function edit($code)
    {


        $room_allocation = (new RoomAllocation())->with('roomId','batchId','subjectId')->find($code);

        $facilities = (new ClassFacilities())->where('class_room_id',$room_allocation->room_id)->with('facilityId')->get();

        $schedule = (new RoomAllocationSchedule())->where('allocation_id',$code)->orderBy('schedule_date')->get();

        $batches = (new Batches())->pluck('name','id');

        $transaction_type = ApplicationVarible::$ROOM_ALLOCATION;

        $title = "";

        $i = 0;

        return view('transaction.room_allocation.edit',compact('room_allocation','batches','transaction_type','title','facilities','schedule','i'));



    }
    public function update(Request $request, $code)
    {
        $this->validate(Request(),
            [
                'room_id' => 'required',
                'h_room_id' => 'required',
                'batch_id' => 'required',
                'h_batch_id' => 'required',
                'from_date' => 'required',
                'to_date' => 'required',
                'from_time' => 'required',
                'to_time' => 'required',
                'h_subject_id' => 'required',
                'subject_id' => 'required',
            ]
        );




        try{

            if(!isset($request->schedule) && !sizeof($request->schedule)>0){
                return ["Generate Schedule and try again"];
            }else {
                $this->removeScheduleData($request);

                $sch_list = json_decode($request->schedule, true);
                $date_check = [];
                $i = 0;

                foreach ($sch_list as $key => $list) {

                    if (!$list[3]) {


                        $date_check[$i] = $list[0];
                        $i++;
                    }


                }

                $datetime_from = (new Carbon($request->from_time))->format('H:i:s');
                $datetime_to = (new Carbon($request->to_time))->format('H:i:s');


                $datetime_from = (new Carbon($request->from_time))->format('H:i:s');
                $datetime_to = (new Carbon($request->to_time))->format('H:i:s');

                $validate_batch_possible = (new RoomAllocationSchedule())->
                selectRaw('count(room_allocation_schedules.id) as num,room_allocation_schedules.schedule_date as res_date,class_rooms.name as classroom,subjects.name as subjectname')->
                join('room_allocations', 'room_allocations.id', '=', 'room_allocation_schedules.allocation_id')->
                join('class_rooms', 'class_rooms.id', '=', 'room_allocations.room_id')->
                join('subjects', 'subjects.id', '=', 'room_allocations.subject_id')->

                where('room_allocations.room_id', $request->h_room_id)->
                where('room_allocations.batch_id','<>', $request->h_batch_id)->
                where('room_allocations.subject_id', $request->h_subject_id)->
                where(function ($query) use ($request, $date_check) {
                    //$query->whereBetween('room_allocation_schedules.schedule_date', [$request->from_date, $request->to_date]);
                    $query->whereIn('room_allocation_schedules.schedule_date', $date_check);
                })->
                where(function ($query) use ($datetime_from, $datetime_to) {
                    $query->whereBetween('room_allocation_schedules.from_time', [$datetime_from, $datetime_to]);
                    $query->orwhereBetween('room_allocation_schedules.to_time', [$datetime_from, $datetime_to]);
                    $query->orwhereRaw('? BETWEEN room_allocation_schedules.from_time AND room_allocation_schedules.to_time', [$datetime_from]);
                    $query->orwhereRaw('? BETWEEN room_allocation_schedules.from_time AND room_allocation_schedules.to_time', [$datetime_to]);
                })->
                first();

                $validate = (new RoomAllocationSchedule())->
                selectRaw('count(room_allocation_schedules.id) as num,room_allocation_schedules.schedule_date as res_date')->
                join('room_allocations', 'room_allocations.id', '=', 'room_allocation_schedules.allocation_id')->
                where('room_allocations.room_id', $request->h_room_id)->
                where(function ($query) use ($request,$date_check) {
                    //$query->whereBetween('room_allocation_schedules.schedule_date', [$request->from_date, $request->to_date]);
                    $query->whereIn('room_allocation_schedules.schedule_date', $date_check);
                })->
                where(function ($query) use ($datetime_from,$datetime_to) {
                    $query->whereBetween('room_allocation_schedules.from_time', [ $datetime_from, $datetime_to]);
                    $query->orwhereBetween('room_allocation_schedules.to_time', [ $datetime_from, $datetime_to]);
                    $query->orwhereRaw('? BETWEEN room_allocation_schedules.from_time AND room_allocation_schedules.to_time',[$datetime_from]);
                    $query->orwhereRaw('? BETWEEN room_allocation_schedules.from_time AND room_allocation_schedules.to_time',[$datetime_to]);
                })->
                first();

                $validate_batch = (new RoomAllocationSchedule())->
                selectRaw('count(room_allocation_schedules.id) as num,room_allocation_schedules.schedule_date as res_date')->
                join('room_allocations', 'room_allocations.id', '=', 'room_allocation_schedules.allocation_id')->
                where('room_allocations.batch_id', $request->h_batch_id)->
                where(function ($query) use ($request,$date_check) {
                    //$query->whereBetween('room_allocation_schedules.schedule_date', [$request->from_date, $request->to_date]);
                    $query->whereIn('room_allocation_schedules.schedule_date', $date_check);
                })->
                where(function ($query) use ($datetime_from,$datetime_to) {
                    $query->whereBetween('room_allocation_schedules.from_time', [ $datetime_from, $datetime_to]);
                    $query->orwhereBetween('room_allocation_schedules.to_time', [ $datetime_from, $datetime_to]);
                    $query->orwhereRaw('? BETWEEN room_allocation_schedules.from_time AND room_allocation_schedules.to_time',[$datetime_from]);
                    $query->orwhereRaw('? BETWEEN room_allocation_schedules.from_time AND room_allocation_schedules.to_time',[$datetime_to]);
                })->
                first();


                $find_hall_id = (new ClassRooms())->find($request->h_room_id);

                $validate_exam_hall = (new ExamAllocationSchedules())->
                selectRaw('count(exam_allocation_schedules.id) as num,exam_allocation_schedules.schedule_date as res_date')->
                join('exam_allocations', 'exam_allocations.id', '=', 'exam_allocation_schedules.allocation_id')->
                where('exam_allocations.hall_id',$find_hall_id->hall_id)->
                where(function ($query) use ($request,$date_check) {
                    //$query->whereBetween('room_allocation_schedules.schedule_date', [$request->from_date, $request->to_date]);
                    $query->whereIn('exam_allocation_schedules.schedule_date', $date_check);
                })->
                first();


                if (intval($validate_batch_possible->num) > 0 && $request->h_validate=="false") {

                    return ["confirm", "Class room already reserve in " . $validate_batch_possible->classroom . "\n"
                        . 'Date: ' . $validate_batch_possible->res_date . "\nSame Subject: " . $validate_batch_possible->subjectname];
                } else if (intval($validate->num) > 0 && $request->h_validate=="false") {

                    return ["This Batch Cannot Reserve another! ".$validate->res_date];

                }else if(intval($validate_exam_hall->num) > 0){

                    return ["This Class Room Cannot Reserve ! ".$validate_exam_hall->res_date.' This Class Room is already assign to Exam Hall'];

                } else {

                    DB::beginTransaction();

//                $class_r = [];
//                $rm_update = (new RoomAllocation())->find($request->id);



                    $rm = (new RoomAllocation())->find($request->id);
              $fill_table = [
                        'room_id' => $request->h_room_id,
                        'batch_id' => $request->h_batch_id,
                        'from_date'=>$request->from_date,
                        'to_date'=>$request->to_date,
                        'from_time'=>$request->from_time,
                        'to_time'=>$request->to_time,
                        'status' => "P",
                        'sun' => isset($request->sun)?true:false,
                        'mon' => isset($request->mon)?true:false,
                        'tue' => isset($request->tue)?true:false,
                        'wed' => isset($request->wed)?true:false,
                        'thu' => isset($request->thu)?true:false,
                        'fri' => isset($request->fri)?true:false,
                        'sat' => isset($request->sat)?true:false,
                    ];



                    $rm->update($fill_table);

                    $sch_list = json_decode($request->schedule, true);




                    foreach ($sch_list as $key => $list) {

                        $schedule = new RoomAllocationSchedule();
                        $schedule->allocation_id = $request->id;
                        $schedule->schedule_date = $list[0];
                        $schedule->from_time = $list[1];
                        $schedule->to_time = $list[2];
                        $schedule->day_of_wk = "";
                        $schedule->leave = $list[3];
                        $schedule->save();
                    }

                    DB::commit();
                    return ["save", '/room_allocation'];

                }

            }
        }catch (Exception $ex){
            DB::rollback();
        }


    }

    function removeScheduleData(Request $request){

            $data = (new RoomAllocationSchedule())->where('allocation_id',$request->id);

           return $data->delete();

    }
    function capacityCheck(Request $request){

        dd($request->all());

    }




}
