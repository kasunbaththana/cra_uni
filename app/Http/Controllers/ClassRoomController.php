<?php

namespace CRA\Http\Controllers;

use CRA\Buildings;
use CRA\ClassFacilities;
use CRA\ClassRooms;
use CRA\ClassTypes;
use CRA\Facility;
use CRA\Floors;
use CRA\Halls;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Mockery\Exception;
use PhpParser\Node\Expr\Array_;

class ClassRoomController extends Controller
{
    public function index(){

        $class_rooms = (new ClassRooms())->with('hallId','hallId.floorId')->get();



        return view('registration.class_rooms.index', compact('class_rooms'));

    }
    public function create()
    {


        $class_rooms= (new ClassRooms());


        $new_index =(new ClassRooms())->count('id')+1;

        $types = (new ClassTypes())->pluck('name','id');

        $buildings =  (new Buildings())->orderBy('name')->pluck('name','id');

        $floors = [];
        $halls = [];
        $items = (new Facility())->pluck('name','id');

        $set_items =[];


        return view('registration.class_rooms.create', compact('class_rooms','types','buildings','floors','halls','new_index','items','set_items'));

    }
    public function loadFloors(Request $request){

        $floors =  (new Floors())->where('building_id',$request->building_id)->get();

        return compact('floors');
    }
    public function loadHalls(Request $request){

        $halls =  (new Halls())->where('floor_id',$request->floor_id)->get();

        return compact('halls');
    }

    public function store(Request $request){


        $this->validate(Request(),[
            "name"=>"required|max:30",
            "building_id"=>"required",
            "floor_id"=>"required",
            "hall_id"=>"required"
        ]);



        try{

             $data = DB::table('class_rooms')
             ->select(DB::raw('count(class_rooms.id) as id'))
             ->join('halls','halls.id','=','class_rooms.hall_id')
             ->join('floors','floors.id','=','halls.floor_id')
             ->join('buildings','floors.building_id','=','buildings.id')
             ->where('class_rooms.name','=',$request->name)
             ->where('buildings.id','=',$request->building_id)->first();

             if($data->id > 0){

                return redirect('class_room/create')->with('alert', 'Name cannot be same in building');
                                            
                      
            // return [
            //         'name' => 'Name cannot be same in building',
                    
            //     ];
               // "Name cannot be same in building";
             }else{
            DB::beginTransaction();
            $rooms = (new ClassRooms($request->all()));
            $rooms->save();


            //facility save




            if (isset($request->tbl_item)) {

                foreach ($request->tbl_item as $key => $row1) {
                    $facility = (new ClassFacilities());
                    $facility->class_room_id = $rooms->id;
                    $facility->facility_id = $key;

                    $facility->save();

                }
            }


            }

            DB::commit();
            return redirect('class_room/create')->with('alert', 'Save Success!');
        }catch(Exception $exception){
            DB::rollback();
            return $exception;

        }



    }
    public function getItems(Request $request)
    {
        $items = (new Facility())->find($request->code);
        return compact('items');
    }

    public function edit($code)
    {


        $class_rooms= (new ClassRooms())->with('hallId','hallId.floorId','hallId.floorId.buildingId')->find($code);


        $types = (new ClassTypes())->pluck('name','id');

        $buildings =  (new Buildings())->orderBy('name')->pluck('name','id');

        $floors =  (new Floors())->pluck('name','id');

        $halls =  (new Halls())->pluck('name','id');
        $items = (new Facility())->pluck('name','id');

        $set_items =(new ClassFacilities())->where('class_room_id',$code)->with('facilityId')->get();

        return view('registration.class_rooms.edit', compact('class_rooms','types','buildings','floors','halls','items','set_items'));


    }
    public function update(Request $request, $code)
    {

        $this->validate($request, [

            'name' => 'sometimes|required|max:30|unique:class_rooms,name,'.$request->id,
            'capacity' => 'required|min:0',

        ]);

        try {
            DB::beginTransaction();
            $class_room = (new ClassRooms())->find($request->id);
            $class_room->update($request->all());


            //facility save

            $facility = (new ClassFacilities())->where('class_room_id',$request->id);

            if(isset($facility)){

                $facility->delete();
            }


            if (isset($request->tbl_item)) {

                foreach ($request->tbl_item as $key => $row1) {

                    $facilitys =[
                    'class_room_id' => $request->id,
                    'facility_id' => $key];

                    $facility->create($facilitys);

                }
            }
            DB::commit();
            return redirect('class_room')->with('alert', 'Updated!');
        } catch (Exception $e) {
            DB::rollback();
            return $e;
        }


    }


}
