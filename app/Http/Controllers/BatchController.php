<?php

namespace CRA\Http\Controllers;

use CRA\Batches;
use CRA\BatchSubject;
use CRA\Degrees;
use CRA\DegreeSubject;
use CRA\Subject;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Mockery\Exception;

class BatchController extends Controller
{
    public function index(){

        $batches = (new Batches())->with('degreeId')->get();

        return view('registration.batches.index', compact('batches'));

    }
    public function create()
    {

        $batches = new Batches();

        $new_index =(new Batches())->count('id')+1;

        $degrees = (new Degrees())->orderBy('name')->pluck('name','id');

        $subjects = (new Subject())->orderBy('name')->pluck('name','id');

        $items = (new Subject())->pluck('name','id');
        $types = [];

        $prefix = '';

        return view('registration.batches.create', compact('batches','items','degrees','new_index','subjects','types','prefix'));

    }
    public function getSubjectList(Request $request){




        $subjects = (new Subject())->find($request->id);


        return response()->json($subjects, 200);

    }
    public function getPrefix(Request $request){


        $degrees = (new Degrees())->find($request->id);


        return compact( 'degrees');
    }
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:50|unique:batches',
            'year'=> 'required',
            'type'=> 'required',


        ]);



        try {

            DB::beginTransaction();

            $department = new Batches($request->all());
            $department->save();
            $saved_batch_id = $department->id;


            $sch_list = json_decode($request->tmp_table, true);

            $this->deleteSubjectBatch($saved_batch_id);
            foreach ($sch_list as $key => $list) {
                $batch_subject = new BatchSubject();
                $date_check = [
                    'year'=>$list[0],
                    'semester'=>$list[1],
                    'subject_id'=>$list[2],
                    'subject_duration'=>$list[3],
                    'batch_id'=>$saved_batch_id
                     ];

                $batch_subject->create($date_check);
            }




            DB::commit();
            return ["save", '/batch'];
        } catch (Exception $ex) {
            DB::rollback();
            return $ex;
        }

    }
    public function deleteSubjectBatch($batch_id){

        $subject = (new BatchSubject())->where('batch_id',$batch_id);
        $subject->delete();


    }
    public function edit($code)
    {


        $batches= (new Batches())->with('degreeId')->find($code);

        $degrees = (new Degrees())->orderBy('name')->pluck('name','id');

        $types = array("B1"=>"B1","B2"=>"B2");

        $prefix = $batches->degreeId->prefix;

        $items = (new Subject())->pluck('name','id');

        $batches_subject = (new BatchSubject())->where('batch_id',$code)->with('subjectId')->get();


        return view('registration.batches.edit', compact('batches','degrees','types','prefix','items','batches_subject'));


    }
    public function update(Request $request, $code)
    {

        $this->validate($request, [

            'name' => 'sometimes|required|max:50|unique:batches,name,'.$request->id,
            'year'=> 'required',
            'type'=> 'required',

        ]);

        try {
            DB::beginTransaction();
            $batches = (new Batches())->find($request->id);
            $batches->update($request->all());


            $sch_list = json_decode($request->tmp_table, true);

            $this->deleteSubjectBatch($request->id);
            foreach ($sch_list as $key => $list) {
                $batch_subject = new BatchSubject();
                $date_check = [
                    'year'=>trim($list[0].''),
                    'semester'=>trim($list[1].''),
                    'subject_id'=>trim($list[2]),
                    'subject_duration'=>trim($list[3]),
                    'batch_id'=>$request->id
                ];

                $batch_subject->create($date_check);
            }


            DB::commit();
            return ["save", '/batch'];
        } catch (Exception $e) {
            DB::rollback();
            return $e;
        }


    }
}
