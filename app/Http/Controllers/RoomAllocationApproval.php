<?php

namespace CRA\Http\Controllers;

use CRA\Batches;
use CRA\ClassFacilities;
use CRA\RoomAllocation;
use CRA\RoomAllocationSchedule;
use CRA\Util\ApplicationVarible;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Mockery\Exception;

class RoomAllocationApproval extends Controller
{
    public function index(){

        $room_allocation = (new RoomAllocation())->
        with('roomId','batchId')->
        where('status','P')->
        orderBy('id')->
        get();
        $today = date("Y-m-d");


        return view('transaction.approval.index', compact('room_allocation'));

    }
    public function edit($code)
    {


        $room_allocation = (new RoomAllocation())->with('roomId','batchId','subjectId')->find($code);

        $facilities = (new ClassFacilities())->where('class_room_id',$room_allocation->room_id)->with('facilityId')->get();

        $schedule = (new RoomAllocationSchedule())->where('allocation_id',$code)->orderBy('schedule_date')->get();

        $batches = (new Batches())->pluck('name','id');

        $transaction_type = ApplicationVarible::$ROOM_ALLOCATION;

        $title = "";

        $i = 0;

        return view('transaction.approval.edit',compact('room_allocation','batches','transaction_type','title','facilities','schedule','i'));



    }
    public function update(Request $request){

        $this->validate(Request(),
            [
                'room_id' => 'required',
                'h_room_id' => 'required',
                'batch_id' => 'required',
                'h_batch_id' => 'required',
                'from_date' => 'required',
                'to_date' => 'required',
                'from_time' => 'required',
                'to_time' => 'required'
            ]
        );




        try{

            if(!isset($request->schedule) && !sizeof($request->schedule)>0){
                return ["Generate Schedule and try again"];
            }else {
                $this->removeScheduleData($request);

                $sch_list = json_decode($request->schedule, true);
                $date_check = [];
                $i = 0;

                foreach ($sch_list as $key => $list) {

                    if (!$list[3]) {


                        $date_check[$i] = $list[0];
                        $i++;
                    }


                }

                $validate = (new RoomAllocationSchedule())->
                selectRaw('count(room_allocation_schedules.id) as num,room_allocation_schedules.schedule_date as res_date')->
                join('room_allocations', 'room_allocations.id', '=', 'room_allocation_schedules.allocation_id')->
                where('room_allocations.room_id', $request->h_room_id)->
                where(function ($query) use ($request, $date_check) {
                    //$query->whereBetween('room_allocation_schedules.schedule_date', [$request->from_date, $request->to_date]);
                    $query->whereIn('room_allocation_schedules.schedule_date', $date_check);
                })->
                where(function ($query) use ($request) {
                    $query->whereBetween('room_allocation_schedules.from_time', [$request->from_time, $request->to_time]);
                    $query->orwhereBetween('room_allocation_schedules.to_time', [$request->from_time, $request->to_time]);
                })->
                first();

                if (intval($validate->num) > 0) {

                    return ["Cannot reserve Something went wrong !"];

                } else {

                    DB::beginTransaction();

//                $class_r = [];
//                $rm_update = (new RoomAllocation())->find($request->id);



                    $rm = (new RoomAllocation())->find($request->id);
                    $fill_table = [
                        'room_id' => $request->h_room_id,
                        'batch_id' => $request->h_batch_id,
                        'from_date'=>$request->from_date,
                        'to_date'=>$request->to_date,
                        'from_time'=>$request->from_time,
                        'to_time'=>$request->to_time,
                        'status' => "O",
                        'sun' => isset($request->sun)?true:false,
                        'mon' => isset($request->mon)?true:false,
                        'tue' => isset($request->tue)?true:false,
                        'wed' => isset($request->wed)?true:false,
                        'thu' => isset($request->thu)?true:false,
                        'fri' => isset($request->fri)?true:false,
                        'sat' => isset($request->sat)?true:false,
                    ];



                    $rm->update($fill_table);

                    $sch_list = json_decode($request->schedule, true);




                    foreach ($sch_list as $key => $list) {

                        $schedule = new RoomAllocationSchedule();
                        $schedule->allocation_id = $request->id;
                        $schedule->schedule_date = $list[0];
                        $schedule->from_time = $list[1];
                        $schedule->to_time = $list[2];
                        $schedule->day_of_wk = "";
                        $schedule->leave = $list[3];
                        $schedule->save();
                    }

                    DB::commit();
                    return ["save", '/room_approval'];

                }

            }
        }catch (Exception $ex){
            DB::rollback();
        }


    }
    function removeScheduleData(Request $request){

        $data = (new RoomAllocationSchedule())->where('allocation_id',$request->id);

        return $data->delete();

    }


}
