<?php

namespace CRA\Http\Controllers;

use CRA\RoomAllocation;

use Illuminate\Http\Request;

class RoomAllocationCancelController extends Controller
{
     public function index(){

    $room_allocation = (new RoomAllocation())->
    with('roomId','batchId')->
    where('status','<>','C')->
    where('status','<>','O')->
    orderBy('id')->
    get();
    $today = date("Y-m-d");


    return view('transaction.cancel_allocation.index', compact('room_allocation'));

}

public function edit($id){


	$class_room = (new RoomAllocation())->find($id);

	$class_room->status="C";

	$class_room->update();

	return redirect('room_cancel')->with('alert', 'Updated!');

}
}
