<?php



namespace CRA\Http\Controllers;



use CRA\Lecturer;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;

use Mockery\Exception;



class LecturerController extends Controller

{

    // public function handle($request, Closure $next)

    // {

    //     $permittedModules = Auth::user()->role()->permission()->modules()->module_path;





    //     if (in_array($permittedModules, $request->route())) {

    //         return true;

    //     }



    //     return $next($request);

    // }

    

    public function index()

    {

        $lecturers = Lecturer::all();



        return view('registration.lecturers.index', compact('lecturers'));

    }



    public function create()

    {

        $lecturers = (new Lecturer());



        $new_index = Lecturer::max('id') + 1;



      



        return view('registration.lecturers.create', compact('lecturers', 'new_index'));

    }



    public function store(Request $request)

    {

        $this->validate($request, [



            'salutation' => 'required',

            'name' => 'required|max:100',

            'long_name' => 'required|max:250',

            'address_line1' => 'nullable|max:100',

            'address_line2' => 'nullable|max:100',

            'address_line3' => 'nullable|max:100',

            'telephone1' => 'required|min:9|max:12',

            'telephone2' => 'nullable|max:12',

            'mobile' => 'nullable|max:12',

            'fax' => 'nullable|max:12',

            'email' => 'required|email|max:200',


        ]);






        try {
            DB::beginTransaction();
                $lecturer = new Lecturer();

                $lecturer->create($request->all());


            DB::commit();
                return redirect()->back()->with('alert', 'Saved successfully!');

            } catch (Exception $th) {
            DB::rollback();
                throw $th;

            }



    }



    public function edit($id)

    {

        $lecturers= Lecturer::findOrFail($id);





        return view('registration.lecturers.edit', compact('lecturers'));

    }



    public function update(Request $request)

    {

        $this->validate($request, [

            'salutation' => 'required',

            'name' => 'required|max:100',

            'long_name' => 'required|max:250',

            'address_line1' => 'nullable|max:100',

            'address_line2' => 'nullable|max:100',

            'address_line3' => 'nullable|max:100',

            'telephone1' => 'nullable|min:9|max:12',

            'telephone2' => 'nullable|max:12',

            'mobile' => 'nullable|max:12',

            'fax' => 'nullable|max:12',

            'email' => 'required|email|max:200',

            'note' => 'nullable|max:255',

            'photo' => 'nullable|max:200',


        ]);





            try {

                DB::beginTransaction();
                $lecturer = (new Lecturer())->find($request->id);

                $lecturer->update($request->all());




                DB::commit();
                return redirect('lecturers/create')->with('alert', 'Save Success!');
            } catch (Exception $ex) {

                DB::rollback();
                return $ex;

            }



    }

}

