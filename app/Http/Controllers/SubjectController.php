<?php

namespace CRA\Http\Controllers;

use CRA\Subject;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SubjectController extends Controller
{
    public function index()
    {
        $subjects = Subject::all();

        return view('registration.subjects.index', compact('subjects'));
    }

    public function create()
    {
        $subjects =  (new Subject());

        $new_index = Subject::max('id') + 1;

        return view('registration.subjects.create', compact('subjects', 'new_index'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:100',
            'description' => 'required|max:255',

        ]);


            try {
                DB::beginTransaction();
                $degrees = array();

                $subject = new Subject();
                $subject['name'] = $request->name;
                $subject['description'] = $request->description;

                $subject->save();

//                foreach ($degrees as $key => $value) {
//                    array_push($degrees, $value);
//                }

              //  $user->degrees()->attach($degrees);
                DB::commit();
                return redirect()->back()->with('alert', 'Saved successfully!');
            } catch (\Throwable $th) {
                DB::rollback();
                throw $th;
            }

    }

    public function edit($id)
    {
        $subjects= Subject::findOrFail($id);


        return view('registration.subjects.edit', compact('subjects'));
    }

    public function update(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:100',
            'description' => 'required|max:255',

        ]);


            try {

                DB::beginTransaction();
                $subject = Subject::findOrFail($request->id);
                $subject['name'] = $request['name'];
                $subject['description'] = $request['description'];

                $subject->update();
                DB::commit();
                return redirect()->back()->with('alert', 'Saved successfully!');
            } catch (\Throwable $th) {
                DB::rollback();
                throw $th;
            }

    }
    public function getSubjectList()
    {
        $subjects = Subject::select('id', 'name')->get();

        return response()->json($subjects, 200);
    }

}
