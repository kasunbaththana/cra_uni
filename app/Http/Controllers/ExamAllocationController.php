<?php

namespace CRA\Http\Controllers;

use Carbon\Carbon;
use CRA\Batches;
use CRA\Buildings;
use CRA\ClassFacilities;
use CRA\ClassRooms;
use CRA\ExamAllocation;
use CRA\ExamAllocationLecture;
use CRA\ExamAllocationSchedules;
use CRA\Lecturer;
use CRA\RoomAllocation;
use CRA\RoomAllocationSchedule;
use CRA\Util\ApplicationVarible;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Mockery\Exception;

class ExamAllocationController extends Controller
{
    public function index(){

        $exam_allocation = (new ExamAllocation())->with('hallId')->get();
        $today = date("Y-m-d");
        $buildings = (new Buildings())->pluck('name','id');

        return view('transaction.exam_allocation.index', compact('exam_allocation','today','buildings'));

    }
    public function dataFind(Request $request){


//        $data = DB::table('exam_allocations')
//            ->select(
//                'exam_allocations.id',
//                'exam_allocations.room_id',
//                'exam_allocations.batch_id',
//                'exam_allocations.from_date',
//                'exam_allocations.to_date',
//                'exam_allocations.from_time',
//                'exam_allocations.to_time',
//                'exam_allocations.status'
//
//            )
//            ->join('class_rooms','class_rooms.id','=','exam_allocations.room_id')
//            ->where('exam_allocations.from_date','<=',$request->t_date)
//            ->where('exam_allocations.to_date','>=',$request->t_date)
//            ->orderBy('class_rooms.id')
//            ->get();

        $data = DB::table('exam_allocation_schedules')
            ->select(
                'exam_allocations.id',
                'exam_allocations.hall_id',
                'batches.name as batch',
                'exam_allocation_schedules.from_time',
                'exam_allocation_schedules.to_time',
                'exam_allocations.status',
                'subjects.name as subject',

                DB::raw( '"" AS batch_name'),
                DB::raw( 'gradeCalculation(`exam_allocation_schedules`.`from_time`) AS grade'),

                DB::raw('IF(
                        `exam_allocation_schedules`.`to_time` > "8:00" && `exam_allocation_schedules`.`to_time` <= "12:00",
                        "A",
                        ""
                      ) AS S1'),
                DB::raw('IF(
                        `exam_allocation_schedules`.`to_time` > "12:30" && `exam_allocation_schedules`.`to_time` <= "17:30" ,
                        "B",
                        ""
                      ) AS S2'),


                DB::raw( 'CASE
                   WHEN `exam_allocation_schedules`.`to_time` < "12:30" THEN 1
                   WHEN `exam_allocation_schedules`.`to_time` < "17:30" && `exam_allocation_schedules`.`to_time` >= "12:30" THEN 2
                  END ending')

            )
            ->join('exam_allocations','exam_allocation_schedules.allocation_id','=','exam_allocations.id')
            ->join('halls','halls.id','=','exam_allocations.hall_id')
            ->join('floors','floors.id','=','halls.floor_id')
            ->join('buildings','buildings.id','=','floors.building_id')
            ->join('batches','batches.id','=','exam_allocation_schedules.batch_id')
            ->join('subjects','subjects.id','=','exam_allocation_schedules.subject_id')
            ->where('exam_allocation_schedules.schedule_date','=',$request->t_date)
            ->where('exam_allocation_schedules.leave','=',"0")
            ->where('buildings.id','=',$request->building_id)
            ->orderBy('halls.id')
            ->orderBy('exam_allocation_schedules.from_time')
            ->get();



        $rooms = DB::table('halls')
            ->select(
                'halls.*'

            )
            ->join('floors','floors.id','=','halls.floor_id')
            ->join('buildings','buildings.id','=','floors.building_id')
            ->where('buildings.id','=',$request->building_id)
            ->orderBy('halls.name')
            ->get();


       return compact('data','rooms');
    }

    public function create(){


        $exam_allocation = new ExamAllocation();
        $new_index =(new ExamAllocation())->max('id')+1;

        $batches = (new Batches())->pluck('name','id');

        $transaction_type = ApplicationVarible::$EXAM_ALLOCATION;

        $title = "";
//        $head = ['Name','Type','Capacity','Hall'];
//        $data = ($this->getColumnData());
        $today = date("Y-m-d");
        $items = (new Lecturer())->selectRaw('concat(salutation," ",name) as name_, id')->pluck('name_','id');
        $set_items = [];



        return view('transaction.exam_allocation.create',compact('exam_allocation','new_index','batches','transaction_type','title','set_items','items'));

    }
    public function getItems(Request $request)
        {
            $items = (new Lecturer())->find($request->code);
            return compact('items');
        }


    function getHallsColumnData(){

            $class_rooms = DB::table('halls')->
                join('floors','floors.id','=','halls.floor_id')->
                where('halls.is_exam_hall','=','1')->
                select('halls.*','floors.name as floor')->
                groupBy('halls.id')->
                get();
            $data = [];
            if(sizeof($class_rooms)>0) {
                foreach ($class_rooms as $list) {

                    $data[] =
                        [
                            $list->id,
                            $list->name,
                            $list->floor,

                        ];
                }
            }


        return $data;

    }

    public function dataHall(Request $request){

        //dd($request->chk_facility);
        $head[] = ['title'=>'#'];
        $head[] = ['title'=>'Name'];
        $head[] = ['title'=>'Floor'];

        $data = ($this->getHallsColumnData());

        $title = "Hall";



        return compact('head','data','title');
    }

    public function dataBatch(){
        $head[] = ['title'=>'#'];
        $head[] = ['title'=>'Name'];
        $head[] = ['title'=>'Degree'];
        $head[] = ['title'=>'Capacity'];
        $head[] = ['title'=>'Type'];
        $head[] = ['title'=>'Year'];

        $data = ($this->getBatchColumnData());

        $title = "Batch";

        return compact('head','data','title');

    }
    function getBatchColumnData(){

        $batches = (new Batches())->with('degreeId')->get();
        $data = [];

        foreach ($batches as $list){

            $data[] =
                [
                    $list->id,
                    $list->name,
                    $list->degreeId->name,
                    $list->capacity,
                    $list->type,
                    $list->year,


                ];
        }

        return $data;

    }

    public function dataFacility(Request $request){


        $facility = (new ClassFacilities())
            ->with('facilityId')
            ->where('class_room_id',$request->id)
            ->get();



        return compact('facility');
    }
//
//    function getRoomColumnData(){
//
//        $data = (new ClassRooms())->get();
//
//        return $data;
//
//    }
//
//    public function dataRoom(){
//
//        $head = ['Name','Type','Capacity','Hall'];
//
//        $data = ($this->getRoomColumnData());
//
//
//
//        return compact('head','data');
//    }

        public function resValidation(Request $request){


            $datetime_from = (new Carbon($request->from_time))->format('H:i:s');
            $datetime_to = (new Carbon($request->to_time))->format('H:i:s');



            $validate = (new ExamAllocationSchedules())->
                         selectRaw('count(exam_allocation_schedules.id) as num,exam_allocation_schedules.schedule_date as res_date')->
                         join('exam_allocations', 'exam_allocations.id', '=', 'exam_allocation_schedules.allocation_id')->

                         where('exam_allocation_schedules.schedule_date', $request->from_date)->
                         where('exam_allocation_schedules.batch_id', $request->h_batch_id)->
                        where(function ($query) use ($datetime_from,$datetime_to) {
                        $query->whereRaw('? BETWEEN exam_allocation_schedules.from_time AND exam_allocation_schedules.to_time',$datetime_from);
                        $query-> orwhereRaw('? BETWEEN exam_allocation_schedules.from_time AND exam_allocation_schedules.to_time',$datetime_to);
                        $query->orWhereBetween('exam_allocation_schedules.from_time',[$datetime_from,$datetime_to]);
                        $query->orWhereBetween('exam_allocation_schedules.to_time',[$datetime_from,$datetime_to]);
                            })->
                         first();


                return $validate->num;


        }
        public function save(Request $request){


           $this->validate(Request(),
                [
                    'hall_id' => 'required',
                    'h_hall_id' => 'required',

                ]
            );

        //   dd($request->all());

              try{

                     if(!isset($request->schedule) && !sizeof($request->schedule)>0){
                        return ["Generate Schedule and try again"];
                    }else {

                         $sch_list = json_decode($request->schedule, true);
                         $date_check=[];
                        $i=0;

                         foreach ($sch_list as $key => $list) {

                             if(!$list[3]){


                                 $date_check[$i] = $list[0];
                                 $i++;
                             }



                         }

                         $datetime_from = (new Carbon($request->from_time))->addMinutes(1)->format('H:i:s');
                         $datetime_to = (new Carbon($request->to_time))->subMinutes(1)->format('H:i:s');



//                         $validate = (new ExamAllocationSchedules())->
//                         selectRaw('count(exam_allocation_schedules.id) as num,exam_allocation_schedules.schedule_date as res_date')->
//                         join('exam_allocations', 'exam_allocations.id', '=', 'exam_allocation_schedules.allocation_id')->
//                         where('exam_allocations.hall_id', $request->h_hall_id)->
//                         where(function ($query) use ($request,$date_check) {
//                             //$query->whereBetween('exam_allocation_schedules.schedule_date', [$request->from_date, $request->to_date]);
//                             $query->whereIn('exam_allocation_schedules.schedule_date', $date_check);
//                         })->
//                         where(function ($query) use ($datetime_from,$datetime_to) {
//                             $query->whereBetween('exam_allocation_schedules.from_time', [ $datetime_from, $datetime_to]);
//                             $query->orwhereBetween('exam_allocation_schedules.to_time', [ $datetime_from, $datetime_to]);
//                         })->
//                         first();



                       //  if (intval($validate->num) > 0) {

                         //    return ["Exam hall already reserve ! ".$validate->res_date];

                        // } else {
                             DB::beginTransaction();


                             $index = (new ExamAllocation())->store($request);

                             foreach ($sch_list as $key => $list) {

                                     $schedule = new ExamAllocationSchedules();
                                     $schedule->allocation_id = $index->id;
                                     $schedule->schedule_date = $list[0];
                                     $schedule->from_time = $list[1];
                                     $schedule->to_time = $list[2];
                                     $schedule->batch_id = $list[3];
                                     $schedule->subject_id = $list[4];

                                     $schedule->save();


                             }


                             if(isset($request->tbl_item)){
                                 foreach ($request->tbl_item as $key => $list) {

                                     $lecture = new ExamAllocationLecture();
                                     $lecture->allocation_id = $index->id;
                                     $lecture->lecture_id = $key;
                                     $lecture->save();
                                 }

                             }


                             DB::commit();

                         return ["save", '/exam_allocation'];
                       //  }
                     }
              }catch (Exception $ex){
                  DB::rollback();
              }
        }
    public function edit($code)
    {


        $exam_allocation = (new ExamAllocation())->with('hallId')->find($code);

        $set_items = (new ExamAllocationLecture())->with('lectureId')->where('allocation_id',$exam_allocation->id)->get();

        $schedule = (new ExamAllocationSchedules())->where('allocation_id',$exam_allocation->id)->with('batchId','subjectId')->orderBy('schedule_date')->get();

        $batches = (new Batches())->pluck('name','id');

        $transaction_type = ApplicationVarible::$EXAM_ALLOCATION;

        $items = (new Lecturer())->selectRaw('concat(salutation," ",name) as name_, id')->pluck('name_','id');
        $title = "";

        $i = 0;

        return view('transaction.exam_allocation.edit',compact('exam_allocation','batches','transaction_type','title','set_items','schedule','i','items'));



    }
    public function update(Request $request, $code)
    {
        $this->validate(Request(),
            [
                'hall_id' => 'required',
                'h_hall_id' => 'required',

            ]
        );




        try{

            if(!isset($request->schedule) && !sizeof($request->schedule)>0){
                return ["Generate Schedule and try again"];
            }else {
                $this->removeScheduleData($request);

                $sch_list = json_decode($request->schedule, true);
                $date_check = [];
                $i = 0;

                foreach ($sch_list as $key => $list) {

                    if (!$list[3]) {


                        $date_check[$i] = $list[0];
                        $i++;
                    }


                }

//                $validate = (new RoomAllocationSchedule())->
//                selectRaw('count(exam_allocation_schedules.id) as num,exam_allocation_schedules.schedule_date as res_date')->
//                join('exam_allocations', 'exam_allocations.id', '=', 'exam_allocation_schedules.allocation_id')->
//                where('exam_allocations.room_id', $request->h_room_id)->
//                where(function ($query) use ($request, $date_check) {
//                    //$query->whereBetween('exam_allocation_schedules.schedule_date', [$request->from_date, $request->to_date]);
//                    $query->whereIn('exam_allocation_schedules.schedule_date', $date_check);
//                })->
//                where(function ($query) use ($request) {
//                    $query->whereBetween('exam_allocation_schedules.from_time', [$request->from_time, $request->to_time]);
//                    $query->orwhereBetween('exam_allocation_schedules.to_time', [$request->from_time, $request->to_time]);
//                })->
//                first();

//                if (intval($validate->num) > 0) {
//
//                    return ["Cannot reserve Something went wrong !"];
//
//                } else {

                    DB::beginTransaction();

//                $class_r = [];
//                $rm_update = (new RoomAllocation())->find($request->id);



                    $rm = (new ExamAllocation())->find($request->id);
                      $fill_table = [
                                'h_hall_id' => $request->h_hall_id,
                                'status' => "P",
                            ];



                    $rm->update($fill_table);

                    $sch_list = json_decode($request->schedule, true);




                    foreach ($sch_list as $key => $list) {

                        $schedule = new ExamAllocationSchedules();
                        $schedule->allocation_id = $request->id;
                        $schedule->schedule_date = $list[0];
                        $schedule->from_time = $list[1];
                        $schedule->to_time = $list[2];
                        $schedule->batch_id = $list[3];
                        $schedule->subject_id = $list[4];
                        $schedule->leave = 0;
                        $schedule->save();
                    }

                    DB::commit();
                    return ["save", '/exam_allocation'];

//                }

          }
        }catch (Exception $ex){
            DB::rollback();
        }


    }

    function removeScheduleData(Request $request){

            $data = (new ExamAllocationSchedules())->where('allocation_id',$request->id);

           return $data->delete();

    }
    function capacityCheck(Request $request){

        dd($request->all());

    }




}
