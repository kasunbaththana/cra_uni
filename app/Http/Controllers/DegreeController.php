<?php

namespace CRA\Http\Controllers;

use CRA\BatchSubject;
use CRA\Degrees;
use CRA\DegreeSubject;
use CRA\Department;
use CRA\Subject;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Mockery\Exception;

class DegreeController extends Controller
{
    public function index(){

        $degrees = (new Degrees())->with('departmentId')->get();

        return view('registration.degrees.index', compact('degrees'));

    }
    public function create()
    {

        $degrees = new Degrees();

        $new_index =(new Degrees())->count('id')+1;

        $departments = (new Department())->orderBy('name')->pluck('name','id');

        $subject = [];
        $subject_d = [];
        return view('registration.degrees.create', compact('degrees','subject','subject_d','departments','new_index'));

    }
    public function edit($code)
    {


        $degrees = (new Degrees())->find($code);
        $departments = (new Department())->pluck('name','id');
        $subject = (new DegreeSubject())->where('degree_id',$code)->get();
        $subject_d = (new Subject())->pluck('name','id');


        return view('registration.degrees.edit', compact('degrees','departments','subject','subject_d'));

    }
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:200|unique:degrees',
            'prefix' => 'required|max:10|unique:degrees',
        ]);


        try {

            $subjects = array();

            foreach ($request->all() as $key => $value) {
                if (substr($key,0, 7) == "subject") {
                    array_push($subjects, $value);
                }
            }





            DB::beginTransaction();

            $degrees = new Degrees($request->all());
            $degrees->is_weekend =$request->is_weekend == 'on' ? '1' : '0';
            $degrees->is_weekday =$request->is_weekday == 'on' ? '1' : '0';

            $degrees->save();

            $degreeId = $degrees->id;

            foreach ($subjects as $subject) {
                $degree_subject = new DegreeSubject();
                $degree_subject['degree_id'] = $degreeId;
                $degree_subject['subject_id'] = $subject;
                $degree_subject->save();
            }



            DB::commit();
            return redirect('lecturers')->with('alert', 'Save Success!');
        } catch (Exception $ex) {
            DB::rollback();
            return $ex;
        }

    }
    public function getSubjects(Request $request)
    {

        $subjects = DegreeSubject::where('degree_id', $request->id)->with('subject')->get();

        return response()->json($subjects, 200);
    }

    public function update(Request $request, $code)
    {

        $this->validate($request, [

            'name' => 'required|max:200|unique:degrees,name,'.$request->id,
            'prefix' => 'required|max:10|unique:degrees,prefix,'.$request->id,
        ]);


        try {

            $subjects = array();

            foreach ($request->all() as $key => $value) {
                if (substr($key,0, 7) == "subject") {
                    if($value!=null){
                        array_push($subjects, $value);
                    }

                }
            }


            $this->deleteDegreeSubject($request->id);
            $degrees = (new Degrees())->find($request->id);
            $degrees->name = $request->name;
            $degrees->department_id = $request->department_id;
            $degrees->department_id = $request->department_id;
            $degrees->prefix = $request->prefix;
            $degrees->is_weekend =isset($request->is_weekend) ? '1' : '0';
            $degrees->is_weekday =isset($request->is_weekday) ? '1' : '0';
            $degrees->update();

            $degreeId = $degrees->id;

            foreach ($subjects as $subject) {
                $degree_subject = new DegreeSubject();
                $degree_subject['degree_id'] = $degreeId;
                $degree_subject['subject_id'] = $subject;
                $degree_subject->save();
            }


            return redirect('degree')->with('alert', 'Updated!');
        } catch (Exception $e) {
            return $e;
        }


    }

    public function deleteDegreeSubject($degree_id){


            $subject =(new DegreeSubject())->where('degree_id',$degree_id);
            $subject->delete();
    }
}
