<?php

namespace CRA\Http\Controllers;

use CRA\UserRole;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Mockery\Exception;

class UserRoleController extends Controller
{
    public function index(){

        $users = (new UserRole())->get();

        return view('registration.users.roles.index', compact('users'));

    }
    public function create()
    {

        $users = new UserRole();

        $new_index =(new UserRole())->count('id')+1;

        $users_list = (new UserRole())->get();

    #    $buildings = (new Buildings())->orderBy('name')->pluck('name','id');

        return view('registration.users.roles.create', compact('users','new_index','users_list'));

    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:30|unique:pro_user_role'

        ]);


        try {

            DB::beginTransaction();

            $new_index =(new UserRole())->count('priority')+1;

            $department = new UserRole($request->all());
            $department->priority = $new_index;
            $department->save();


            DB::commit();
            return redirect('users_role/create')->with('alert', 'save!');
        } catch (Exception $ex) {
            DB::rollback();
            return $ex;
        }

    }
    public function edit($code)
    {


        $users= (new UserRole())->find($code);

        $users_list = (new UserRole())->get();
        return view('registration.users.roles.edit', compact('users','users_list'));

    }
    public function update(Request $request, $code)
    {

        $this->validate($request, [

            'name' => 'required|max:30|unique:pro_user_role,name,'.$request->id

        ]);

        try {

            $users = (new UserRole())->find($request->id);
            $users->update($request->all());

            return redirect('users_role')->with('alert', 'Updated!');
        } catch (Exception $e) {
            return $e;
        }


    }
}
