<?php

namespace CRA\Http\Controllers;

use CRA\Branch;
use CRA\CashierSession;
use CRA\Employee;
use CRA\Permission;
use CRA\User;
use CRA\UserLogin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Mockery\Exception;

class UserController extends Controller
{

    public function dashboard(){


        return view('index');
    }
    public function login(Request $request)
    {

        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required',
        ]);


        $credential = [
            'email' => $request->email,
            'password' => $request->password,
        ];




        if(Auth::attempt($credential)){

            $emp_user = (new User())->where('email', $request->email)->first();

//dd($emp_user);
            Session::put('user_id', $emp_user->id);
            Session::put('user_name', $emp_user->name);
            Session::put('user_role', $emp_user->user_role);

            return redirect('dashboard');

        }else{
            return redirect()->back()->withInput($request->only('user_name', 'remember'))->withErrors([
                'password_fail' => 'Wrong username or password !',
            ]);
        }



    }


    public function logout(Request $request)
    {


       //// Auth::guard('admin')->logout();
        Auth::logout();
            Session::flush();
        
         $request->session()->invalidate();
        $request->session()->regenerate();
      //  return redirect('main_page');  
    $request->session()->flash('status', 'Task was successful!');

       

        return redirect('/main_page');
    }

    public function store(Request $request)
    {
        // $validated = $request->validate()

        // $new_user = new User();




        

        // ERROR:::::> Correctly pass the permissions with user data
        // TODO: restrict this method only for the admin roles
    }

}
