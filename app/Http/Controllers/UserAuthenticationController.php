<?php

namespace CRA\Http\Controllers;

use CRA\User;
use CRA\UserRole;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Mockery\Exception;

class UserAuthenticationController extends Controller
{

    public function index(){

        $users = (new User())->get();

        return view('registration.users.index', compact('users'));

    }
    public function create()
    {

        $users = new User();

        $new_index =(new User())->count('id')+1;

        $roles =(new UserRole())->where('priority','>=',$this->findAuthPriority())->pluck('name','id');
        $roles->prepend("Please Select",0);

     
        return view('registration.users.create', compact('users','new_index','roles'));

    }

    public function findAuthPriority(){

        $user = (new User())->with('userRole')->find(Auth::id());


        return $user->userRole->priority;
    }
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:30',
            'email' => 'required|unique:users|max:255|email',
            'password' => 'required|confirmed|min:6',
            'password_confirmation' => 'required|min:6'
        ]);


        try {

            DB::beginTransaction();


            $users = new User($request->all());
            $users->password = Hash::make($request->password);
            $users->save();


            DB::commit();
            return redirect('users/create')->with('alert', 'save!');
        } catch (Exception $ex) {
            DB::rollback();
            return $ex;
        }

    }
    public function edit($id)
    {
        $users= (New User())->with('userRole')->findOrFail($id);



        $roles =(new UserRole())->
        where('priority','>=',$this->findAuthPriority())->
        pluck('name','id');
        $roles->prepend("Please Select",0);
        return view('registration.users.edit', compact('users','roles'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required|max:30',
            'email' => 'required|unique:users,email,'.$request->id,
            'password' => 'required|confirmed|min:6',
            'password_confirmation' => 'required|min:6'

        ]);


            try {
                $users = User::findOrFail($request->id);


                $request->merge([
                    'password' => Hash::make($request->password)
                ]);

          
                $users->update($request->all());



                return redirect('users')->with('alert', 'Update Success!');
            } catch (Exception $th) {
                return $th;
            }

    }

    public function reset(Request $request)
    {


    }

}
