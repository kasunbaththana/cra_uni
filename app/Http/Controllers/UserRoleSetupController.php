<?php

namespace CRA\Http\Controllers;

use CRA\UserPermission;
use CRA\UserRole;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Mockery\Exception;

class UserRoleSetupController extends Controller
{
    public function index()
    {

        $users = new UserPermission();

        $new_index =(new UserPermission())->count('id')+1;

        $roles = (new UserRole())->pluck('name','id');

        return view('registration.users.role_setup.create', compact('users','new_index','roles'));

    }
    public function viewPermission(Request $request){




        $data1 = DB::table('pro_user_module')
            ->select('pro_user_module.id AS id',
                'pro_user_module.name AS description',
                DB::raw('0'),
                DB::raw('0'),
                DB::raw('0'),
                DB::raw('0'),
                DB::raw('0'))
            ->whereRaw('pro_user_module.id NOT IN 
                        ( SELECT  pro_user_permissions.module_id 
                        FROM pro_user_permissions 
                        WHERE pro_user_permissions.role_id = ? )',$request->code);


        $data = DB::table('pro_user_module')
            ->select('pro_user_module.id AS id',
                'pro_user_module.name AS description',
                DB::raw('pro_user_permissions.view'),
                DB::raw('pro_user_permissions.add'),
                DB::raw('pro_user_permissions.edit'),
                DB::raw('pro_user_permissions.delete'),
                DB::raw('pro_user_permissions.print'))
            ->join('pro_user_permissions','pro_user_permissions.module_id','=','pro_user_module.id','inner')
            ->where('pro_user_permissions.role_id','=',$request->code)
            ->groupBy('pro_user_module.id')
            ->union($data1)
            ->orderBy('description')->get();



        return compact('data');

    }

    public function store(Request $request){

        try{
            DB::beginTransaction();
            $user_role = (new UserPermission())->where('role_id',$request->role_id);
            $user_role->delete();



            foreach ($request->module_id as $key=>$val){


                $is_view = "is_view_".$key;
                $is_add = "is_add_".$key;
                $is_edit = "is_edit_".$key;
                $is_delete = "is_delete_".$key;
                $is_print = "is_print_".$key;


                if(($request->$is_view == "on")){

                    $data=[
                        'role_id'=>$request->role_id,
                        'module_id'=>$val,
                        'view'=>$request->$is_view == "on" ? "1":"0",
                        'add'=>$request->$is_add == "on" ? "1":"0",
                        'edit'=>$request->$is_edit == "on" ? "1":"0",
                        'delete'=>$request->$is_delete == "on" ? "1":"0",
                        'print'=>$request->$is_print == "on" ? "1":"0",
                    ];

                    (new UserPermission())->create($data);

                }
            }
            DB::commit();
                return ["save","/user_role_setup"];
        }catch(Exception $exception){
            DB::rolleBack();


            return compact("".$exception);
        }

    }
}
