<?php

namespace CRA\Http\Controllers;


use Carbon\Carbon;
use CRA\Batches;
use CRA\ClassRooms;
use CRA\ReportPermissions;
use CRA\Reports;
use CRA\Subject;
use CRA\Util\Common;
use Faker\Provider\DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Mockery\Exception;
use PDF;

class ReportGenerateController extends Controller
{


    public function reportPdf(Request $request){

    try{



        $reports = (new Reports())->find($request->report_id);


        switch ($reports->path){

            case "rpt_subject_list":
                $this->rpt_subject_list($request);
                break;
            case "rpt_class_room_allocation":
                $this->rpt_class_room_allocation($request);
                break;
            case "rpt_class_room_allocation_batch_wise":
                $this->rpt_class_room_allocation_batch_wise($request);
                break;
            case "rpt_class_room_allocation_summary":
                $this->rpt_class_room_allocation_summary($request);
                break;
            case "rpt_total_class_room_valuation_summery":
                $this->rpt_total_class_room_valuation_summery($request);
                break;
            case "rpt_class_room_allocation_summary_weekly":
                $this->rpt_class_room_allocation_summary_weekly($request);
                break;
            case "rpt_class_room_allocation_summary_weekend":
                $this->rpt_class_room_allocation_summary_weekend($request);
                break;
        }


    }catch(Exception $exception){
        dd($exception);
    }


}

    public function rpt_subject_list(Request $request){

        try{



            $reports = (new Reports())->find($request->report_id);


            $data = (new Subject())->get();


            $pdf = PDF::loadView('pdf.'.$reports->path, compact('data'),[],[
                'format'=> 'A4',
                'default_font'=> 'sans-serif',
                'orientation'=> 'P',
            ]);


            return $pdf->stream($reports->path.'.pdf');



        }catch(Exception $exception){
            dd($exception);
        }


    }



    public function rpt_class_room_allocation(Request $request){

        try{



            $reports = (new Reports())->find($request->report_id);


            $data =  DB::select("SELECT  
                            b.`name` AS batch,c.`id`,c.`name` as class_room,s.`schedule_date`,
                            s.`from_time`,s.`to_time`,sb.`name` AS sub_name,DAYNAME(s.`schedule_date`) AS wk
                         FROM `room_allocations` r 
                        INNER JOIN `room_allocation_schedules` s ON s.`allocation_id` = r.`id`
                        INNER JOIN `class_rooms` c ON c.`id` = r.`room_id`
                        INNER JOIN `batches` b ON r.`batch_id` =b.`id` 
                        INNER JOIN `subjects` sb ON sb.`id` = r.`subject_id`
                        
                        WHERE s.`schedule_date` BETWEEN :from_date AND :to_date
                        
                        
                        ORDER BY b.`id`,c.`id`,s.`schedule_date`", ['from_date' => $request->from_date,'to_date'=>$request->to_date]);
            $param =[];
            $param['from_date'] = $request->from_date;
            $param['to_date'] = $request->to_date;

            $pdf = PDF::loadView('pdf.'.$reports->path, compact('data','param'),[],[
                'format'=> 'A4',
                'default_font'=> 'sans-serif',
                'orientation'=> 'P',
            ]);




            return $pdf->stream($reports->path.'.pdf');



        }catch(Exception $exception){
            dd($exception);
        }


    }
    public function rpt_class_room_allocation_batch_wise(Request $request){

        try{



            $reports = (new Reports())->find($request->report_id);


            $data =  DB::select("SELECT  
                            b.`name` AS batch,c.`id`,c.`name` as class_room,s.`schedule_date`,
                            s.`from_time`,s.`to_time`,sb.`name` AS sub_name,DAYNAME(s.`schedule_date`) AS wk
                         FROM `room_allocations` r 
                        INNER JOIN `room_allocation_schedules` s ON s.`allocation_id` = r.`id`
                        INNER JOIN `class_rooms` c ON c.`id` = r.`room_id`
                        INNER JOIN `batches` b ON r.`batch_id` =b.`id` 
                        INNER JOIN `subjects` sb ON sb.`id` = r.`subject_id`
                        WHERE s.`schedule_date` BETWEEN :from_date AND :to_date AND r.batch_id=:batch_id
                        ORDER BY b.`id`,c.`id`,s.`schedule_date`", ['from_date' => $request->from_date,'to_date'=>$request->to_date,'batch_id'=>$request->batch_id]);

            $param =[];
            $param['from_date'] = $request->from_date;
            $param['to_date'] = $request->to_date;

            $pdf = PDF::loadView('pdf.'.$reports->path, compact('data','param'),[],[
                'format'=> 'A4',
                'default_font'=> 'sans-serif',
                'orientation'=> 'P',
            ]);




            return $pdf->stream($reports->path.'.pdf');



        }catch(Exception $exception){
            dd($exception);
        }


    }
    public function rpt_class_room_allocation_summary(Request $request){
        try{



            $reports = (new Reports())->find($request->report_id);


            $data =  DB::select("SELECT * FROM (SELECT d.`id`,d.`class_room` AS class_room,
                    DAYNAME(d.`schedule_date`) AS wk,
                    IF(DAYNAME(d.`schedule_date`)='Sunday',
                   CONCAT(d.`schedule_date`,' ',d.`batch`,' ',d.`sub_name`,' ',d.`from_time`,' to ',d.`to_time`),'') AS Sunday,
                   
                   IF(DAYNAME(d.`schedule_date`)='Saturday',
                    CONCAT(d.`schedule_date`,' ',d.`batch`,' ',d.`sub_name`,' ',d.`from_time`,' to ',d.`to_time`),'') AS Saturday,
                    
                    IF(DAYNAME(d.`schedule_date`)='Monday',
                    CONCAT(d.`schedule_date`,' ',d.`batch`,' ',d.`sub_name`,' ',d.`from_time`,' to',d.`to_time`),'') AS Monday,
                    
                    IF(DAYNAME(d.`schedule_date`)='Tuesday',
                    CONCAT(d.`schedule_date`,' ',d.`batch`,' ',d.`sub_name`,' ',d.`from_time`,' to ',d.`to_time`),'') AS Tuesday,
                    
                    IF(DAYNAME(d.`schedule_date`)='Wednesday',
                    CONCAT(d.`schedule_date`,' ',d.`batch`,' ',d.`sub_name`,' ',d.`from_time`,' to ',d.`to_time`),'') AS Wednesday,
                    
                    IF(DAYNAME(d.`schedule_date`)='Thursday',
                    CONCAT(d.`schedule_date`,' ',d.`batch`,' ',d.`sub_name`,' ',d.`from_time`,' to ',d.`to_time`),'') AS Thursday,
                    
                    IF(DAYNAME(d.`schedule_date`)='Friday',
                    CONCAT(d.`schedule_date`,' ',d.`batch`,' ',d.`sub_name`,' ',d.`from_time`,' to ',d.`to_time`),'') AS Friday
                   
                     FROM 
                    (SELECT
                    b.`name` AS batch,c.`id`,c.`name` AS class_room,s.`schedule_date`,
                    s.`from_time`,s.`to_time`,sb.`name` AS sub_name,DAYNAME(s.`schedule_date`) AS wk
                     FROM `room_allocations` r
                    INNER JOIN `room_allocation_schedules` s ON s.`allocation_id` = r.`id`
                    INNER JOIN `class_rooms` c ON c.`id` = r.`room_id`
                    INNER JOIN `batches` b ON r.`batch_id` =b.`id`
                    INNER JOIN `subjects` sb ON sb.`id` = r.`subject_id`
                    WHERE s.`schedule_date`  BETWEEN :from_date AND :to_date and r.status='O' 
                        ORDER BY s.`schedule_date`) AS d
                    
                     GROUP BY d.`id`,d.`schedule_date`
                    
                    ORDER BY d.`id`,d.`id`,d.`schedule_date` ) AS c  ORDER BY 
                c.Monday,c.Tuesday,c.Wednesday,c.Thursday,c.Friday,c.Saturday,c.Sunday", ['from_date' => $request->from_date,'to_date'=>$request->to_date]);

            $rooms= (new ClassRooms())->get();

            $param =[];
            $param['from_date'] = $request->from_date;
            $param['to_date'] = $request->to_date;

            $pdf = PDF::loadView('pdf.'.$reports->path, compact('data','param','rooms'),[],[
                'format'=> 'A3',
                'default_font'=> 'sans-serif',
                'orientation'=> 'L',
            ]);




            return $pdf->stream($reports->path.'.pdf');



        }catch(Exception $exception){
            dd($exception);
        }

    } 
    public function rpt_class_room_allocation_summary_weekly(Request $request){
        try{



            $reports = (new Reports())->find($request->report_id);


            $data =  DB::select("SELECT s.batch_id as batch_id,s.batch,s.wk,replace(GROUP_CONCAT(s.Monday,'\n'),',','') AS Monday,replace(GROUP_CONCAT(s.Tuesday,'\n'),',','') AS Tuesday,replace(GROUP_CONCAT(s.Wednesday,'\n'),',','') AS Wednesday  
                                    ,replace(GROUP_CONCAT(s.Thursday,'\n'),',','') AS Thursday  ,replace(GROUP_CONCAT(s.Friday,'\n'),',','') AS Friday 
                            FROM (
                            SELECT d.batch_id,d.`batch` AS batch,
                                                DAYNAME(d.`schedule_date`) AS wk,
                                           
                                                
                                                IF(DAYNAME(d.`schedule_date`)='Monday',
                                                CONCAT(d.`schedule_date`,'\n ',d.`class_room`,'\n ',d.`sub_name`,'\n ',d.`from_time`,'-',d.`to_time`),'  ') AS Monday,
                                                
                                                IF(DAYNAME(d.`schedule_date`)='Tuesday',
                                                CONCAT(d.`schedule_date`,'\n ',d.`class_room`,'\n ',d.`sub_name`,'\n ',d.`from_time`,'-',d.`to_time`),'  ') AS Tuesday,
                                                
                                                IF(DAYNAME(d.`schedule_date`)='Wednesday',
                                                CONCAT(d.`schedule_date`,'\n ',d.`class_room`,'\n ',d.`sub_name`,'\n ',d.`from_time`,'-',d.`to_time`),'  ') AS Wednesday,
                                                
                                                IF(DAYNAME(d.`schedule_date`)='Thursday',
                                                CONCAT(d.`schedule_date`,'\n ',d.`class_room`,'\n ',d.`sub_name`,'\n ',d.`from_time`,'-',d.`to_time`),'  ') AS Thursday,
                                                
                                                IF(DAYNAME(d.`schedule_date`)='Friday',
                                                CONCAT(d.`schedule_date`,'\n ',d.`class_room`,'\n ',d.`sub_name`,'\n ',d.`from_time`,'-',d.`to_time`),'  ') AS Friday
                                               
                                                 FROM 
                                                (SELECT
                                                b.id AS batch_id,b.`name` AS batch,c.`id`,c.`name` AS class_room,s.`schedule_date`,r.id AS al_id,
                                                s.`from_time`,s.`to_time`,sb.`name` AS sub_name,DAYNAME(s.`schedule_date`) AS wk
                                                 FROM `room_allocations` r
                                                INNER JOIN `room_allocation_schedules` s ON s.`allocation_id` = r.`id`
                                                INNER JOIN `class_rooms` c ON c.`id` = r.`room_id`
                                                INNER JOIN `batches` b ON r.`batch_id` =b.`id`
                                                INNER JOIN `subjects` sb ON sb.`id` = r.`subject_id`
                                                WHERE s.`schedule_date`  BETWEEN :from_date AND :to_date AND DAYNAME(s.`schedule_date`) NOT IN('Saturday','Sunday') and  r.status='O' 
                                                GROUP BY r.`batch_id`,r.id,DAYNAME(s.`schedule_date`)
                                                    ORDER BY s.`schedule_date`) AS d
                                                
                                                 GROUP BY d.`batch_id`,d.al_id,d.`wk`
                                                
                                                ORDER BY d.`id`,d.`id`,d.`schedule_date`) s 
                                                
                                                GROUP BY s.batch", ['from_date' => $request->from_date,'to_date'=>$request->to_date]);

            $rooms= (new Batches())->get();

            $param =[];
            $param['from_date'] = $request->from_date;
            $param['to_date'] = $request->to_date;

            $pdf = PDF::loadView('pdf.'.$reports->path, compact('data','param','rooms'),[],[
                'format'=> 'A3',
                'default_font'=> 'sans-serif',
                'orientation'=> 'L',
            ]);




            return $pdf->stream($reports->path.'.pdf');



        }catch(Exception $exception){
            dd($exception);
        }

    } 

    public function rpt_class_room_allocation_summary_weekend(Request $request){
        try{



            $reports = (new Reports())->find($request->report_id);


            $data =  DB::select("SELECT s.batch_id as batch_id,s.batch,s.wk,REPLACE(GROUP_CONCAT(s.Saturday,'\n'),',','') AS Saturday,REPLACE(GROUP_CONCAT(s.Sunday,'\n'),',','') AS Sunday
                            FROM (
                            SELECT d.batch_id,d.`batch` AS batch,
                                                DAYNAME(d.`schedule_date`) AS wk,
                                           
                                                
                                                 IF(DAYNAME(d.`schedule_date`)='Saturday',
                                                CONCAT(d.`schedule_date`,'\n ',d.`class_room`,'\n ',d.`sub_name`,'\n ',d.`from_time`,'-',d.`to_time`),'  ') AS Saturday,
                                                
                                                IF(DAYNAME(d.`schedule_date`)='Sunday',
                                                CONCAT(d.`schedule_date`,'\n ',d.`class_room`,'\n ',d.`sub_name`,'\n ',d.`from_time`,'-',d.`to_time`),'  ') AS Sunday
                                               
                                               
                                                 FROM 
                                                (SELECT
                                                b.id AS batch_id,b.`name` AS batch,c.`id`,c.`name` AS class_room,s.`schedule_date`,r.id AS al_id,
                                                s.`from_time`,s.`to_time`,sb.`name` AS sub_name,DAYNAME(s.`schedule_date`) AS wk
                                                 FROM `room_allocations` r
                                                INNER JOIN `room_allocation_schedules` s ON s.`allocation_id` = r.`id`
                                                INNER JOIN `class_rooms` c ON c.`id` = r.`room_id`
                                                INNER JOIN `batches` b ON r.`batch_id` =b.`id`
                                                INNER JOIN `subjects` sb ON sb.`id` = r.`subject_id`
                                                WHERE s.`schedule_date`  BETWEEN :from_date AND :to_date AND DAYNAME(s.`schedule_date`)  IN('Saturday','Sunday') and  r.status='O' 
                                                GROUP BY r.`batch_id`,r.id,DAYNAME(s.`schedule_date`)
                                                    ORDER BY s.`schedule_date`) AS d
                                                
                                                 GROUP BY d.`batch_id`,d.al_id,d.`wk`
                                                
                                                ORDER BY d.`id`,d.`id`,d.`schedule_date`) s 
                                                
                                                GROUP BY s.batch", ['from_date' => $request->from_date,'to_date'=>$request->to_date]);

            $rooms= (new Batches())->get();

            $param =[];
            $param['from_date'] = $request->from_date;
            $param['to_date'] = $request->to_date;

            $pdf = PDF::loadView('pdf.'.$reports->path, compact('data','param','rooms'),[],[
                'format'=> 'A3',
                'default_font'=> 'sans-serif',
                'orientation'=> 'L',
            ]);




            return $pdf->stream($reports->path.'.pdf');



        }catch(Exception $exception){
            dd($exception);
        }

    } 

     function rpt_total_class_room_valuation_summery(Request $request){
        try{



            $reports = (new Reports())->find($request->report_id);


            $data =  DB::select("
                    SELECT a.`room_id`,MONTHNAME(r.`schedule_date`) AS s_date, 
                    ROUND((SUM(TIME_TO_SEC(r.`to_time`))-SUM(TIME_TO_SEC(r.`from_time`)))/60/60,2) AS  time_dif
                    
                    FROM `room_allocation_schedules` r 
                    INNER JOIN `room_allocations` a ON a.`id` = r.`allocation_id`
                    INNER JOIN `class_rooms` c ON c.`id` = a.`room_id`
                    
                    WHERE c.id=:room_id AND r.`schedule_date`  BETWEEN :from_date AND :to_date AND a.status <> 'C'

                    GROUP BY YEAR(r.`schedule_date`),MONTH(r.`schedule_date`),a.`room_id`   order by month(r.`schedule_date`)
            ", ['from_date' => $request->from_date,'to_date'=>$request->to_date,'room_id'=>$request->room_id]);

            $rooms= (new ClassRooms())->find($request->room_id);

            $param =[];
            $param['from_date'] = $request->from_date;
            $param['to_date'] = $request->to_date;
            $param['class_room'] = $rooms->name;

            $pdf = PDF::loadView('pdf.'.$reports->path, compact('data','param','rooms'),[],[
                'format'=> 'A4',
                'default_font'=> 'sans-serif',
                'orientation'=> 'P',
            ]);




            return $pdf->stream($reports->path.'.pdf');



        }catch(Exception $exception){
            dd($exception);
        }

    }


    public function reportView(Request $request){


        $param =array();
        $name ="";
        $type = str_replace("/","",$request->getRequestUri());

        $reports = (new ReportPermissions())
            ->where('user_role_id',$request->session()->get('user_role'))
            ->where('view','1')->with('reportId')->get();

        $date = Carbon::now();
        $batches = (new Batches())->where('expired_date','>',$date)->pluck('name','id');
        $rooms = (new ClassRooms())->pluck('name','id');

        switch ($type){

            case  "loan_report" :

                $name="Loan Report";

                break;

            default :

        }

        $title = "";


        return view('reports.report',compact('name','type','reports','title','batches','rooms'));
    }



}
