<?php

namespace CRA\Http\Controllers;

use CRA\Batches;
use CRA\BatchSubject;
use CRA\BatchSubjectLecture;
use CRA\Lecturer;
use CRA\Subject;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Mockery\Exception;

class LectureAssignController extends Controller
{
    public function index()
    {

        $degrees = (new BatchSubject())->with('subjectId', 'batches')->orderBy('batch_id')->get();

        return view('registration.lecture_subject.index', compact('degrees'));

    }

    public function edit($code)
    {


        $degrees = (new BatchSubjectLecture())->where('batch_subject_id', $code)->get();
        $subject = (new BatchSubject())->find($code);
        $lectures = (new Lecturer())->pluck('name', 'id');


        $batches = (new Batches())->where('id', $subject->batch_id)->pluck('name', 'id');
        $subjects = (new Subject())->where('id', $subject->subject_id)->pluck('name', 'id');

        $items = (new Lecturer())->selectRaw('concat(salutation," ",name) as name_, id')->pluck('name_', 'id');
        $set_items = (new BatchSubjectLecture())->with('lectureId')->where('batch_subject_id', $code)->get();

        return view('registration.lecture_subject.edit', compact('items', 'set_items', 'degrees', 'subject', 'lectures', 'batches', 'subjects'));

    }

    public function update(Request $request)
    {

        try {

            if (!isset($request->tbl_item) && !sizeof($request->tbl_item) > 0) {
                return ["Generate Schedule and try again"];
            } else {
                $this->removeScheduleData($request);


                DB::beginTransaction();







                foreach ($request->tbl_item as $key => $list) {


                    $schedule = new BatchSubjectLecture();
                    $schedule->batch_subject_id = $request->as_id;
                    $schedule->lecture_id = $key;

                    $schedule->save();
                }

                DB::commit();

                return redirect('lecture_subject')->with('alert', 'Assign Successfully!');

            }
        } catch (Exception $exception) {
            DB::rollback();
            dd($exception);
        }


    }

    public function removeScheduleData(Request $request){

        $schedule = (new BatchSubjectLecture())->where('batch_subject_id',$request->as_id);
        $schedule->delete();

    }
}
