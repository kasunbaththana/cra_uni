<?php

namespace CRA\Http\Controllers;

use CRA\Facility;
use CRA\Faculties;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Mockery\Exception;

class FacultyController extends Controller
{
    public function index(){

        $faculties = (new Faculties())->get();

        return view('registration.faculties.index', compact('faculties'));

    }

    public function create()
    {

        $faculties = new Faculties();

        $new_index =(new Faculties())->count('id')+1;


        return view('registration.faculties.create', compact('faculties','new_index'));

    }
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:100',
        ]);


        try {

            DB::beginTransaction();

            $faculties = new Faculties($request->all());
            $faculties->save();


            DB::commit();
            return redirect('faculty/create')->with('alert', 'save!');
        } catch (Exception $ex) {
            DB::rollback();
            return $ex;
        }

    }
    public function edit($code)
    {


        $faculties= (new Faculties())->find($code);

        return view('registration.faculties.edit', compact('faculties'));

    }
    public function update(Request $request, $code)
    {

        $this->validate($request, [

            'name' => 'required|max:100',

        ]);

        try {

            $floors = (new Faculties())->find($request->id);
            $floors->update($request->all());

            return redirect('faculty')->with('alert', 'Updated!');
        } catch (Exception $e) {
            return $e;
        }


    }
}
