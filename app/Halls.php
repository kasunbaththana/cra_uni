<?php

namespace CRA;

use Illuminate\Database\Eloquent\Model;

class Halls extends Model
{
    protected $fillable = ['id','name','is_exam_hall','floor_id'];

    public function floorId(){

        return $this->belongsTo(Floors::class,'floor_id');
    }
}
