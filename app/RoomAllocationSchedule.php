<?php

namespace CRA;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class RoomAllocationSchedule extends Model
{
    protected $fillable = ['id','allocation_id','schedule_date','from_time','to_time','day_of_wk','leave'];

    public function getFromTimeAttribute($value)
    {
        if($value!=null){

            $time = Carbon::createFromFormat('H:i:s', $value);
            return $time->format('H:i');

        }else{
            return null;
        }


    }
    public function getToTimeAttribute($value)
    {
        if($value!=null){

            $time = Carbon::createFromFormat('H:i:s', $value);

            return $time->format('H:i');
        }else{

            return null;
        }
    }

}
