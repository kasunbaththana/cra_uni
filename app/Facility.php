<?php

namespace CRA;

use Illuminate\Database\Eloquent\Model;

class Facility extends Model
{
    protected $fillable = ['id','name'];

}
