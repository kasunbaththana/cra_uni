<?php

namespace CRA;

use Illuminate\Database\Eloquent\Model;

class Floors extends Model
{
    protected $fillable = ['id','name','building_id'];

    public function buildingId(){

        return $this->belongsTo(Buildings::class,'building_id');
    }
}
