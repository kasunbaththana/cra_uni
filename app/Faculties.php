<?php

namespace CRA;

use Illuminate\Database\Eloquent\Model;

class Faculties extends Model
{
    protected $fillable = ['id','name'];
}
