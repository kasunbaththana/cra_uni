<?php

namespace CRA;

use Illuminate\Database\Eloquent\Model;

class Batches extends Model
{
    protected $fillable = ['name','degree_id','type','expired_date','capacity'];

    public function degreeId(){

        return $this->belongsTo(Degrees::class,'degree_id');
    }
    public function subjects()
    {
        return $this->hasMany('App\BatchSubject', 'subject_id', 'id');
    }




}
