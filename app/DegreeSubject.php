<?php

namespace CRA;

use Illuminate\Database\Eloquent\Model;

class DegreeSubject extends Model
{
    protected $table = 'degree_subjects';

    protected $fillable = [
        'degree_id', 'subject_id'
    ];

    public function subject()
    {
        return $this->belongsTo(Subject::class, 'subject_id', 'id');
    }

    public function degree()
    {
        return $this->belongsTo(Degrees::class, 'degree_id', 'id');
    }

}
