<?php

namespace CRA;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class RoomAllocation extends Model
{
    protected $fillable = ['id','room_id','batch_id','subject_id','from_date','to_date','from_time','to_time','status',
        'sun' ,
        'sat' ,
        'mon' ,
        'tue' ,
        'wed' ,
        'thu' ,
        'fri'];

    public function roomId(){

        return $this->belongsTo(ClassRooms::class,'room_id');
    }
    public function batchId(){

        return $this->belongsTo(Batches::class,'batch_id');
    }
    public function schedule(){

        return $this->hasMany(RoomAllocationSchedule::class,'allocation_id');
    }
    public function subjectId(){

        return $this->belongsTo(Subject::class,'subject_id');
    }

    public function getFromTimeAttribute($value)
    {
        if($value!=null){

        $time = Carbon::createFromFormat('H:i:s', $value);
        return $time->format('H:i');

        }else{
            return null;
        }


    }
    public function getToTimeAttribute($value)
    {
        if($value!=null){

        $time = Carbon::createFromFormat('H:i:s', $value);

        return $time->format('H:i');
        }else{

            return null;
        }
    }
    public function store(Request $request){


        $fill_table = [
            'room_id' => $request->h_room_id,
            'batch_id' => $request->h_batch_id,
            'subject_id' => $request->h_subject_id,
            'from_date'=>$request->from_date,
            'to_date'=>$request->to_date,
            'from_time'=>$request->from_time,
            'to_time'=>$request->to_time,
            'status' => "P",
            'sun' => isset($request->sun)?true:false,
            'mon' => isset($request->mon)?true:false,
            'tue' => isset($request->tue)?true:false,
            'wed' => isset($request->wed)?true:false,
            'thu' => isset($request->thu)?true:false,
            'fri' => isset($request->fri)?true:false,
            'sat' => isset($request->sat)?true:false,
        ];

        return $this->create($fill_table);
    }




//    public function modify(Request $request){
//
//        $cls = (new RoomAllocation())->find($request->id);
//
//
//        return $cls->update($fill_table);
//    }
}
