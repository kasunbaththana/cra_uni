<?php

namespace CRA;

use Illuminate\Database\Eloquent\Model;

class ExamAllocationLecture extends Model
{
    protected $fillable = ['id','allocation_id','lecture_id'];

    public function allocationId(){

        return $this->belongsTo(ExamAllocation::class,'allocation_id');

    }public function lectureId(){

        return $this->belongsTo(Lecturer::class,'lecture_id');
    }
}
