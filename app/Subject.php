<?php

namespace CRA;

use Illuminate\Database\Eloquent\Model;

class Subject extends Model
{
    protected $fillable = [
        'id',
        'name',
        'description',
        'prefix'
    ];

//    public function degrees()
//    {
//        return $this->belongsToMany('App\Degrees', 'degree_subject', 'subject_id', 'degree_id')->withPivot('subject_duration')->withTimestamps();
//    }

    public function degree_subjects()
    {

        return $this->hasMany(DegreeSubject::class, 'subject_id', 'id');
    }

}
