<?php

namespace CRA;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class ExamAllocationSchedules extends Model
{
    protected $fillable = ['id','allocation_id','schedule_date','from_time','to_time','leave','batch_id','subject_id'];

    public function batchId(){

        return $this->belongsTo(Batches::class,'batch_id');

    }
    public function subjectId(){

    return $this->belongsTo(Subject::class,'subject_id');

}
    public function getFromTimeAttribute($value)
    {
        if($value!=null){

            $time = Carbon::createFromFormat('H:i:s', $value);
            return $time->format('H:i');

        }else{
            return null;
        }


    }
    public function getToTimeAttribute($value)
    {
        if($value!=null){

            $time = Carbon::createFromFormat('H:i:s', $value);

            return $time->format('H:i');
        }else{

            return null;
        }
    }

}
