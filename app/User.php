<?php

namespace CRA;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'nic_no',
        'salutation',
        'name',
        'long_name',
        'prefix',
        'address_line1',
        'address_line2',
        'address_line3',
        'mobile',
        'telephone1',
        'telephone2',
        'fax',
        'email',
        'note',
        'user_role',
        'login_access',
        'password',
        'photo',
        'active',

    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function userRole(){


        return $this->belongsTo(UserRole::class,'user_role');
    }

    // public function role()
    // {
    //     return $this->hasOne('App\UserRole', 'user_id', 'id');
    // }
}
