<?php

namespace CRA;

use Illuminate\Database\Eloquent\Model;

class UserModuleCategory extends Model
{
    protected $table="pro_user_module_category";

    protected $fillable=['id','name','icon'];
}
