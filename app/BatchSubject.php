<?php

namespace CRA;

use Illuminate\Database\Eloquent\Model;

class BatchSubject extends Model
{
    protected $table = 'batch_subjects';

    protected $fillable = ['batch_id','subject_id','subject_duration','semester','year'];

    public function subjectId(){

        return $this->belongsTo(Subject::class,'subject_id');
    }

    public function batches(){

        return $this->belongsTo(Batches::class,'batch_id');
    }

}
