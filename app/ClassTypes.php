<?php

namespace CRA;

use Illuminate\Database\Eloquent\Model;

class ClassTypes extends Model
{
    protected $fillable = ['id','name'];

}
