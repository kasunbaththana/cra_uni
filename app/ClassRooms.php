<?php

namespace CRA;

use Illuminate\Database\Eloquent\Model;

class ClassRooms extends Model
{
    protected $fillable = ['id','name','type','capacity','hall_id','location'];

    public function hallId(){

        return $this->belongsTo(Halls::class,'hall_id');
    }
    public function allocation(){

        return $this->hasMany(RoomAllocation::class,'room_id');
    }
    public function types(){

        return $this->belongsTo(ClassTypes::class,'type');
    }

    public function facility(){

        return $this->hasMany(ClassFacilities::class,'class_room_id');
    }

}
