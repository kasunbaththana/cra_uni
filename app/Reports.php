<?php

namespace CRA;

use Illuminate\Database\Eloquent\Model;

class Reports extends Model
{
    protected $table = 'reports';
    protected $fillable = [
        'name',
        'path'
    ];
}
