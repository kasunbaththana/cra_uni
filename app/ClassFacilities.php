<?php

namespace CRA;

use Illuminate\Database\Eloquent\Model;

class ClassFacilities extends Model
{
    protected $fillable = ['id','class_room_id','facility_id'];

    public function classRoomId(){

        return $this->belongsTo(ClassRooms::class,'class_room_id');
    }

    public function facilityId(){

        return $this->belongsTo(Facility::class,'facility_id');
    }

}
