<?php

namespace CRA\Util;



use CRA\ReportPermissions;
use CRA\Reports;
use CRA\Subject;
use Illuminate\Http\Request;
use Mockery\Exception;
use PDF;
use SebastianBergmann\CodeCoverage\Report\Xml\Report;


class Common
{



    public function reportGeneratePdf(Request $request)
    {

        try{



            $reports = (new Reports())->find($request->report_id);


            $data = (new Subject())->get();


            $pdf = PDF::loadView('pdf.'.$reports->path, compact('data'),[
                'format'=> 'A4',
                'default_font'=> 'sans-serif',
                'orientation'=> 'P',
            ]);


            return $pdf->stream($reports->path.'.pdf');



        }catch(Exception $exception){
            dd($exception);
        }

    }


}

