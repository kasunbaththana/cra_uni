<?php

namespace CRA;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{

    protected $fillable = [
        'id', 'name','faculty_id'
    ];

    public  function facultyId(){

        return $this->belongsTo(Faculties::class,'faculty_id');
    }


}
