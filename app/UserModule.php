<?php

namespace CRA;

use Illuminate\Database\Eloquent\Model;

class UserModule extends Model
{
    protected $table="pro_user_module";

    protected $fillable=[
        'id',
        'name',
        'module_order_no',
        'module_path',
        'module_category_id'];

    public function module_category(){

        return $this->belongsTo(UserModuleCategory::class,'module_category_id');
    }
}
