<?php

namespace CRA;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class ExamAllocation extends Model
{
    protected $fillable = ['id','hall_id','status'];


    public function hallId(){

        return $this->belongsTo(Halls::class,'hall_id');
    }

    public function schedule(){

        return $this->hasMany(ExamAllocationSchedules::class,'allocation_id');
    }

    public function getFromTimeAttribute($value)
    {
        if($value!=null){

            $time = Carbon::createFromFormat('H:i:s', $value);
            return $time->format('H:i');

        }else{
            return null;
        }


    }
    public function getToTimeAttribute($value)
    {
        if($value!=null){

            $time = Carbon::createFromFormat('H:i:s', $value);

            return $time->format('H:i');
        }else{

            return null;
        }
    }
    public function store(Request $request){


        $fill_table = [
            'hall_id' => $request->h_hall_id,
            'status' => "P",
        ];

        return $this->create($fill_table);
    }




}
