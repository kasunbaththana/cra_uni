<?php

namespace CRA;

use Illuminate\Database\Eloquent\Model;

class Buildings extends Model
{

    protected $fillable = ['id','name'];
}
