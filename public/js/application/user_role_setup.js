$(document).ready(function () {

    $('#role_id').change(function(){

        $('#model_view tbody').empty();
    });


    var token = document.getElementsByName('_token');
        $('#view_permission').click(function () {

            $('#model_view tbody').empty();
            var code  = $('#role_id').val()
            $.ajax({
                'type': 'POST',
                'url': '/user_role_setup/view',
                data: {
                    '_token': token[0].value,
                    code: code
                },
                success: function (response) {

                    console.log(response.data)
                    var appending="";
                    for(let i=0;i<response.data.length;i++){

                        let is_view = response.data[i].view==1?'checked':'unchecked';
                        let is_add = response.data[i].add==1?'checked':'unchecked';
                        let is_edit = response.data[i].edit==1?'checked':'unchecked';
                        let is_delete = response.data[i].delete==1?'checked':'unchecked';
                        let is_print = response.data[i].print==1?'checked':'unchecked';

                        console.log(is_add)
                        appending+= '<tr>' +
                            '<td>'+response.data[i].description+'</td>' +
                            '<td class="text-center"><input type="checkbox" id="is_view_'+i+'" name="is_view_'+i+'"  '+is_view+'></td>' +
                            '<td class="text-center"><input type="checkbox" id="is_add_'+i+'" name="is_add_'+i+'"  '+is_add+'></td>' +
                            '<td class="text-center"><input type="checkbox" id="is_edit_'+i+'" name="is_edit_'+i+'"  '+is_edit+'></td>' +
                            '<td class="text-center"><input type="checkbox" id="is_delete_'+i+'" name="is_delete_'+i+'"  '+is_delete+'></td>' +
                            '<td class="text-center"><input type="checkbox" id="is_print_'+i+'" name="is_print_'+i+'"  '+is_print+'></td>' +
                                '<input type="hidden" id="module_id_'+i+'" name="module_id['+i+']" value="'+response.data[i].id+'">'+
                            '</tr>';

                    }


                    $('#model_view > tbody:last-child')
                        .append(appending);


                }
            });

        });
    $('#btnSave').click(function () {

        var form = $('#user_role_setup');
//make array to hidden table schedule looping
        console.log(validate())

        if(validate()) {
            $.ajax({
                type: "POST",
                url: form.attr('action'),
                data: form.serialize(),
                success: function (response) {

                    console.log(response[0])
                    if (response[0] == "save") {
                        alert("Save Success !");
                        //location.href = response[1];
                        window.location.replace(response[1]);
                    } else {
                        alert("Save Fail ! " + response[0]);
                    }

                },
                error: function (xhr, status, error) {
                    errorDisplay(xhr.responseText);//calling common js

                }
            });

        }else{

            alert("please generate schedule before save");

        }

    });

    function validate(){

        var rows = document.getElementById('model_view').getElementsByTagName("tr").length;

        console.log(rows)
        if(parseInt(rows) > 0){

            return true;
        }else{


            return false;
        }

    }



})