$(document).ready(function () {

    // Denotes total number of rows
    let rowIdx = 0;
    let subjectIds = [];
    let lastRowId = 0;

    async function fetchSubjects() {
        return $.get('/subjects_list', function (response) {
            return response;
        });
    }


    // jQuery button click event to add a row
    $('#subjects_addBtn').on('click', async function () {

        const subjectsList = await fetchSubjects();
        let list = [];

        subjectsList.forEach(row => {
            list.push("<option  value="+row.id+">"+row.name+"</option>");
        });
        rowIdx=  document.getElementById('tbl_subject').getElementsByTagName("tr").length;
        rowIdx = rowIdx-1;
        // Adding a row inside the tbody.
        $('#subjects_tbody').append(`
    <tr id="R${rowIdx}"> 
      <td class="row-index text-center"> 
        <select class="form-control form-control-sm subject_" name="subject${rowIdx}" id="subject${rowIdx}">
          <option value="">-- Select subject</option>
          ${list}
        </select>
      </td> 
      <td class="text-center"> 
        <button class="btn btn-sm btn-danger remove" type="button">Remove</button> 
      </td>
    </tr>
      `);
    });


    $(document).on('change','select', function(){


        var ss = document.getElementById('tbl_subject').getElementsByTagName("tr").length;

        var g = $(this).get();

        var select_index = (g[0]['name']);

        for (var b = 0; b <= ss-1 ; b++) {

            if('subject'+b == select_index){


            }else{

                if($('#subject'+b).val() == $(this).val()){

                    let vl =parseInt(b)+1;
                    alert("This Subject is already added \n"+"Check line number "+vl);
                    $(this).val("")
                }
            }


            // if($('#subject'+b).val()!=undefined){
            //     console.log($('#subject'+b).val());
            //
            // }
        }

    });

    // $('#subject' + lastRowId).change(function () {
    //   console.log(lastRowId);
    //   // subjectIds.push(
    //   //   $('#subject' + rowIdx).val()
    //   // )
    //   // console.log(subjectIds);
    // });

    // jQuery button click event to remove a row.
    $('#subjects_tbody').on('click', '.remove', function () {

        // Getting all the rows next to the row
        // containing the clicked button
        var child = $(this).closest('tr').nextAll();

        // Iterating across all the rows
        // obtained to change the index
        child.each(function () {

            // Getting <tr> id.
            var id = $(this).attr('id');

            // Getting the <p> inside the .row-index class.
            var idx = $(this).children('.row-index').children('p');

            // Gets the row number from <tr> id.
            var dig = parseInt(id.substring(1));

            // Modifying row index.
            idx.html(`Row ${dig - 1}`);

            // Modifying row id.
            $(this).attr('id', `R${dig - 1}`);
        });

        // Removing the current row.
        $(this).closest('tr').remove();

        // Decreasing total number of rows by 1.
        rowIdx--;
    });

    // * End of dynamic list section jQuery * //
});
