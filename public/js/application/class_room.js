$(document).ready(function () {

    $('#building_id').click(function () {

        var val =$('#building_id').val();
        var token = (document.getElementsByName('_token'));
        $("#floor_id").empty();
        $.ajax({
            type: 'post',
            url: '/class_room/load_floors',
            data: {
                "_token": token[0].value,
                building_id: val,
            },
            success: function (response) {


                for(var i=0;i<response.floors.length;i++){


                    $("#floor_id").append("<option value='"+response.floors[i].id+"'>"+response.floors[i].name+"</option>");

                }

            }
        });
    })
    $('#floor_id').click(function () {

        var val =$('#floor_id').val();
        var token = (document.getElementsByName('_token'));
        $("#hall_id").empty();
        $.ajax({
            type: 'post',
            url: '/class_room/load_halls',
            data: {
                "_token": token[0].value,
                floor_id: val,
            },
            success: function (response) {


                for(var i=0;i<response.halls.length;i++){


                    $("#hall_id").append("<option value='"+response.halls[i].id+"'>"+response.halls[i].name+"</option>");

                }

            }
        });
    })

    $('#addItem').click(function () {
        var tmp = $('#item').val();
        var token = document.getElementsByName('_token');

        var rows = document.getElementById('tbl_item').getElementsByTagName("tr");

        var ss = document.getElementById('item').length;



        var result =true;
        for (var b = 1; b <= ss ; b++) {

            if($('#tbl_item_'+b).val()!=undefined){
                console.log(tmp+" "+$('#tbl_item_'+b).val());
                if(b==tmp){

                console.log(b+" "+$('#tbl_item_'+b).val());
                    result = false;
                }
            }
        }

        if(result) {
            $.ajax({
                'type': 'POST',
                'url': '/class_room/get_items',
                data: {
                    '_token': token[0].value,
                    code: tmp
                },
                success: function (response) {

                    $('#tbl_item > tbody:last-child')
                        .append('<tr>' +
                            '<td><input class="form-control form-control-sm" border="0" readonly id="tbl_item_' + response.items.id + '"' +
                            ' name="tbl_item[' + response.items.id + ']" ' +
                            'value="' + response.items.name + '"/></td>' +
                            '<td> <a type="button"  class="btn btn-danger btn-circle btn-sm text-gray-100">' +
                            '<i class="fas fa-trash"></i></td></tr>');
                }
            });
        }
    });


    $('#tbl_item tbody').on('click', '.btn', function () {
        $(this).closest('tr').remove();

    });


})