$(document).ready(function(){

    $('.date').datepicker({
        format: 'yyyy-mm-dd',
        autoclose:true,

    });
    $('.selectpicker').selectpicker();

    console.log('3333');

    $('#t_date').change(function () {


        makeBatchName()

    });
    makeBatchName()
    // var today = new Date();
    // $("#t_date").datepicker( "setDate" , today );
    // var dateToday = new Date();
    // $('.year').datepicker({
    //     format: 'LT'
    // });

    var startDate = new Date();
    var FromEndDate = new Date();
    $('.date_from').datepicker({
        format: 'yyyy-mm-dd',
        startDate: startDate,
       // autoclose: true
        // endDate: '1y'

    }).on('changeDate', function (selected) {
        startDate = new Date(selected.date.valueOf());
        startDate.setDate(startDate.getDate(new Date(selected.date.valueOf())));
        $('.date_to').datepicker('setStartDate', startDate);
    });





    $('.date_to').datepicker({
        format: 'yyyy-mm-dd',
        startDate: startDate,
    }).on('changeDate', function (selected) {
        FromEndDate = new Date(selected.date.valueOf());
        FromEndDate.setDate(FromEndDate.getDate(new Date(selected.date.valueOf())));
        $('.date_from').datepicker('setEndDate', FromEndDate);
    });

    $(".form_datetime").datetimepicker({format: 'hh:mm'});
    // $('#time_to').datetimepicker({
    //     format: 'LT'
    // });
    //
    //
    if($('.time').val()==null || $('.time').val() == ''){ $('.time').val('08:00')}


    $('#from_time').change(function(){

        timeValidate();

    });
    $('#to_time').change(function(){

        toTimeValidate();

    });
    $('#addItem').click(function () {
        var tmp = $('#item').val();
        var token = document.getElementsByName('_token');

        var rows = document.getElementById('tbl_item').getElementsByTagName("tr");

        var ss = document.getElementById('item').length;



        var result =true;
        for (var b = 1; b <= ss ; b++) {

            if($('#tbl_item_'+b).val()!=undefined){
                console.log(tmp+" "+$('#tbl_item_'+b).val());
                if(b==tmp){

                    console.log(b+" "+$('#tbl_item_'+b).val());
                    result = false;
                }
            }
        }
        console.log(tmp)
        if(result) {
            $.ajax({
                'type': 'POST',
                'url': '/exam_allocation/get_items',
                data: {
                    '_token': token[0].value,
                    code: tmp
                },
                success: function (response) {

                    $('#tbl_item > tbody:last-child')
                        .append('<tr>' +
                            '<td><input class="form-control form-control-sm" border="0" readonly id="tbl_item_' + response.items.id + '"' +
                            ' name="tbl_item[' + response.items.id + ']" ' +
                            'value="' + response.items.name + '"/></td>' +
                            '<td> <a type="button"  class="btn btn-danger btn-circle btn-sm text-gray-100">' +
                            '<i class="fas fa-trash"></i></td></tr>');
                }
            });
        }
    });


    $('#tbl_item tbody').on('click', '.btn', function () {
        $(this).closest('tr').remove();

    });


    $('#batch_id').click(function () {

        batchClick();

    });
    function batchClick(){

        tableDestroy();

        var val = $('#batch_id').val();
        var token = (document.getElementsByName('_token'));
        document.getElementById("title").innerHTML = "Batch";
        $.ajax({
            type: 'post',
            url: '/room_allocation/batch',
            data: {
                "_token": token[0].value,
                id: val,
            },
            success: function (response) {


                $('#dataTable1').DataTable( {
                    data: response.data,
                    columns: response.head

                });
                $('#temp_').val('batch_id');
                $('#search_model').modal('show');

            }
        });


    }

    $('#subject_id').click(function () {

        subjectClick();

    });
    function subjectClick(){

        tableDestroy();

        var val = $('#subject_id').val();
        var token = (document.getElementsByName('_token'));
        document.getElementById("title").innerHTML = "Batch";
        $.ajax({
            type: 'post',
            url: '/room_allocation/subject',
            data: {
                "_token": token[0].value,
                id: val,
            },
            success: function (response) {


                $('#dataTable1').DataTable( {
                    data: response.data,
                    columns: response.head

                });
                $('#temp_').val('subject_id');
                $('#search_model').modal('show');

            }
        });


    }



    function timeValidate(){

        var start_time = $("#from_time").val();
        var end_time = $("#to_time").val();
        var stt,endt;


        var stt2 = new Date("November 13, 2013 " + start_time);
        stt = stt2.getTime();

        var endt2 = new Date("November 13, 2013 " + end_time);
        endt = endt2.getTime();


console.log(stt + " > "+ endt);
        if(stt >= endt){

            stt2.setMinutes(stt2.getMinutes() + 30);

            var h = addZero(stt2.getHours(), 2);
            var m = addZero(stt2.getMinutes(), 2);


            var tm= h+":"+m;
            console.log(tm)

            $("#to_time").val(tm)
        }else{


        }






    }
    function toTimeValidate(){

        var start_time = $("#from_time").val();
        var end_time = $("#to_time").val();
        var stt,endt;


        var stt2 = new Date("November 13, 2013 " + start_time);
        stt = stt2.getTime();

        var endt2 = new Date("November 13, 2013 " + end_time);
        endt = endt2.getTime();


        console.log(stt + " < "+ endt);
        if(stt >= endt){

            stt2.setMinutes(stt2.getMinutes() + 30);

            var h = addZero(stt2.getHours(), 2);
            var m = addZero(stt2.getMinutes(), 2);


            var tm= h+":"+m;
            console.log(tm)

            $("#to_time").val(tm)
        }else{


        }






    }

    function addZero(x, n) {
        while (x.toString().length < n) {
            x = "0" + x;
        }
        return x;
    }



    function makeBatchName(){

        var token = document.getElementsByName('_token');
        var t_date = $('#t_date').val();
        var building_id = $('#building_id').val();
        $('#grid_room tbody').empty();
        if(t_date!=null)
        $.ajax({
            type: 'post',
            url: '/exam_allocation/data_find',
            data:{
                '_token':token[0].value,
                't_date':t_date,
                'building_id':building_id
            },success: function (response) {


                let temp_td = '';
                let temp_in_td = '';
                let counter = ['A','B'];


                for (let h = 0; h < response.rooms.length; h++) {
                    if (response.data.length > 0) {

                        temp_td += '<tr> <div class=""><td class=" bg-secondary text-white text-center ac">' + response.rooms[h].name + '</td></div>';



                        for (let j = 0; j < counter.length; j++) {

                            for (let i = 0; i < response.data.length; i++) {


                                if (parseInt(response.rooms[h].id) == parseInt(response.data[i].hall_id)) {



                                    if (response.data[i].grade == counter[j]) {
                                        temp_in_td += '<div class="bg-info ac col-11 m-1 "> '
                                            + '<a type="button" href="exam_allocation/edit/' + response.data[i].id + '" '
                                            + '<i class="fa  text-white "> '
                                            + '<div> Res No: ' + response.data[i].id +' Title: '+response.data[i].subject+ ' </br> ' + response.data[i].batch + ' at : ' + response.data[i].from_time + ' ' + 'to: ' + response.data[i].to_time + '</div>'
                                            + '</i></a></div>';

                                    }else if(response.data[i].S1 == counter[j] ){

                                        temp_in_td += '<div class="bg-info ac col-11 m-1 " > '
                                            + '<a type="button" href="exam_allocation/edit/' + response.data[i].id + '" '
                                            + '<i class="fa  text-white "> '
                                            + '<div> Res No: ' + response.data[i].id +' Title: '+response.data[i].subject+ ' </br> ' + response.data[i].batch + ' at : ' + response.data[i].from_time + ' ' + 'to: ' + response.data[i].to_time + '</div>'
                                            + '</i></a></div>';

                                    }
                                    else if(response.data[i].S2 == counter[j] ){

                                        temp_in_td += '<div class="bg-info ac col-11 m-1 " > '
                                            + '<a type="button" href="exam_allocation/edit/' + response.data[i].id + '" '
                                            + '<i class="fa  text-white "> '
                                            + '<div> Res No: ' + response.data[i].id +' Title: '+response.data[i].subject+ ' </br> ' + response.data[i].batch + ' at : ' + response.data[i].from_time + ' ' + 'to: ' + response.data[i].to_time + '</div>'
                                            + '</i></a></div>';

                                    }

                                } else {


                                }


                            }

                            temp_td += '<td class="text-sm-left">'
                                + '<div class="row p-2">' + temp_in_td + '</div></td>';
                            temp_in_td = '';
                        }

                        $('#grid_room > tbody:last-child')
                            .append(
                                temp_td +
                                '</tr>');
                        temp_td = '';

                    } else {
                        temp_td = '<td class="text-sm-left ac"><div class="" >' + response.rooms[h].name + '</div></td>';
                        temp_td += '<td class="text-sm-left ac"><div class="" ></div></td>';
                        temp_td += '<td class="text-sm-left ac"><div class="" ></div></td>';

                    $('#grid_room > tbody:last-child')
                        .append('<tr>' +
                            temp_td +
                            '</tr>');

                }
            }

            }
        });

    }
    $('#hall_id').click(function () {

        roomClick();
console.log('ddddddd');
    });

    $('.chkcl').click(function() {
        var the_input = $(this).next('input');
        if (the_input.prop('checked')) {

            console.log("ddddd");
        }
    });
    $('#building_id').change(function(){

        makeBatchName();
    });

    //$('table.dataTable1').DataTable();
    $('#dataTable1').dataTable( {
        "columns": [
            {
                "data": ""
            }
        ]
    } );


        function roomClick(){

            tableDestroy()


        var token = (document.getElementsByName('_token'));



            document.getElementById("title").innerHTML = "Class Room";



            $.ajax({
                type: 'post',
                url: '/exam_allocation/halls',
                data: {
                    "_token": token[0].value,
                },
                success: function (response) {

                    $('#dataTable1').DataTable( {
                        data: response.data,
                        columns: response.head

                    });

                    $('#temp_').val('hall_id');
                    $('#search_model').modal('show');

                }
            });


    }

    function tableDestroy() {

        var table = $('#dataTable1').DataTable();

        table.clear().destroy();
        $('#dataTable1').find('td,th').remove();
       // $('#dataTable1 tbody').empty();

    }
    $("table.dataTable1").on("click", "tr", function () {

        var cur_row = $(this).closest('tr');
        var col1 = cur_row.find('td:eq(0)').text();
        var col2 = cur_row.find('td:eq(1)').text();
        var col3 = cur_row.find('td:eq(3)').text();


        var concat_string = (col2);
        var id = $('#temp_').val()

        $('#'+id).val(concat_string);
        $('#h_'+id).val(col1);


        $('#search_model').modal('hide');

        // if(id == "room_id"){
        //
        //
        //     $('#h_room_cap').val(col3);
        // }




    });


    $('#btnSave').click(function () {

        var form = $('#exam_allocation');
//make array to hidden table schedule looping
        if(validate()) {
            $.ajax({
                type: "POST",
                url: form.attr('action'),
                data: form.serialize(),
                success: function (response) {

                    if (response[0] == "save") {
                        alert("Save Success !");
                        //location.href = response[1];
                        window.location.replace(response[1]);
                    } else {
                        alert("Save Fail ! " + response[0]);
                    }

                },
                error: function (xhr, status, error) {
                    errorDisplay(xhr.responseText);//calling common js

                }
            });

        }else{

            alert("please generate schedule before save");

        }

    });

    $('#generate').click(function () {

      //  $('#tbl_schedule tbody').empty();
        var from_date = $('#from_date').val();


        var from_time = $('#from_time').val();
        var to_time = $('#to_time').val();

        let s_date = from_date;
        let a=0;
        var ss =$('#tbl_schedule tr').length;
        var degree = $('#batch_id').val();
        var subject = $('#subject_id').val();
        var hall_id = $('#hall_id').val();


        var time_dif = moment.duration(moment(to_time, "HH:mm:ss").diff(moment(from_time, "HH:mm:ss")));
var result =true;
        if(hall_id == ""){

            alert("Please fill All details!");

        }else if(degree == ""){


            alert("Please fill All details!");
        }else if(subject == ""){

            alert("Please fill All details!");
        }else if(from_date == "") {

            alert("Please fill All details!");
        }else if(time_dif.asMinutes()<30){

            alert("Please assign valued time");
        }else {

            var form = $('#exam_allocation');
            $.ajax({
                type: "POST",
                url: '/exam_allocation/validate_check',
                data: form.serialize(),
                success: function (response) {

                    if (parseInt(response[0])<=0){

                        for (var b = 1; b <= ss; b++) {

                            if ($('#s_date_' + b).val() == s_date &&
                                $('#degree_' + b).val() == degree &&
                                $('#subject_' + b).val() == subject
                            ) {

                                console.log($('#f_time_' + b).val());
                                console.log(from_time);

                                console.log($('#f_time_' + b).val());
                                console.log(to_time);


                                if ((!(from_time < $('#f_time_' + b).val()) && !(to_time < $('#f_time_' + b).val()))
                                ) {

                                    result = false;
                                    break;
                                } else if ((!(from_time > $('#t_time_' + b).val()))) {

                                    result = true;
                                    break;
                                }
                            }
                        }
                        console.log("res " + result)
                        if (result) {
                            let d = parseInt(moment(s_date).day());


                            makeShedule(ss, s_date, $('#from_time').val(), $('#to_time').val());

                        }

                    }else{

                        alert("Already reserved\n On : "+s_date+"\nBatch : "+degree+"\nCheck Time");
                    }


                },
                error: function (xhr, status, error) {
                    errorDisplay(xhr.responseText);//calling common js

                }
            });



        }

    });

    function clear(){

        $('#batch_id').val("");
        $('#subject_id').val("");
        $('#hall_id').attr("readonly",'readonly');
        $('#hall_id').off('click');
        $('#from_date').val("");

    }
     function makeShedule(a,s_date,from_time,to_time){
         var from_date = $('#from_date').val();
         var degree = $('#batch_id').val();
         var subject = $('#subject_id').val();


         var h_batch_id = $('#h_batch_id').val();
         var h_subject_id = $('#h_subject_id').val();

         $('#tbl_schedule > tbody:last-child')
             .append('<tr>' +
                 '<td>'+a+'</td>' +
                 '<td><input readonly type="text"  class="border-0 text-gray-800 bg-gray-100" id="s_date_'+a+'" name="s_date_'+a+'" value="'+s_date+'"></td>' +
                 '<td><input readonly type="text" class="border-0 text-gray-800 bg-gray-100" id="degree_'+a+'" name="degree_'+a+'" value="'+degree+'"></td>' +
                 '<td><input readonly type="text" class="border-0 text-gray-800 bg-gray-100" id="subject_'+a+'" name="subject_'+a+'" value="'+subject+'"></td>' +
                 '<td><input readonly type="time" class="time" id="f_time_'+a+'" name="f_time_'+a+'" value="'+from_time+'"></td>' +
                 '<td><input readonly type="time" class="time" id="t_time_'+a+'" name="t_time_'+a+'" value="'+to_time+'"></td>' +
                 '<td class="text-center"><a type="button"  class="btn btn-danger btn-circle btn-sm text-gray-100"><i class="fas fa-trash"></i></a></td>' +
                 '<input type="hidden" name="degree_id_'+a+'" id="degree_id_'+a+'" value="'+h_batch_id+'" >'+
                 '<input type="hidden" name="subject_id_'+a+'" id="subject_id_'+a+'" value="'+h_subject_id+'" >'+
                 '</tr>');

         clear();
    }
    $('#tbl_schedule tbody').on('click', '.btn', function () {
        $(this).closest('tr').remove();

    });
    function validate(){

        var rows = document.getElementById('tbl_schedule').getElementsByTagName("tr").length;

        var shd = []
        $('#schedule').val("");
        for (var b = 1; b < rows ; b++) {

            if ($('#date_'+b).val() != ''  && $('#f_time_' + b).val() !== '' && $('#t_time_' + b).val() !== '') {
                shd.push([
                    $('#s_date_'+b).val(),
                    $('#f_time_' + b).val(),
                    $('#t_time_' + b).val(),
                    $('#degree_id_' + b).val(),
                    $('#subject_id_' + b).val(),
                    ])

            }
        }


        $('#schedule').val(JSON.stringify(shd));
        return shd.length>0;
    }

    $('.chg_schedule').change(function () {

      //  $('#tbl_schedule tbody').empty();
    });






});