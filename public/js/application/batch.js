/**
 * Created by USER on 5/3/2020.
 */
$(document).ready(function () {
    var result = [];
    var subjectList = [];

    var dateToday = new Date();
    $('.date').datepicker({
        format: "yyyy",
        viewMode: "years",
        minViewMode: "years",
        startDate: dateToday,
        autoclose: true
        // endDate: '1y'
    });


    $('#degree_id').change(function () {


       makeBatchName()
        getSubjects($('#degree_id').val());

    });
    $('#year').change(function () {


        setBatchName()

    });
    $('#type').change(function () {


        setBatchName()

    });



    function makeBatchName(){

        var token = document.getElementsByName('_token');
        var id = $('#degree_id').val();
            $.ajax({
                type: 'post',
                url: '/batch/prefix',
                data:{
                    '_token':token[0].value,
                    'id':id
                },success: function (response) {

                    $('#prefix').val(response.degrees.prefix)
                    let _type =[]

                    if(response.degrees.is_weekend == '1'){
                        _type.push('B2')
                    }if(response.degrees.is_weekday == '1'){
                        _type.push('B1')
                    }

                    console.log(_type)

                    var type = $('#type');
                    type.find('option')
                        .remove();

                    $.each(_type, function(key,value) {
                        var option = $("<option/>", {
                            value: value,
                            text: value
                        });
                        type.append(option);
                    });



                    setBatchName()
                }
            });

    }
     function setBatchName(){

        var prefix = $('#prefix').val();
        var year = $('#year').val();
        var type = $('#type').val();



        if((prefix != null || prefix != '') && (year != '') && (type != null || type != '')){

            var name = prefix+'/'+year+'/'+type;

            $('#name').val(name);
        }

    }
    function getSubjects(degree_id){

        var token = document.getElementsByName('_token');
        $('#subjects_ tbody').empty();

        $.ajax({
            'type': 'POST',
            'url': '/degree/subjects_list',
            data: {
                '_token': token[0].value,
                id: degree_id
            },
            success: function (response) {

                let count=0;
                response.forEach(row => {
                    count = count + 1;
                    $('#subjects_ > tbody:last-child')
                        .append('<tr>' +
                            '<td>' +
                            '<input type="number" class="form-control form-control-sm" border="0"  id="tbl_year' + count+ '"' +
                            ' name="tbl_year' + count + '" />' +
                            '</td>' +
                            '<td>' +
                            '<input type="number" class="form-control form-control-sm" border="0"  id="tbl_sem' + count+ '"' +
                            ' name="tbl_sem' + count + '" />' +
                            '</td>' +
                            '<td>' +
                            '<input class="form-control form-control-sm" border="0" readonly id="tbl_subject' + count + '"' +
                            ' name="tbl_subject' + count + '" value="' + row.subject.name + '"/>' +
                            ' <input type="hidden" name="hid_subj' + count + '" id="hid_subj' + count + '" value="' + row.subject.id + '" /> '+
                            '</td>' +
                            '<td>' +
                            '<input type="number" class="form-control form-control-sm" border="0"  id="tbl_duration' + count + '"' +
                            ' name="tbl_duration' + count + '" placeholder="Days" maxlength="3"/>' +
                            '</td>' +
                            '<td class="text-center"> <a type="button"  class="btn btn-danger btn-circle btn-sm text-gray-100">' +
                            '<i class="fas fa-trash"></i></td></tr>');
                });


            }
        });


    }
    $('#subjects_ tbody').on('click', '.btn', function () {
        $(this).closest('tr').remove();

    });

    $('#btnSave').click(function () {

        var form = $('#batch');
//make array to hidden table schedule looping
        if(validate()) {
            $.ajax({
                type: "POST",
                url: form.attr('action'),
                data: form.serialize(),
                success: function (response) {

                    if (response[0] == "save") {
                        alert("Save Success !");
                        //location.href = response[1];
                        window.location.replace(response[1]);
                    } else {
                        alert("Save Fail ! " + response[0]);
                    }

                },
                error: function (xhr, status, error) {
                    errorDisplay(xhr.responseText);//calling common js

                }
            });

        }else{

            alert("Something Went Wrong ! fill all details in subject table !");

        }

    });
    function validate(){
        $('#tmp_table').val("");
        var rows = document.getElementById('subjects_').getElementsByTagName("tr").length;

        var shd = []
        console.log(rows)

        for (var b = 0; b < rows ; b++) {

            if ($('#tbl_year'+b).val() != ''  && $('#tbl_sem' + b).val() !== '' && $('#hid_subj' + b).val() !== '' && $('#tbl_duration' + b).val() !=='') {
                if ($('#tbl_year'+b).val() != null  && $('#tbl_sem' + b).val() !== null && $('#hid_subj' + b).val() !== null && $('#tbl_duration' + b).val() !==null) {

                    shd.push([$('#tbl_year' + b).val(), $('#tbl_sem' + b).val(), $('#hid_subj' + b).val(), $('#tbl_duration' + b).val()])
                }
            }
        }


        $('#tmp_table').val(JSON.stringify(shd));
        return shd.length>0;
    }


    $('#addItem').on('click', function () {
        var tmp = $('#item').val();
        var token = document.getElementsByName('_token');

        var count = document.getElementById('subjects_').getElementsByTagName("tr").length;

        var ss = document.getElementById('item').length;



        var result =true;
        for (var b = 1; b <= ss ; b++) {

            if($('#hid_subj'+b).val()!=undefined){
                console.log(tmp+" "+$('#hid_subj'+b).val());
                let bb = $('#hid_subj'+b).val();
                if(bb==tmp){

                   console.log("sssssssssss");
                    result = false;
                }
            }
        }

        count = count-1;

        if(result) {
            $.ajax({
                'type': 'POST',
                'url': '/batch/batch_list',
                data: {
                    '_token': token[0].value,
                    id: tmp
                },
                success: function (row) {



                        console.log(row)
                        count = count + 1;
                    $('#subjects_ > tbody:last-child')
                        .append('<tr>' +
                            '<td>' +
                            '<input type="number" class="form-control form-control-sm" border="0"  id="tbl_year' + count+ '"' +
                            ' name="tbl_year' + count + '" />' +
                            '</td>' +
                            '<td>' +
                            '<input type="number" class="form-control form-control-sm" border="0"  id="tbl_sem' + count+ '"' +
                            ' name="tbl_sem' + count + '" />' +
                            '</td>' +
                            '<td>' +
                            '<input class="form-control form-control-sm" border="0" readonly id="tbl_subject' + count + '"' +
                            ' name="tbl_subject' + count + '" value="' + row.name + '"/>' +
                                ' <input type="hidden" name="hid_subj' + count + '" id="hid_subj' + count + '" value="' + row.id + '" /> '+
                            '</td>' +
                            '<td>' +
                            '<input type="number" class="form-control form-control-sm" border="0"  id="tbl_duration' + count + '"' +
                            ' name="tbl_duration' + count + '" placeholder="Days" maxlength="3" />' +
                            '</td>' +
                            '<td class="text-center"> <a type="button"  class="btn btn-danger btn-circle btn-sm text-gray-100">' +
                            '<i class="fas fa-trash"></i></td></tr>');

                }
            });
        }

    });




});