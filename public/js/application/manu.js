$(document).ready(function () {

    var token = document.getElementsByName('_token');


    $.ajax({
        'type': 'post',
        'url': 'main_page/login/permission',
        'data': {
            '_token': token[0].value
        },
        success: function (response) {

            var link_reg = [];
            var link_tranc = [];
            var link_payment = [];
            var link_report = [];
            var link_voucher = [];
            var link_setting = [];
            var x=-1;
            for(var j=0 ; j < response.module.length ; j++) {

                for (var i = 0; i < response.user_auth.length; i++) {
                    //   console.log(response.user_auth[i].module.module_path);

                    if (response.user_auth[i].module.module_category_id === 1 && response.module[j].id === response.user_auth[i].module.id ) {
                        x++;

                        var link = $("<a>");
                        link.attr("href", response.user_auth[i].module.module_path);
                        link.attr("title", "some Description");
                        link.text(response.user_auth[i].module.name);
                        link.addClass("collapse-item");
                        link_reg[x] = link;
                    } else if (response.user_auth[i].module.module_category_id === 2) {
                        var link = $("<a>");
                        link.attr("href", response.user_auth[i].module.module_path);
                        link.attr("title", "some Description");
                        link.text(response.user_auth[i].module.name);
                        link.addClass("collapse-item");
                        link_tranc[i] = link;
                    } else if (response.user_auth[i].module.module_category_id === 3) {
                        var link = $("<a>");

                        link.addClass(response.user_auth[i].module_category_icon);
                        link.addClass("collapse-item ");
                        link.attr("href", response.user_auth[i].module.module_path);
                        link.attr("title", "some Description");
                        link.text(response.user_auth[i].module.name);
                        link_setting[i] = link;


                    } else if (response.user_auth[i].module.module_category_id === 4) {
                        var link = $("<a>");
                        link.attr("href", response.user_auth[i].module.module_path);
                        link.attr("title", "some Description");
                        link.text(response.user_auth[i].module.name);
                        link.addClass("collapse-item");
                        link_report[i] = link;
                    } else if (response.user_auth[i].module.module_category_id === 5) {
                        var link = $("<a>");
                        link.attr("href", response.user_auth[i].module.module_path);
                        link.attr("title", "some Description");
                        link.text(response.user_auth[i].module.name);
                        link.addClass("collapse-item");
                        link_voucher[i] = link;
                    }
                    /*  <i class="fas fa-fw text-danger">SS</i> */
                }
            }

            $(".registration").html(link_reg);
            $(".transaction").html(link_tranc);
            $(".setting").html(link_setting);
            $(".report").html(link_report);
            $(".loan_voucher").html(link_voucher);


        }

    });


});
